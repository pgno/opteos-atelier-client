var t,e,n,i,r="undefined"!=typeof globalThis?globalThis:"undefined"!=typeof self?self:"undefined"!=typeof window?window:"undefined"!=typeof global?global:{},s=!1,o=!1,a=[];function l(t){!function(t){a.includes(t)||a.push(t);o||s||(s=!0,queueMicrotask(c))}(t)}function h(t){let e=a.indexOf(t);-1!==e&&a.splice(e,1)}function c(){s=!1,o=!0;for(let t=0;t<a.length;t++)a[t]();a.length=0,o=!1}var u=!0;function f(t){e=t}var d=[],p=[],g=[];function m(t,e){"function"==typeof e?(t._x_cleanups||(t._x_cleanups=[]),t._x_cleanups.push(e)):(e=t,p.push(e))}function y(t,e){t._x_attributeCleanups&&Object.entries(t._x_attributeCleanups).forEach((([n,i])=>{(void 0===e||e.includes(n))&&(i.forEach((t=>t())),delete t._x_attributeCleanups[n])}))}var v=new MutationObserver(C),b=!1;function _(){v.observe(document,{subtree:!0,childList:!0,attributes:!0,attributeOldValue:!0}),b=!0}function E(){(w=w.concat(v.takeRecords())).length&&!S&&(S=!0,queueMicrotask((()=>{C(w),w.length=0,S=!1}))),v.disconnect(),b=!1}var w=[],S=!1;function A(t){if(!b)return t();E();let e=t();return _(),e}var x=!1,T=[];function C(t){if(x)return void(T=T.concat(t));let e=[],n=[],i=new Map,r=new Map;for(let s=0;s<t.length;s++)if(!t[s].target._x_ignoreMutationObserver&&("childList"===t[s].type&&(t[s].addedNodes.forEach((t=>1===t.nodeType&&e.push(t))),t[s].removedNodes.forEach((t=>1===t.nodeType&&n.push(t)))),"attributes"===t[s].type)){let e=t[s].target,n=t[s].attributeName,o=t[s].oldValue,a=()=>{i.has(e)||i.set(e,[]),i.get(e).push({name:n,value:e.getAttribute(n)})},l=()=>{r.has(e)||r.set(e,[]),r.get(e).push(n)};e.hasAttribute(n)&&null===o?a():e.hasAttribute(n)?(l(),a()):l()}r.forEach(((t,e)=>{y(e,t)})),i.forEach(((t,e)=>{d.forEach((n=>n(e,t)))}));for(let t of n)if(!e.includes(t)&&(p.forEach((e=>e(t))),t._x_cleanups))for(;t._x_cleanups.length;)t._x_cleanups.pop()();e.forEach((t=>{t._x_ignoreSelf=!0,t._x_ignore=!0}));for(let t of e)n.includes(t)||t.isConnected&&(delete t._x_ignoreSelf,delete t._x_ignore,g.forEach((e=>e(t))),t._x_ignore=!0,t._x_ignoreSelf=!0);e.forEach((t=>{delete t._x_ignoreSelf,delete t._x_ignore})),e=null,n=null,i=null,r=null}function I(t){return k(L(t))}function D(t,e,n){return t._x_dataStack=[e,...L(n||t)],()=>{t._x_dataStack=t._x_dataStack.filter((t=>t!==e))}}function O(t,e){let n=t._x_dataStack[0];Object.entries(e).forEach((([t,e])=>{n[t]=e}))}function L(t){return t._x_dataStack?t._x_dataStack:"function"==typeof ShadowRoot&&t instanceof ShadowRoot?L(t.host):t.parentNode?L(t.parentNode):[]}function k(t){let e=new Proxy({},{ownKeys:()=>Array.from(new Set(t.flatMap((t=>Object.keys(t))))),has:(e,n)=>t.some((t=>t.hasOwnProperty(n))),get:(n,i)=>(t.find((t=>{if(t.hasOwnProperty(i)){let n=Object.getOwnPropertyDescriptor(t,i);if(n.get&&n.get._x_alreadyBound||n.set&&n.set._x_alreadyBound)return!0;if((n.get||n.set)&&n.enumerable){let r=n.get,s=n.set,o=n;r=r&&r.bind(e),s=s&&s.bind(e),r&&(r._x_alreadyBound=!0),s&&(s._x_alreadyBound=!0),Object.defineProperty(t,i,{...o,get:r,set:s})}return!0}return!1}))||{})[i],set:(e,n,i)=>{let r=t.find((t=>t.hasOwnProperty(n)));return r?r[n]=i:t[t.length-1][n]=i,!0}});return e}function N(t){let e=(n,i="")=>{Object.entries(Object.getOwnPropertyDescriptors(n)).forEach((([r,{value:s,enumerable:o}])=>{if(!1===o||void 0===s)return;let a=""===i?r:`${i}.${r}`;var l;"object"==typeof s&&null!==s&&s._x_interceptor?n[r]=s.initialize(t,a,r):"object"!=typeof(l=s)||Array.isArray(l)||null===l||s===n||s instanceof Element||e(s,a)}))};return e(t)}function R(t,e=(()=>{})){let n={initialValue:void 0,_x_interceptor:!0,initialize(e,n,i){return t(this.initialValue,(()=>function(t,e){return e.split(".").reduce(((t,e)=>t[e]),t)}(e,n)),(t=>P(e,n,t)),n,i)}};return e(n),t=>{if("object"==typeof t&&null!==t&&t._x_interceptor){let e=n.initialize.bind(n);n.initialize=(i,r,s)=>{let o=t.initialize(i,r,s);return n.initialValue=o,e(i,r,s)}}else n.initialValue=t;return n}}function P(t,e,n){if("string"==typeof e&&(e=e.split(".")),1!==e.length){if(0===e.length)throw error;return t[e[0]]||(t[e[0]]={}),P(t[e[0]],e.slice(1),n)}t[e[0]]=n}var M={};function j(t,e){M[t]=e}function B(t,e){return Object.entries(M).forEach((([n,i])=>{Object.defineProperty(t,`$${n}`,{get(){let[t,n]=it(e);return t={interceptor:R,...t},m(e,n),i(e,t)},enumerable:!1})})),t}function F(t,e,n,...i){try{return n(...i)}catch(n){$(n,t,e)}}function $(t,e,n){Object.assign(t,{el:e,expression:n}),console.warn(`Alpine Expression Error: ${t.message}\n\n${n?'Expression: "'+n+'"\n\n':""}`,e),setTimeout((()=>{throw t}),0)}var U=!0;function H(t,e,n={}){let i;return V(t,e)((t=>i=t),n),i}function V(...t){return z(...t)}var z=W;function W(t,e){let n={};B(n,t);let i=[n,...L(t)];if("function"==typeof e)return function(t,e){return(n=(()=>{}),{scope:i={},params:r=[]}={})=>{G(n,e.apply(k([i,...t]),r))}}(i,e);let r=function(t,e,n){let i=function(t,e){if(K[t])return K[t];let n=Object.getPrototypeOf((async function(){})).constructor,i=/^[\n\s]*if.*\(.*\)/.test(t)||/^(let|const)\s/.test(t)?`(() => { ${t} })()`:t;let r=(()=>{try{return new n(["__self","scope"],`with (scope) { __self.result = ${i} }; __self.finished = true; return __self.result;`)}catch(n){return $(n,e,t),Promise.resolve()}})();return K[t]=r,r}(e,n);return(r=(()=>{}),{scope:s={},params:o=[]}={})=>{i.result=void 0,i.finished=!1;let a=k([s,...t]);if("function"==typeof i){let t=i(i,a).catch((t=>$(t,n,e)));i.finished?(G(r,i.result,a,o,n),i.result=void 0):t.then((t=>{G(r,t,a,o,n)})).catch((t=>$(t,n,e))).finally((()=>i.result=void 0))}}}(i,e,t);return F.bind(null,t,e,r)}var K={};function G(t,e,n,i,r){if(U&&"function"==typeof e){let s=e.apply(n,i);s instanceof Promise?s.then((e=>G(t,e,n,i))).catch((t=>$(t,r,e))):t(s)}else t(e)}var q="x-";function X(t=""){return q+t}var Y={};function J(t,e){Y[t]=e}function Q(t,e,n){if(e=Array.from(e),t._x_virtualDirectives){let n=Object.entries(t._x_virtualDirectives).map((([t,e])=>({name:t,value:e}))),i=Z(n);n=n.map((t=>i.find((e=>e.name===t.name))?{name:`x-bind:${t.name}`,value:`"${t.value}"`}:t)),e=e.concat(n)}let i={},r=e.map(st(((t,e)=>i[t]=e))).filter(lt).map(function(t,e){return({name:n,value:i})=>{let r=n.match(ht()),s=n.match(/:([a-zA-Z0-9\-:]+)/),o=n.match(/\.[^.\]]+(?=[^\]]*$)/g)||[],a=e||t[n]||n;return{type:r?r[1]:null,value:s?s[1]:null,modifiers:o.map((t=>t.replace(".",""))),expression:i,original:a}}}(i,n)).sort(ut);return r.map((e=>function(t,e){let n=()=>{},i=Y[e.type]||n,[r,s]=it(t);!function(t,e,n){t._x_attributeCleanups||(t._x_attributeCleanups={}),t._x_attributeCleanups[e]||(t._x_attributeCleanups[e]=[]),t._x_attributeCleanups[e].push(n)}(t,e.original,s);let o=()=>{t._x_ignore||t._x_ignoreSelf||(i.inline&&i.inline(t,e,r),i=i.bind(i,t,e,r),tt?et.get(nt).push(i):i())};return o.runCleanups=s,o}(t,e)))}function Z(t){return Array.from(t).map(st()).filter((t=>!lt(t)))}var tt=!1,et=new Map,nt=Symbol();function it(t){let i=[],[r,s]=function(t){let i=()=>{};return[r=>{let s=e(r);return t._x_effects||(t._x_effects=new Set,t._x_runEffects=()=>{t._x_effects.forEach((t=>t()))}),t._x_effects.add(s),i=()=>{void 0!==s&&(t._x_effects.delete(s),n(s))},s},()=>{i()}]}(t);i.push(s);return[{Alpine:Xt,effect:r,cleanup:t=>i.push(t),evaluateLater:V.bind(V,t),evaluate:H.bind(H,t)},()=>i.forEach((t=>t()))]}var rt=(t,e)=>({name:n,value:i})=>(n.startsWith(t)&&(n=n.replace(t,e)),{name:n,value:i});function st(t=(()=>{})){return({name:e,value:n})=>{let{name:i,value:r}=ot.reduce(((t,e)=>e(t)),{name:e,value:n});return i!==e&&t(i,e),{name:i,value:r}}}var ot=[];function at(t){ot.push(t)}function lt({name:t}){return ht().test(t)}var ht=()=>new RegExp(`^${q}([^:^.]+)\\b`);var ct=["ignore","ref","data","id","bind","init","for","mask","model","modelable","transition","show","if","DEFAULT","teleport"];function ut(t,e){let n=-1===ct.indexOf(t.type)?"DEFAULT":t.type,i=-1===ct.indexOf(e.type)?"DEFAULT":e.type;return ct.indexOf(n)-ct.indexOf(i)}function ft(t,e,n={}){t.dispatchEvent(new CustomEvent(e,{detail:n,bubbles:!0,composed:!0,cancelable:!0}))}var dt=[],pt=!1;function gt(t=(()=>{})){return queueMicrotask((()=>{pt||setTimeout((()=>{mt()}))})),new Promise((e=>{dt.push((()=>{t(),e()}))}))}function mt(){for(pt=!1;dt.length;)dt.shift()()}function yt(t,e){if("function"==typeof ShadowRoot&&t instanceof ShadowRoot)return void Array.from(t.children).forEach((t=>yt(t,e)));let n=!1;if(e(t,(()=>n=!0)),n)return;let i=t.firstElementChild;for(;i;)yt(i,e),i=i.nextElementSibling}function vt(t,...e){console.warn(`Alpine Warning: ${t}`,...e)}var bt=[],_t=[];function Et(){return bt.map((t=>t()))}function wt(){return bt.concat(_t).map((t=>t()))}function St(t){bt.push(t)}function At(t){_t.push(t)}function xt(t,e=!1){return Tt(t,(t=>{if((e?wt():Et()).some((e=>t.matches(e))))return!0}))}function Tt(t,e){if(t){if(e(t))return t;if(t._x_teleportBack&&(t=t._x_teleportBack),t.parentElement)return Tt(t.parentElement,e)}}function Ct(t,e=yt){!function(t){tt=!0;let e=Symbol();nt=e,et.set(e,[]);let n=()=>{for(;et.get(e).length;)et.get(e).shift()();et.delete(e)};t(n),tt=!1,n()}((()=>{e(t,((t,e)=>{Q(t,t.attributes).forEach((t=>t())),t._x_ignore&&e()}))}))}function It(t,e){return Array.isArray(e)?Dt(t,e.join(" ")):"object"==typeof e&&null!==e?function(t,e){let n=t=>t.split(" ").filter(Boolean),i=Object.entries(e).flatMap((([t,e])=>!!e&&n(t))).filter(Boolean),r=Object.entries(e).flatMap((([t,e])=>!e&&n(t))).filter(Boolean),s=[],o=[];return r.forEach((e=>{t.classList.contains(e)&&(t.classList.remove(e),o.push(e))})),i.forEach((e=>{t.classList.contains(e)||(t.classList.add(e),s.push(e))})),()=>{o.forEach((e=>t.classList.add(e))),s.forEach((e=>t.classList.remove(e)))}}(t,e):"function"==typeof e?It(t,e()):Dt(t,e)}function Dt(t,e){return e=!0===e?e="":e||"",n=e.split(" ").filter((e=>!t.classList.contains(e))).filter(Boolean),t.classList.add(...n),()=>{t.classList.remove(...n)};var n}function Ot(t,e){return"object"==typeof e&&null!==e?function(t,e){let n={};return Object.entries(e).forEach((([e,i])=>{n[e]=t.style[e],e.startsWith("--")||(e=e.replace(/([a-z])([A-Z])/g,"$1-$2").toLowerCase()),t.style.setProperty(e,i)})),setTimeout((()=>{0===t.style.length&&t.removeAttribute("style")})),()=>{Ot(t,n)}}(t,e):function(t,e){let n=t.getAttribute("style",e);return t.setAttribute("style",e),()=>{t.setAttribute("style",n||"")}}(t,e)}function Lt(t,e=(()=>{})){let n=!1;return function(){n?e.apply(this,arguments):(n=!0,t.apply(this,arguments))}}function kt(t,e,n={}){t._x_transition||(t._x_transition={enter:{during:n,start:n,end:n},leave:{during:n,start:n,end:n},in(n=(()=>{}),i=(()=>{})){Rt(t,e,{during:this.enter.during,start:this.enter.start,end:this.enter.end},n,i)},out(n=(()=>{}),i=(()=>{})){Rt(t,e,{during:this.leave.during,start:this.leave.start,end:this.leave.end},n,i)}})}function Nt(t){let e=t.parentNode;if(e)return e._x_hidePromise?e:Nt(e)}function Rt(t,e,{during:n,start:i,end:r}={},s=(()=>{}),o=(()=>{})){if(t._x_transitioning&&t._x_transitioning.cancel(),0===Object.keys(n).length&&0===Object.keys(i).length&&0===Object.keys(r).length)return s(),void o();let a,l,h;!function(t,e){let n,i,r,s=Lt((()=>{A((()=>{n=!0,i||e.before(),r||(e.end(),mt()),e.after(),t.isConnected&&e.cleanup(),delete t._x_transitioning}))}));t._x_transitioning={beforeCancels:[],beforeCancel(t){this.beforeCancels.push(t)},cancel:Lt((function(){for(;this.beforeCancels.length;)this.beforeCancels.shift()();s()})),finish:s},A((()=>{e.start(),e.during()})),pt=!0,requestAnimationFrame((()=>{if(n)return;let s=1e3*Number(getComputedStyle(t).transitionDuration.replace(/,.*/,"").replace("s","")),o=1e3*Number(getComputedStyle(t).transitionDelay.replace(/,.*/,"").replace("s",""));0===s&&(s=1e3*Number(getComputedStyle(t).animationDuration.replace("s",""))),A((()=>{e.before()})),i=!0,requestAnimationFrame((()=>{n||(A((()=>{e.end()})),mt(),setTimeout(t._x_transitioning.finish,s+o),r=!0)}))}))}(t,{start(){a=e(t,i)},during(){l=e(t,n)},before:s,end(){a(),h=e(t,r)},after:o,cleanup(){l(),h()}})}function Pt(t,e,n){if(-1===t.indexOf(e))return n;const i=t[t.indexOf(e)+1];if(!i)return n;if("scale"===e&&isNaN(i))return n;if("duration"===e){let t=i.match(/([0-9]+)ms/);if(t)return t[1]}return"origin"===e&&["top","right","left","center","bottom"].includes(t[t.indexOf(e)+2])?[i,t[t.indexOf(e)+2]].join(" "):i}J("transition",((t,{value:e,modifiers:n,expression:i},{evaluate:r})=>{"function"==typeof i&&(i=r(i)),i?function(t,e,n){kt(t,It,""),{enter:e=>{t._x_transition.enter.during=e},"enter-start":e=>{t._x_transition.enter.start=e},"enter-end":e=>{t._x_transition.enter.end=e},leave:e=>{t._x_transition.leave.during=e},"leave-start":e=>{t._x_transition.leave.start=e},"leave-end":e=>{t._x_transition.leave.end=e}}[n](e)}(t,i,e):function(t,e,n){kt(t,Ot);let i=!e.includes("in")&&!e.includes("out")&&!n,r=i||e.includes("in")||["enter"].includes(n),s=i||e.includes("out")||["leave"].includes(n);e.includes("in")&&!i&&(e=e.filter(((t,n)=>n<e.indexOf("out"))));e.includes("out")&&!i&&(e=e.filter(((t,n)=>n>e.indexOf("out"))));let o=!e.includes("opacity")&&!e.includes("scale"),a=o||e.includes("opacity"),l=o||e.includes("scale"),h=a?0:1,c=l?Pt(e,"scale",95)/100:1,u=Pt(e,"delay",0),f=Pt(e,"origin","center"),d="opacity, transform",p=Pt(e,"duration",150)/1e3,g=Pt(e,"duration",75)/1e3,m="cubic-bezier(0.4, 0.0, 0.2, 1)";r&&(t._x_transition.enter.during={transformOrigin:f,transitionDelay:u,transitionProperty:d,transitionDuration:`${p}s`,transitionTimingFunction:m},t._x_transition.enter.start={opacity:h,transform:`scale(${c})`},t._x_transition.enter.end={opacity:1,transform:"scale(1)"});s&&(t._x_transition.leave.during={transformOrigin:f,transitionDelay:u,transitionProperty:d,transitionDuration:`${g}s`,transitionTimingFunction:m},t._x_transition.leave.start={opacity:1,transform:"scale(1)"},t._x_transition.leave.end={opacity:h,transform:`scale(${c})`})}(t,n,e)})),window.Element.prototype._x_toggleAndCascadeWithTransitions=function(t,e,n,i){const r="visible"===document.visibilityState?requestAnimationFrame:setTimeout;let s=()=>r(n);e?t._x_transition&&(t._x_transition.enter||t._x_transition.leave)?t._x_transition.enter&&(Object.entries(t._x_transition.enter.during).length||Object.entries(t._x_transition.enter.start).length||Object.entries(t._x_transition.enter.end).length)?t._x_transition.in(n):s():t._x_transition?t._x_transition.in(n):s():(t._x_hidePromise=t._x_transition?new Promise(((e,n)=>{t._x_transition.out((()=>{}),(()=>e(i))),t._x_transitioning.beforeCancel((()=>n({isFromCancelledTransition:!0})))})):Promise.resolve(i),queueMicrotask((()=>{let e=Nt(t);e?(e._x_hideChildren||(e._x_hideChildren=[]),e._x_hideChildren.push(t)):r((()=>{let e=t=>{let n=Promise.all([t._x_hidePromise,...(t._x_hideChildren||[]).map(e)]).then((([t])=>t()));return delete t._x_hidePromise,delete t._x_hideChildren,n};e(t).catch((t=>{if(!t.isFromCancelledTransition)throw t}))}))})))};var Mt=!1;function jt(t,e=(()=>{})){return(...n)=>Mt?e(...n):t(...n)}function Bt(e,n,i,r=[]){switch(e._x_bindings||(e._x_bindings=t({})),e._x_bindings[n]=i,n=r.includes("camel")?n.toLowerCase().replace(/-(\w)/g,((t,e)=>e.toUpperCase())):n){case"value":!function(t,e){if("radio"===t.type)void 0===t.attributes.value&&(t.value=e),window.fromModel&&(t.checked=Ft(t.value,e));else if("checkbox"===t.type)Number.isInteger(e)?t.value=e:Number.isInteger(e)||Array.isArray(e)||"boolean"==typeof e||[null,void 0].includes(e)?Array.isArray(e)?t.checked=e.some((e=>Ft(e,t.value))):t.checked=!!e:t.value=String(e);else if("SELECT"===t.tagName)!function(t,e){const n=[].concat(e).map((t=>t+""));Array.from(t.options).forEach((t=>{t.selected=n.includes(t.value)}))}(t,e);else{if(t.value===e)return;t.value=e}}(e,i);break;case"style":!function(t,e){t._x_undoAddedStyles&&t._x_undoAddedStyles();t._x_undoAddedStyles=Ot(t,e)}(e,i);break;case"class":!function(t,e){t._x_undoAddedClasses&&t._x_undoAddedClasses();t._x_undoAddedClasses=It(t,e)}(e,i);break;default:!function(t,e,n){[null,void 0,!1].includes(n)&&function(t){return!["aria-pressed","aria-checked","aria-expanded","aria-selected"].includes(t)}(e)?t.removeAttribute(e):($t(e)&&(n=e),function(t,e,n){t.getAttribute(e)!=n&&t.setAttribute(e,n)}(t,e,n))}(e,n,i)}}function Ft(t,e){return t==e}function $t(t){return["disabled","checked","required","readonly","hidden","open","selected","autofocus","itemscope","multiple","novalidate","allowfullscreen","allowpaymentrequest","formnovalidate","autoplay","controls","loop","muted","playsinline","default","ismap","reversed","async","defer","nomodule"].includes(t)}function Ut(t,e){var n;return function(){var i=this,r=arguments,s=function(){n=null,t.apply(i,r)};clearTimeout(n),n=setTimeout(s,e)}}function Ht(t,e){let n;return function(){let i=this,r=arguments;n||(t.apply(i,r),n=!0,setTimeout((()=>n=!1),e))}}var Vt={},zt=!1;var Wt={};function Kt(t,e,n){let i=[];for(;i.length;)i.pop()();let r=Object.entries(e).map((([t,e])=>({name:t,value:e}))),s=Z(r);r=r.map((t=>s.find((e=>e.name===t.name))?{name:`x-bind:${t.name}`,value:`"${t.value}"`}:t)),Q(t,r,n).map((t=>{i.push(t.runCleanups),t()}))}var Gt={};var qt={get reactive(){return t},get release(){return n},get effect(){return e},get raw(){return i},version:"3.10.3",flushAndStopDeferringMutations:function(){x=!1,C(T),T=[]},dontAutoEvaluateFunctions:function(t){let e=U;U=!1,t(),U=e},disableEffectScheduling:function(t){u=!1,t(),u=!0},setReactivityEngine:function(r){t=r.reactive,n=r.release,e=t=>r.effect(t,{scheduler:t=>{u?l(t):t()}}),i=r.raw},closestDataStack:L,skipDuringClone:jt,addRootSelector:St,addInitSelector:At,addScopeToNode:D,deferMutations:function(){x=!0},mapAttributes:at,evaluateLater:V,setEvaluator:function(t){z=t},mergeProxies:k,findClosest:Tt,closestRoot:xt,interceptor:R,transition:Rt,setStyles:Ot,mutateDom:A,directive:J,throttle:Ht,debounce:Ut,evaluate:H,initTree:Ct,nextTick:gt,prefixed:X,prefix:function(t){q=t},plugin:function(t){t(Xt)},magic:j,store:function(e,n){if(zt||(Vt=t(Vt),zt=!0),void 0===n)return Vt[e];Vt[e]=n,"object"==typeof n&&null!==n&&n.hasOwnProperty("init")&&"function"==typeof n.init&&Vt[e].init(),N(Vt[e])},start:function(){var t;document.body||vt("Unable to initialize. Trying to load Alpine before `<body>` is available. Did you forget to add `defer` in Alpine's `<script>` tag?"),ft(document,"alpine:init"),ft(document,"alpine:initializing"),_(),t=t=>Ct(t,yt),g.push(t),m((t=>{yt(t,(t=>y(t)))})),function(t){d.push(t)}(((t,e)=>{Q(t,e).forEach((t=>t()))})),Array.from(document.querySelectorAll(wt())).filter((t=>!xt(t.parentElement,!0))).forEach((t=>{Ct(t)})),ft(document,"alpine:initialized")},clone:function(t,i){i._x_dataStack||(i._x_dataStack=t._x_dataStack),Mt=!0,function(t){let i=e;f(((t,e)=>{let r=i(t);return n(r),()=>{}})),t(),f(i)}((()=>{!function(t){let e=!1;Ct(t,((t,n)=>{yt(t,((t,i)=>{if(e&&function(t){return Et().some((e=>t.matches(e)))}(t))return i();e=!0,n(t,i)}))}))}(i)})),Mt=!1},bound:function(t,e,n){if(t._x_bindings&&void 0!==t._x_bindings[e])return t._x_bindings[e];let i=t.getAttribute(e);return null===i?"function"==typeof n?n():n:$t(e)?!![e,"true"].includes(i):""===i||i},$data:I,data:function(t,e){Gt[t]=e},bind:function(t,e){let n="function"!=typeof e?()=>e:e;t instanceof Element?Kt(t,n()):Wt[t]=n}},Xt=qt;function Yt(t,e){const n=Object.create(null),i=t.split(",");for(let t=0;t<i.length;t++)n[i[t]]=!0;return e?t=>!!n[t.toLowerCase()]:t=>!!n[t]}var Jt,Qt=Object.freeze({}),Zt=(Object.freeze([]),Object.assign),te=Object.prototype.hasOwnProperty,ee=(t,e)=>te.call(t,e),ne=Array.isArray,ie=t=>"[object Map]"===ae(t),re=t=>"symbol"==typeof t,se=t=>null!==t&&"object"==typeof t,oe=Object.prototype.toString,ae=t=>oe.call(t),le=t=>ae(t).slice(8,-1),he=t=>"string"==typeof t&&"NaN"!==t&&"-"!==t[0]&&""+parseInt(t,10)===t,ce=t=>{const e=Object.create(null);return n=>e[n]||(e[n]=t(n))},ue=/-(\w)/g,fe=(ce((t=>t.replace(ue,((t,e)=>e?e.toUpperCase():"")))),/\B([A-Z])/g),de=(ce((t=>t.replace(fe,"-$1").toLowerCase())),ce((t=>t.charAt(0).toUpperCase()+t.slice(1)))),pe=(ce((t=>t?`on${de(t)}`:"")),(t,e)=>t!==e&&(t==t||e==e)),ge=new WeakMap,me=[],ye=Symbol("iterate"),ve=Symbol("Map key iterate");var be=0;function _e(t){const{deps:e}=t;if(e.length){for(let n=0;n<e.length;n++)e[n].delete(t);e.length=0}}var Ee=!0,we=[];function Se(){const t=we.pop();Ee=void 0===t||t}function Ae(t,e,n){if(!Ee||void 0===Jt)return;let i=ge.get(t);i||ge.set(t,i=new Map);let r=i.get(n);r||i.set(n,r=new Set),r.has(Jt)||(r.add(Jt),Jt.deps.push(r),Jt.options.onTrack&&Jt.options.onTrack({effect:Jt,target:t,type:e,key:n}))}function xe(t,e,n,i,r,s){const o=ge.get(t);if(!o)return;const a=new Set,l=t=>{t&&t.forEach((t=>{(t!==Jt||t.allowRecurse)&&a.add(t)}))};if("clear"===e)o.forEach(l);else if("length"===n&&ne(t))o.forEach(((t,e)=>{("length"===e||e>=i)&&l(t)}));else switch(void 0!==n&&l(o.get(n)),e){case"add":ne(t)?he(n)&&l(o.get("length")):(l(o.get(ye)),ie(t)&&l(o.get(ve)));break;case"delete":ne(t)||(l(o.get(ye)),ie(t)&&l(o.get(ve)));break;case"set":ie(t)&&l(o.get(ye))}a.forEach((o=>{o.options.onTrigger&&o.options.onTrigger({effect:o,target:t,key:n,type:e,newValue:i,oldValue:r,oldTarget:s}),o.options.scheduler?o.options.scheduler(o):o()}))}var Te=Yt("__proto__,__v_isRef,__isVue"),Ce=new Set(Object.getOwnPropertyNames(Symbol).map((t=>Symbol[t])).filter(re)),Ie=Ne(),De=Ne(!1,!0),Oe=Ne(!0),Le=Ne(!0,!0),ke={};function Ne(t=!1,e=!1){return function(n,i,r){if("__v_isReactive"===i)return!t;if("__v_isReadonly"===i)return t;if("__v_raw"===i&&r===(t?e?hn:ln:e?an:on).get(n))return n;const s=ne(n);if(!t&&s&&ee(ke,i))return Reflect.get(ke,i,r);const o=Reflect.get(n,i,r);if(re(i)?Ce.has(i):Te(i))return o;if(t||Ae(n,"get",i),e)return o;if(pn(o)){return!s||!he(i)?o.value:o}return se(o)?t?un(o):cn(o):o}}function Re(t=!1){return function(e,n,i,r){let s=e[n];if(!t&&(i=dn(i),s=dn(s),!ne(e)&&pn(s)&&!pn(i)))return s.value=i,!0;const o=ne(e)&&he(n)?Number(n)<e.length:ee(e,n),a=Reflect.set(e,n,i,r);return e===dn(r)&&(o?pe(i,s)&&xe(e,"set",n,i,s):xe(e,"add",n,i)),a}}["includes","indexOf","lastIndexOf"].forEach((t=>{const e=Array.prototype[t];ke[t]=function(...t){const n=dn(this);for(let t=0,e=this.length;t<e;t++)Ae(n,"get",t+"");const i=e.apply(n,t);return-1===i||!1===i?e.apply(n,t.map(dn)):i}})),["push","pop","shift","unshift","splice"].forEach((t=>{const e=Array.prototype[t];ke[t]=function(...t){we.push(Ee),Ee=!1;const n=e.apply(this,t);return Se(),n}}));var Pe={get:Ie,set:Re(),deleteProperty:function(t,e){const n=ee(t,e),i=t[e],r=Reflect.deleteProperty(t,e);return r&&n&&xe(t,"delete",e,void 0,i),r},has:function(t,e){const n=Reflect.has(t,e);return re(e)&&Ce.has(e)||Ae(t,"has",e),n},ownKeys:function(t){return Ae(t,"iterate",ne(t)?"length":ye),Reflect.ownKeys(t)}},Me={get:Oe,set:(t,e)=>(console.warn(`Set operation on key "${String(e)}" failed: target is readonly.`,t),!0),deleteProperty:(t,e)=>(console.warn(`Delete operation on key "${String(e)}" failed: target is readonly.`,t),!0)},je=(Zt({},Pe,{get:De,set:Re(!0)}),Zt({},Me,{get:Le}),t=>se(t)?cn(t):t),Be=t=>se(t)?un(t):t,Fe=t=>t,$e=t=>Reflect.getPrototypeOf(t);function Ue(t,e,n=!1,i=!1){const r=dn(t=t.__v_raw),s=dn(e);e!==s&&!n&&Ae(r,"get",e),!n&&Ae(r,"get",s);const{has:o}=$e(r),a=i?Fe:n?Be:je;return o.call(r,e)?a(t.get(e)):o.call(r,s)?a(t.get(s)):void(t!==r&&t.get(e))}function He(t,e=!1){const n=this.__v_raw,i=dn(n),r=dn(t);return t!==r&&!e&&Ae(i,"has",t),!e&&Ae(i,"has",r),t===r?n.has(t):n.has(t)||n.has(r)}function Ve(t,e=!1){return t=t.__v_raw,!e&&Ae(dn(t),"iterate",ye),Reflect.get(t,"size",t)}function ze(t){t=dn(t);const e=dn(this);return $e(e).has.call(e,t)||(e.add(t),xe(e,"add",t,t)),this}function We(t,e){e=dn(e);const n=dn(this),{has:i,get:r}=$e(n);let s=i.call(n,t);s?sn(n,i,t):(t=dn(t),s=i.call(n,t));const o=r.call(n,t);return n.set(t,e),s?pe(e,o)&&xe(n,"set",t,e,o):xe(n,"add",t,e),this}function Ke(t){const e=dn(this),{has:n,get:i}=$e(e);let r=n.call(e,t);r?sn(e,n,t):(t=dn(t),r=n.call(e,t));const s=i?i.call(e,t):void 0,o=e.delete(t);return r&&xe(e,"delete",t,void 0,s),o}function Ge(){const t=dn(this),e=0!==t.size,n=ie(t)?new Map(t):new Set(t),i=t.clear();return e&&xe(t,"clear",void 0,void 0,n),i}function qe(t,e){return function(n,i){const r=this,s=r.__v_raw,o=dn(s),a=e?Fe:t?Be:je;return!t&&Ae(o,"iterate",ye),s.forEach(((t,e)=>n.call(i,a(t),a(e),r)))}}function Xe(t,e,n){return function(...i){const r=this.__v_raw,s=dn(r),o=ie(s),a="entries"===t||t===Symbol.iterator&&o,l="keys"===t&&o,h=r[t](...i),c=n?Fe:e?Be:je;return!e&&Ae(s,"iterate",l?ve:ye),{next(){const{value:t,done:e}=h.next();return e?{value:t,done:e}:{value:a?[c(t[0]),c(t[1])]:c(t),done:e}},[Symbol.iterator](){return this}}}}function Ye(t){return function(...e){{const n=e[0]?`on key "${e[0]}" `:"";console.warn(`${de(t)} operation ${n}failed: target is readonly.`,dn(this))}return"delete"!==t&&this}}var Je={get(t){return Ue(this,t)},get size(){return Ve(this)},has:He,add:ze,set:We,delete:Ke,clear:Ge,forEach:qe(!1,!1)},Qe={get(t){return Ue(this,t,!1,!0)},get size(){return Ve(this)},has:He,add:ze,set:We,delete:Ke,clear:Ge,forEach:qe(!1,!0)},Ze={get(t){return Ue(this,t,!0)},get size(){return Ve(this,!0)},has(t){return He.call(this,t,!0)},add:Ye("add"),set:Ye("set"),delete:Ye("delete"),clear:Ye("clear"),forEach:qe(!0,!1)},tn={get(t){return Ue(this,t,!0,!0)},get size(){return Ve(this,!0)},has(t){return He.call(this,t,!0)},add:Ye("add"),set:Ye("set"),delete:Ye("delete"),clear:Ye("clear"),forEach:qe(!0,!0)};function en(t,e){const n=e?t?tn:Qe:t?Ze:Je;return(e,i,r)=>"__v_isReactive"===i?!t:"__v_isReadonly"===i?t:"__v_raw"===i?e:Reflect.get(ee(n,i)&&i in e?n:e,i,r)}["keys","values","entries",Symbol.iterator].forEach((t=>{Je[t]=Xe(t,!1,!1),Ze[t]=Xe(t,!0,!1),Qe[t]=Xe(t,!1,!0),tn[t]=Xe(t,!0,!0)}));var nn={get:en(!1,!1)},rn=(en(!1,!0),{get:en(!0,!1)});en(!0,!0);function sn(t,e,n){const i=dn(n);if(i!==n&&e.call(t,i)){const e=le(t);console.warn(`Reactive ${e} contains both the raw and reactive versions of the same object${"Map"===e?" as keys":""}, which can lead to inconsistencies. Avoid differentiating between the raw and reactive versions of an object and only use the reactive version if possible.`)}}var on=new WeakMap,an=new WeakMap,ln=new WeakMap,hn=new WeakMap;function cn(t){return t&&t.__v_isReadonly?t:fn(t,!1,Pe,nn,on)}function un(t){return fn(t,!0,Me,rn,ln)}function fn(t,e,n,i,r){if(!se(t))return console.warn(`value cannot be made reactive: ${String(t)}`),t;if(t.__v_raw&&(!e||!t.__v_isReactive))return t;const s=r.get(t);if(s)return s;const o=(a=t).__v_skip||!Object.isExtensible(a)?0:function(t){switch(t){case"Object":case"Array":return 1;case"Map":case"Set":case"WeakMap":case"WeakSet":return 2;default:return 0}}(le(a));var a;if(0===o)return t;const l=new Proxy(t,2===o?i:n);return r.set(t,l),l}function dn(t){return t&&dn(t.__v_raw)||t}function pn(t){return Boolean(t&&!0===t.__v_isRef)}j("nextTick",(()=>gt)),j("dispatch",(t=>ft.bind(ft,t))),j("watch",((t,{evaluateLater:e,effect:n})=>(i,r)=>{let s,o=e(i),a=!0,l=n((()=>o((t=>{JSON.stringify(t),a?s=t:queueMicrotask((()=>{r(t,s),s=t})),a=!1}))));t._x_effects.delete(l)})),j("store",(function(){return Vt})),j("data",(t=>I(t))),j("root",(t=>xt(t))),j("refs",(t=>(t._x_refs_proxy||(t._x_refs_proxy=k(function(t){let e=[],n=t;for(;n;)n._x_refs&&e.push(n._x_refs),n=n.parentNode;return e}(t))),t._x_refs_proxy)));var gn={};function mn(t){return gn[t]||(gn[t]=0),++gn[t]}function yn(t,e,n){j(e,(e=>vt(`You can't use [$${directiveName}] without first installing the "${t}" plugin here: https://alpinejs.dev/plugins/${n}`,e)))}j("id",(t=>(e,n=null)=>{let i=function(t,e){return Tt(t,(t=>{if(t._x_ids&&t._x_ids[e])return!0}))}(t,e),r=i?i._x_ids[e]:mn(e);return n?`${e}-${r}-${n}`:`${e}-${r}`})),j("el",(t=>t)),yn("Focus","focus","focus"),yn("Persist","persist","persist"),J("modelable",((t,{expression:e},{effect:n,evaluateLater:i})=>{let r=i(e),s=()=>{let t;return r((e=>t=e)),t},o=i(`${e} = __placeholder`),a=t=>o((()=>{}),{scope:{__placeholder:t}}),l=s();a(l),queueMicrotask((()=>{if(!t._x_model)return;t._x_removeModelListeners.default();let e=t._x_model.get,i=t._x_model.set;n((()=>a(e()))),n((()=>i(s())))}))})),J("teleport",((t,{expression:e},{cleanup:n})=>{"template"!==t.tagName.toLowerCase()&&vt("x-teleport can only be used on a <template> tag",t);let i=document.querySelector(e);i||vt(`Cannot find x-teleport element for selector: "${e}"`);let r=t.content.cloneNode(!0).firstElementChild;t._x_teleport=r,r._x_teleportBack=t,t._x_forwardEvents&&t._x_forwardEvents.forEach((e=>{r.addEventListener(e,(e=>{e.stopPropagation(),t.dispatchEvent(new e.constructor(e.type,e))}))})),D(r,{},t),A((()=>{i.appendChild(r),Ct(r),r._x_ignore=!0})),n((()=>r.remove()))}));var vn=()=>{};function bn(t,e,n,i){let r=t,s=t=>i(t),o={},a=(t,e)=>n=>e(t,n);if(n.includes("dot")&&(e=e.replace(/-/g,".")),n.includes("camel")&&(e=function(t){return t.toLowerCase().replace(/-(\w)/g,((t,e)=>e.toUpperCase()))}(e)),n.includes("passive")&&(o.passive=!0),n.includes("capture")&&(o.capture=!0),n.includes("window")&&(r=window),n.includes("document")&&(r=document),n.includes("prevent")&&(s=a(s,((t,e)=>{e.preventDefault(),t(e)}))),n.includes("stop")&&(s=a(s,((t,e)=>{e.stopPropagation(),t(e)}))),n.includes("self")&&(s=a(s,((e,n)=>{n.target===t&&e(n)}))),(n.includes("away")||n.includes("outside"))&&(r=document,s=a(s,((e,n)=>{t.contains(n.target)||!1!==n.target.isConnected&&(t.offsetWidth<1&&t.offsetHeight<1||!1!==t._x_isShown&&e(n))}))),n.includes("once")&&(s=a(s,((t,n)=>{t(n),r.removeEventListener(e,s,o)}))),s=a(s,((t,i)=>{(function(t){return["keydown","keyup"].includes(t)})(e)&&function(t,e){let n=e.filter((t=>!["window","document","prevent","stop","once"].includes(t)));if(n.includes("debounce")){let t=n.indexOf("debounce");n.splice(t,_n((n[t+1]||"invalid-wait").split("ms")[0])?2:1)}if(0===n.length)return!1;if(1===n.length&&En(t.key).includes(n[0]))return!1;const i=["ctrl","shift","alt","meta","cmd","super"].filter((t=>n.includes(t)));if(n=n.filter((t=>!i.includes(t))),i.length>0){if(i.filter((e=>("cmd"!==e&&"super"!==e||(e="meta"),t[`${e}Key`]))).length===i.length&&En(t.key).includes(n[0]))return!1}return!0}(i,n)||t(i)})),n.includes("debounce")){let t=n[n.indexOf("debounce")+1]||"invalid-wait",e=_n(t.split("ms")[0])?Number(t.split("ms")[0]):250;s=Ut(s,e)}if(n.includes("throttle")){let t=n[n.indexOf("throttle")+1]||"invalid-wait",e=_n(t.split("ms")[0])?Number(t.split("ms")[0]):250;s=Ht(s,e)}return r.addEventListener(e,s,o),()=>{r.removeEventListener(e,s,o)}}function _n(t){return!Array.isArray(t)&&!isNaN(t)}function En(t){if(!t)return[];t=t.replace(/([a-z])([A-Z])/g,"$1-$2").replace(/[_\s]/,"-").toLowerCase();let e={ctrl:"control",slash:"/",space:"-",spacebar:"-",cmd:"meta",esc:"escape",up:"arrow-up",down:"arrow-down",left:"arrow-left",right:"arrow-right",period:".",equal:"="};return e[t]=t,Object.keys(e).map((n=>{if(e[n]===t)return n})).filter((t=>t))}function wn(t){let e=t?parseFloat(t):null;return n=e,Array.isArray(n)||isNaN(n)?t:e;var n}function Sn(t,e,n,i){let r={};if(/^\[.*\]$/.test(t.item)&&Array.isArray(e)){t.item.replace("[","").replace("]","").split(",").map((t=>t.trim())).forEach(((t,n)=>{r[t]=e[n]}))}else if(/^\{.*\}$/.test(t.item)&&!Array.isArray(e)&&"object"==typeof e){t.item.replace("{","").replace("}","").split(",").map((t=>t.trim())).forEach((t=>{r[t]=e[t]}))}else r[t.item]=e;return t.index&&(r[t.index]=n),t.collection&&(r[t.collection]=i),r}function An(){}function xn(t,e,n){J(e,(i=>vt(`You can't use [x-${e}] without first installing the "${t}" plugin here: https://alpinejs.dev/plugins/${n}`,i)))}vn.inline=(t,{modifiers:e},{cleanup:n})=>{e.includes("self")?t._x_ignoreSelf=!0:t._x_ignore=!0,n((()=>{e.includes("self")?delete t._x_ignoreSelf:delete t._x_ignore}))},J("ignore",vn),J("effect",((t,{expression:e},{effect:n})=>n(V(t,e)))),J("model",((t,{modifiers:e,expression:n},{effect:i,cleanup:r})=>{let s=V(t,n),o=V(t,`${n} = rightSideOfExpression($event, ${n})`);var a="select"===t.tagName.toLowerCase()||["checkbox","radio"].includes(t.type)||e.includes("lazy")?"change":"input";let l=function(t,e,n){"radio"===t.type&&A((()=>{t.hasAttribute("name")||t.setAttribute("name",n)}));return(n,i)=>A((()=>{if(n instanceof CustomEvent&&void 0!==n.detail)return n.detail||n.target.value;if("checkbox"===t.type){if(Array.isArray(i)){let t=e.includes("number")?wn(n.target.value):n.target.value;return n.target.checked?i.concat([t]):i.filter((e=>!(e==t)))}return n.target.checked}if("select"===t.tagName.toLowerCase()&&t.multiple)return e.includes("number")?Array.from(n.target.selectedOptions).map((t=>wn(t.value||t.text))):Array.from(n.target.selectedOptions).map((t=>t.value||t.text));{let t=n.target.value;return e.includes("number")?wn(t):e.includes("trim")?t.trim():t}}))}(t,e,n),h=bn(t,a,e,(t=>{o((()=>{}),{scope:{$event:t,rightSideOfExpression:l}})}));t._x_removeModelListeners||(t._x_removeModelListeners={}),t._x_removeModelListeners.default=h,r((()=>t._x_removeModelListeners.default()));let c=V(t,`${n} = __placeholder`);t._x_model={get(){let t;return s((e=>t=e)),t},set(t){c((()=>{}),{scope:{__placeholder:t}})}},t._x_forceModelUpdate=()=>{s((e=>{void 0===e&&n.match(/\./)&&(e=""),window.fromModel=!0,A((()=>Bt(t,"value",e))),delete window.fromModel}))},i((()=>{e.includes("unintrusive")&&document.activeElement.isSameNode(t)||t._x_forceModelUpdate()}))})),J("cloak",(t=>queueMicrotask((()=>A((()=>t.removeAttribute(X("cloak")))))))),At((()=>`[${X("init")}]`)),J("init",jt(((t,{expression:e},{evaluate:n})=>"string"==typeof e?!!e.trim()&&n(e,{},!1):n(e,{},!1)))),J("text",((t,{expression:e},{effect:n,evaluateLater:i})=>{let r=i(e);n((()=>{r((e=>{A((()=>{t.textContent=e}))}))}))})),J("html",((t,{expression:e},{effect:n,evaluateLater:i})=>{let r=i(e);n((()=>{r((e=>{A((()=>{t.innerHTML=e,t._x_ignoreSelf=!0,Ct(t),delete t._x_ignoreSelf}))}))}))})),at(rt(":",X("bind:"))),J("bind",((t,{value:e,modifiers:n,expression:i,original:r},{effect:s})=>{if(!e){let e={};return o=e,Object.entries(Wt).forEach((([t,e])=>{Object.defineProperty(o,t,{get:()=>(...t)=>e(...t)})})),void V(t,i)((e=>{Kt(t,e,r)}),{scope:e})}var o;if("key"===e)return function(t,e){t._x_keyExpression=e}(t,i);let a=V(t,i);s((()=>a((r=>{void 0===r&&i.match(/\./)&&(r=""),A((()=>Bt(t,e,r,n)))}))))})),St((()=>`[${X("data")}]`)),J("data",jt(((e,{expression:n},{cleanup:i})=>{n=""===n?"{}":n;let r={};B(r,e);let s={};var o,a;o=s,a=r,Object.entries(Gt).forEach((([t,e])=>{Object.defineProperty(o,t,{get:()=>(...t)=>e.bind(a)(...t),enumerable:!1})}));let l=H(e,n,{scope:s});void 0===l&&(l={}),B(l,e);let h=t(l);N(h);let c=D(e,h);h.init&&H(e,h.init),i((()=>{h.destroy&&H(e,h.destroy),c()}))}))),J("show",((t,{modifiers:e,expression:n},{effect:i})=>{let r=V(t,n);t._x_doHide||(t._x_doHide=()=>{A((()=>{t.style.setProperty("display","none",e.includes("important")?"important":void 0)}))}),t._x_doShow||(t._x_doShow=()=>{A((()=>{1===t.style.length&&"none"===t.style.display?t.removeAttribute("style"):t.style.removeProperty("display")}))});let s,o=()=>{t._x_doHide(),t._x_isShown=!1},a=()=>{t._x_doShow(),t._x_isShown=!0},l=()=>setTimeout(a),h=Lt((t=>t?a():o()),(e=>{"function"==typeof t._x_toggleAndCascadeWithTransitions?t._x_toggleAndCascadeWithTransitions(t,e,a,o):e?l():o()})),c=!0;i((()=>r((t=>{(c||t!==s)&&(e.includes("immediate")&&(t?l():o()),h(t),s=t,c=!1)}))))})),J("for",((e,{expression:n},{effect:i,cleanup:r})=>{let s=function(t){let e=/,([^,\}\]]*)(?:,([^,\}\]]*))?$/,n=/^\s*\(|\)\s*$/g,i=/([\s\S]*?)\s+(?:in|of)\s+([\s\S]*)/,r=t.match(i);if(!r)return;let s={};s.items=r[2].trim();let o=r[1].replace(n,"").trim(),a=o.match(e);a?(s.item=o.replace(e,"").trim(),s.index=a[1].trim(),a[2]&&(s.collection=a[2].trim())):s.item=o;return s}(n),o=V(e,s.items),a=V(e,e._x_keyExpression||"index");e._x_prevKeys=[],e._x_lookup={},i((()=>function(e,n,i,r){let s=t=>"object"==typeof t&&!Array.isArray(t),o=e;i((i=>{var a;a=i,!Array.isArray(a)&&!isNaN(a)&&i>=0&&(i=Array.from(Array(i).keys(),(t=>t+1))),void 0===i&&(i=[]);let l=e._x_lookup,c=e._x_prevKeys,u=[],f=[];if(s(i))i=Object.entries(i).map((([t,e])=>{let s=Sn(n,e,t,i);r((t=>f.push(t)),{scope:{index:t,...s}}),u.push(s)}));else for(let t=0;t<i.length;t++){let e=Sn(n,i[t],t,i);r((t=>f.push(t)),{scope:{index:t,...e}}),u.push(e)}let d=[],p=[],g=[],m=[];for(let t=0;t<c.length;t++){let e=c[t];-1===f.indexOf(e)&&g.push(e)}c=c.filter((t=>!g.includes(t)));let y="template";for(let t=0;t<f.length;t++){let e=f[t],n=c.indexOf(e);if(-1===n)c.splice(t,0,e),d.push([y,t]);else if(n!==t){let e=c.splice(t,1)[0],i=c.splice(n-1,1)[0];c.splice(t,0,i),c.splice(n,0,e),p.push([e,i])}else m.push(e);y=e}for(let t=0;t<g.length;t++){let e=g[t];l[e]._x_effects&&l[e]._x_effects.forEach(h),l[e].remove(),l[e]=null,delete l[e]}for(let t=0;t<p.length;t++){let[e,n]=p[t],i=l[e],r=l[n],s=document.createElement("div");A((()=>{r.after(s),i.after(r),r._x_currentIfEl&&r.after(r._x_currentIfEl),s.before(i),i._x_currentIfEl&&i.after(i._x_currentIfEl),s.remove()})),O(r,u[f.indexOf(n)])}for(let e=0;e<d.length;e++){let[n,i]=d[e],r="template"===n?o:l[n];r._x_currentIfEl&&(r=r._x_currentIfEl);let s=u[i],a=f[i],h=document.importNode(o.content,!0).firstElementChild;D(h,t(s),o),A((()=>{r.after(h),Ct(h)})),"object"==typeof a&&vt("x-for key cannot be an object, it must be a string or an integer",o),l[a]=h}for(let t=0;t<m.length;t++)O(l[m[t]],u[f.indexOf(m[t])]);o._x_prevKeys=f}))}(e,s,o,a))),r((()=>{Object.values(e._x_lookup).forEach((t=>t.remove())),delete e._x_prevKeys,delete e._x_lookup}))})),An.inline=(t,{expression:e},{cleanup:n})=>{let i=xt(t);i._x_refs||(i._x_refs={}),i._x_refs[e]=t,n((()=>delete i._x_refs[e]))},J("ref",An),J("if",((t,{expression:e},{effect:n,cleanup:i})=>{let r=V(t,e);n((()=>r((e=>{e?(()=>{if(t._x_currentIfEl)return t._x_currentIfEl;let e=t.content.cloneNode(!0).firstElementChild;D(e,{},t),A((()=>{t.after(e),Ct(e)})),t._x_currentIfEl=e,t._x_undoIf=()=>{yt(e,(t=>{t._x_effects&&t._x_effects.forEach(h)})),e.remove(),delete t._x_currentIfEl}})():t._x_undoIf&&(t._x_undoIf(),delete t._x_undoIf)})))),i((()=>t._x_undoIf&&t._x_undoIf()))})),J("id",((t,{expression:e},{evaluate:n})=>{n(e).forEach((e=>function(t,e){t._x_ids||(t._x_ids={}),t._x_ids[e]||(t._x_ids[e]=mn(e))}(t,e)))})),at(rt("@",X("on:"))),J("on",jt(((t,{value:e,modifiers:n,expression:i},{cleanup:r})=>{let s=i?V(t,i):()=>{};"template"===t.tagName.toLowerCase()&&(t._x_forwardEvents||(t._x_forwardEvents=[]),t._x_forwardEvents.includes(e)||t._x_forwardEvents.push(e));let o=bn(t,e,n,(t=>{s((()=>{}),{scope:{$event:t},params:[t]})}));r((()=>o()))}))),xn("Collapse","collapse","collapse"),xn("Intersect","intersect","intersect"),xn("Focus","trap","focus"),xn("Mask","mask","mask"),Xt.setEvaluator(W),Xt.setReactivityEngine({reactive:cn,effect:function(t,e=Qt){(function(t){return t&&!0===t._isEffect})(t)&&(t=t.raw);const n=function(t,e){const n=function(){if(!n.active)return t();if(!me.includes(n)){_e(n);try{return we.push(Ee),Ee=!0,me.push(n),Jt=n,t()}finally{me.pop(),Se(),Jt=me[me.length-1]}}};return n.id=be++,n.allowRecurse=!!e.allowRecurse,n._isEffect=!0,n.active=!0,n.raw=t,n.deps=[],n.options=e,n}(t,e);return e.lazy||n(),n},release:function(t){t.active&&(_e(t),t.options.onStop&&t.options.onStop(),t.active=!1)},raw:dn});var Tn=Xt;
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const Cn=function(t){const e=[];let n=0;for(let i=0;i<t.length;i++){let r=t.charCodeAt(i);r<128?e[n++]=r:r<2048?(e[n++]=r>>6|192,e[n++]=63&r|128):55296==(64512&r)&&i+1<t.length&&56320==(64512&t.charCodeAt(i+1))?(r=65536+((1023&r)<<10)+(1023&t.charCodeAt(++i)),e[n++]=r>>18|240,e[n++]=r>>12&63|128,e[n++]=r>>6&63|128,e[n++]=63&r|128):(e[n++]=r>>12|224,e[n++]=r>>6&63|128,e[n++]=63&r|128)}return e},In={byteToCharMap_:null,charToByteMap_:null,byteToCharMapWebSafe_:null,charToByteMapWebSafe_:null,ENCODED_VALS_BASE:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",get ENCODED_VALS(){return this.ENCODED_VALS_BASE+"+/="},get ENCODED_VALS_WEBSAFE(){return this.ENCODED_VALS_BASE+"-_."},HAS_NATIVE_SUPPORT:"function"==typeof atob,encodeByteArray(t,e){if(!Array.isArray(t))throw Error("encodeByteArray takes an array as a parameter");this.init_();const n=e?this.byteToCharMapWebSafe_:this.byteToCharMap_,i=[];for(let e=0;e<t.length;e+=3){const r=t[e],s=e+1<t.length,o=s?t[e+1]:0,a=e+2<t.length,l=a?t[e+2]:0,h=r>>2,c=(3&r)<<4|o>>4;let u=(15&o)<<2|l>>6,f=63&l;a||(f=64,s||(u=64)),i.push(n[h],n[c],n[u],n[f])}return i.join("")},encodeString(t,e){return this.HAS_NATIVE_SUPPORT&&!e?btoa(t):this.encodeByteArray(Cn(t),e)},decodeString(t,e){return this.HAS_NATIVE_SUPPORT&&!e?atob(t):function(t){const e=[];let n=0,i=0;for(;n<t.length;){const r=t[n++];if(r<128)e[i++]=String.fromCharCode(r);else if(r>191&&r<224){const s=t[n++];e[i++]=String.fromCharCode((31&r)<<6|63&s)}else if(r>239&&r<365){const s=((7&r)<<18|(63&t[n++])<<12|(63&t[n++])<<6|63&t[n++])-65536;e[i++]=String.fromCharCode(55296+(s>>10)),e[i++]=String.fromCharCode(56320+(1023&s))}else{const s=t[n++],o=t[n++];e[i++]=String.fromCharCode((15&r)<<12|(63&s)<<6|63&o)}}return e.join("")}(this.decodeStringToByteArray(t,e))},decodeStringToByteArray(t,e){this.init_();const n=e?this.charToByteMapWebSafe_:this.charToByteMap_,i=[];for(let e=0;e<t.length;){const r=n[t.charAt(e++)],s=e<t.length?n[t.charAt(e)]:0;++e;const o=e<t.length?n[t.charAt(e)]:64;++e;const a=e<t.length?n[t.charAt(e)]:64;if(++e,null==r||null==s||null==o||null==a)throw Error();const l=r<<2|s>>4;if(i.push(l),64!==o){const t=s<<4&240|o>>2;if(i.push(t),64!==a){const t=o<<6&192|a;i.push(t)}}}return i},init_(){if(!this.byteToCharMap_){this.byteToCharMap_={},this.charToByteMap_={},this.byteToCharMapWebSafe_={},this.charToByteMapWebSafe_={};for(let t=0;t<this.ENCODED_VALS.length;t++)this.byteToCharMap_[t]=this.ENCODED_VALS.charAt(t),this.charToByteMap_[this.byteToCharMap_[t]]=t,this.byteToCharMapWebSafe_[t]=this.ENCODED_VALS_WEBSAFE.charAt(t),this.charToByteMapWebSafe_[this.byteToCharMapWebSafe_[t]]=t,t>=this.ENCODED_VALS_BASE.length&&(this.charToByteMap_[this.ENCODED_VALS_WEBSAFE.charAt(t)]=t,this.charToByteMapWebSafe_[this.ENCODED_VALS.charAt(t)]=t)}}},Dn=function(t){return function(t){const e=Cn(t);return In.encodeByteArray(e,!0)}(t).replace(/\./g,"")};
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class On{constructor(){this.reject=()=>{},this.resolve=()=>{},this.promise=new Promise(((t,e)=>{this.resolve=t,this.reject=e}))}wrapCallback(t){return(e,n)=>{e?this.reject(e):this.resolve(n),"function"==typeof t&&(this.promise.catch((()=>{})),1===t.length?t(e):t(e,n))}}}class Ln extends Error{constructor(t,e,n){super(e),this.code=t,this.customData=n,this.name="FirebaseError",Object.setPrototypeOf(this,Ln.prototype),Error.captureStackTrace&&Error.captureStackTrace(this,kn.prototype.create)}}class kn{constructor(t,e,n){this.service=t,this.serviceName=e,this.errors=n}create(t,...e){const n=e[0]||{},i=`${this.service}/${t}`,r=this.errors[t],s=r?function(t,e){return t.replace(Nn,((t,n)=>{const i=e[n];return null!=i?String(i):`<${n}?>`}))}(r,n):"Error",o=`${this.serviceName}: ${s} (${i}).`;return new Ln(i,o,n)}}const Nn=/\{\$([^}]+)}/g;function Rn(t,e){if(t===e)return!0;const n=Object.keys(t),i=Object.keys(e);for(const r of n){if(!i.includes(r))return!1;const n=t[r],s=e[r];if(Pn(n)&&Pn(s)){if(!Rn(n,s))return!1}else if(n!==s)return!1}for(const t of i)if(!n.includes(t))return!1;return!0}function Pn(t){return null!==t&&"object"==typeof t}class Mn{constructor(t,e,n){this.name=t,this.instanceFactory=e,this.type=n,this.multipleInstances=!1,this.serviceProps={},this.instantiationMode="LAZY",this.onInstanceCreated=null}setInstantiationMode(t){return this.instantiationMode=t,this}setMultipleInstances(t){return this.multipleInstances=t,this}setServiceProps(t){return this.serviceProps=t,this}setInstanceCreatedCallback(t){return this.onInstanceCreated=t,this}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class jn{constructor(t,e){this.name=t,this.container=e,this.component=null,this.instances=new Map,this.instancesDeferred=new Map,this.instancesOptions=new Map,this.onInitCallbacks=new Map}get(t){const e=this.normalizeInstanceIdentifier(t);if(!this.instancesDeferred.has(e)){const t=new On;if(this.instancesDeferred.set(e,t),this.isInitialized(e)||this.shouldAutoInitialize())try{const n=this.getOrInitializeService({instanceIdentifier:e});n&&t.resolve(n)}catch(t){}}return this.instancesDeferred.get(e).promise}getImmediate(t){var e;const n=this.normalizeInstanceIdentifier(null==t?void 0:t.identifier),i=null!==(e=null==t?void 0:t.optional)&&void 0!==e&&e;if(!this.isInitialized(n)&&!this.shouldAutoInitialize()){if(i)return null;throw Error(`Service ${this.name} is not available`)}try{return this.getOrInitializeService({instanceIdentifier:n})}catch(t){if(i)return null;throw t}}getComponent(){return this.component}setComponent(t){if(t.name!==this.name)throw Error(`Mismatching Component ${t.name} for Provider ${this.name}.`);if(this.component)throw Error(`Component for ${this.name} has already been provided`);if(this.component=t,this.shouldAutoInitialize()){if(function(t){return"EAGER"===t.instantiationMode}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */(t))try{this.getOrInitializeService({instanceIdentifier:"[DEFAULT]"})}catch(t){}for(const[t,e]of this.instancesDeferred.entries()){const n=this.normalizeInstanceIdentifier(t);try{const t=this.getOrInitializeService({instanceIdentifier:n});e.resolve(t)}catch(t){}}}}clearInstance(t="[DEFAULT]"){this.instancesDeferred.delete(t),this.instancesOptions.delete(t),this.instances.delete(t)}async delete(){const t=Array.from(this.instances.values());await Promise.all([...t.filter((t=>"INTERNAL"in t)).map((t=>t.INTERNAL.delete())),...t.filter((t=>"_delete"in t)).map((t=>t._delete()))])}isComponentSet(){return null!=this.component}isInitialized(t="[DEFAULT]"){return this.instances.has(t)}getOptions(t="[DEFAULT]"){return this.instancesOptions.get(t)||{}}initialize(t={}){const{options:e={}}=t,n=this.normalizeInstanceIdentifier(t.instanceIdentifier);if(this.isInitialized(n))throw Error(`${this.name}(${n}) has already been initialized`);if(!this.isComponentSet())throw Error(`Component ${this.name} has not been registered yet`);const i=this.getOrInitializeService({instanceIdentifier:n,options:e});for(const[t,e]of this.instancesDeferred.entries()){n===this.normalizeInstanceIdentifier(t)&&e.resolve(i)}return i}onInit(t,e){var n;const i=this.normalizeInstanceIdentifier(e),r=null!==(n=this.onInitCallbacks.get(i))&&void 0!==n?n:new Set;r.add(t),this.onInitCallbacks.set(i,r);const s=this.instances.get(i);return s&&t(s,i),()=>{r.delete(t)}}invokeOnInitCallbacks(t,e){const n=this.onInitCallbacks.get(e);if(n)for(const i of n)try{i(t,e)}catch(t){}}getOrInitializeService({instanceIdentifier:t,options:e={}}){let n=this.instances.get(t);if(!n&&this.component&&(n=this.component.instanceFactory(this.container,{instanceIdentifier:(i=t,"[DEFAULT]"===i?void 0:i),options:e}),this.instances.set(t,n),this.instancesOptions.set(t,e),this.invokeOnInitCallbacks(n,t),this.component.onInstanceCreated))try{this.component.onInstanceCreated(this.container,t,n)}catch(t){}var i;return n||null}normalizeInstanceIdentifier(t="[DEFAULT]"){return this.component?this.component.multipleInstances?t:"[DEFAULT]":t}shouldAutoInitialize(){return!!this.component&&"EXPLICIT"!==this.component.instantiationMode}}class Bn{constructor(t){this.name=t,this.providers=new Map}addComponent(t){const e=this.getProvider(t.name);if(e.isComponentSet())throw new Error(`Component ${t.name} has already been registered with ${this.name}`);e.setComponent(t)}addOrOverwriteComponent(t){this.getProvider(t.name).isComponentSet()&&this.providers.delete(t.name),this.addComponent(t)}getProvider(t){if(this.providers.has(t))return this.providers.get(t);const e=new jn(t,this);return this.providers.set(t,e),e}getProviders(){return Array.from(this.providers.values())}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Fn=[];var $n,Un;(Un=$n||($n={}))[Un.DEBUG=0]="DEBUG",Un[Un.VERBOSE=1]="VERBOSE",Un[Un.INFO=2]="INFO",Un[Un.WARN=3]="WARN",Un[Un.ERROR=4]="ERROR",Un[Un.SILENT=5]="SILENT";const Hn={debug:$n.DEBUG,verbose:$n.VERBOSE,info:$n.INFO,warn:$n.WARN,error:$n.ERROR,silent:$n.SILENT},Vn=$n.INFO,zn={[$n.DEBUG]:"log",[$n.VERBOSE]:"log",[$n.INFO]:"info",[$n.WARN]:"warn",[$n.ERROR]:"error"},Wn=(t,e,...n)=>{if(e<t.logLevel)return;const i=(new Date).toISOString(),r=zn[e];if(!r)throw new Error(`Attempted to log a message with an invalid logType (value: ${e})`);console[r](`[${i}]  ${t.name}:`,...n)};let Kn,Gn;const qn=new WeakMap,Xn=new WeakMap,Yn=new WeakMap,Jn=new WeakMap,Qn=new WeakMap;let Zn={get(t,e,n){if(t instanceof IDBTransaction){if("done"===e)return Xn.get(t);if("objectStoreNames"===e)return t.objectStoreNames||Yn.get(t);if("store"===e)return n.objectStoreNames[1]?void 0:n.objectStore(n.objectStoreNames[0])}return ni(t[e])},set:(t,e,n)=>(t[e]=n,!0),has:(t,e)=>t instanceof IDBTransaction&&("done"===e||"store"===e)||e in t};function ti(t){return t!==IDBDatabase.prototype.transaction||"objectStoreNames"in IDBTransaction.prototype?(Gn||(Gn=[IDBCursor.prototype.advance,IDBCursor.prototype.continue,IDBCursor.prototype.continuePrimaryKey])).includes(t)?function(...e){return t.apply(ii(this),e),ni(qn.get(this))}:function(...e){return ni(t.apply(ii(this),e))}:function(e,...n){const i=t.call(ii(this),e,...n);return Yn.set(i,e.sort?e.sort():[e]),ni(i)}}function ei(t){return"function"==typeof t?ti(t):(t instanceof IDBTransaction&&function(t){if(Xn.has(t))return;const e=new Promise(((e,n)=>{const i=()=>{t.removeEventListener("complete",r),t.removeEventListener("error",s),t.removeEventListener("abort",s)},r=()=>{e(),i()},s=()=>{n(t.error||new DOMException("AbortError","AbortError")),i()};t.addEventListener("complete",r),t.addEventListener("error",s),t.addEventListener("abort",s)}));Xn.set(t,e)}(t),e=t,(Kn||(Kn=[IDBDatabase,IDBObjectStore,IDBIndex,IDBCursor,IDBTransaction])).some((t=>e instanceof t))?new Proxy(t,Zn):t);var e}function ni(t){if(t instanceof IDBRequest)return function(t){const e=new Promise(((e,n)=>{const i=()=>{t.removeEventListener("success",r),t.removeEventListener("error",s)},r=()=>{e(ni(t.result)),i()},s=()=>{n(t.error),i()};t.addEventListener("success",r),t.addEventListener("error",s)}));return e.then((e=>{e instanceof IDBCursor&&qn.set(e,t)})).catch((()=>{})),Qn.set(e,t),e}(t);if(Jn.has(t))return Jn.get(t);const e=ei(t);return e!==t&&(Jn.set(t,e),Qn.set(e,t)),e}const ii=t=>Qn.get(t);const ri=["get","getKey","getAll","getAllKeys","count"],si=["put","add","delete","clear"],oi=new Map;function ai(t,e){if(!(t instanceof IDBDatabase)||e in t||"string"!=typeof e)return;if(oi.get(e))return oi.get(e);const n=e.replace(/FromIndex$/,""),i=e!==n,r=si.includes(n);if(!(n in(i?IDBIndex:IDBObjectStore).prototype)||!r&&!ri.includes(n))return;const s=async function(t,...e){const s=this.transaction(t,r?"readwrite":"readonly");let o=s.store;return i&&(o=o.index(e.shift())),(await Promise.all([o[n](...e),r&&s.done]))[0]};return oi.set(e,s),s}Zn=(t=>({...t,get:(e,n,i)=>ai(e,n)||t.get(e,n,i),has:(e,n)=>!!ai(e,n)||t.has(e,n)}))(Zn);
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class li{constructor(t){this.container=t}getPlatformInfoString(){return this.container.getProviders().map((t=>{if(function(t){const e=t.getComponent();return"VERSION"===(null==e?void 0:e.type)}(t)){const e=t.getImmediate();return`${e.library}/${e.version}`}return null})).filter((t=>t)).join(" ")}}const hi="https://www.gstatic.com/firebasejs/9.9.3/firebase-app.js",ci=new class{constructor(t){this.name=t,this._logLevel=Vn,this._logHandler=Wn,this._userLogHandler=null,Fn.push(this)}get logLevel(){return this._logLevel}set logLevel(t){if(!(t in $n))throw new TypeError(`Invalid value "${t}" assigned to \`logLevel\``);this._logLevel=t}setLogLevel(t){this._logLevel="string"==typeof t?Hn[t]:t}get logHandler(){return this._logHandler}set logHandler(t){if("function"!=typeof t)throw new TypeError("Value assigned to `logHandler` must be a function");this._logHandler=t}get userLogHandler(){return this._userLogHandler}set userLogHandler(t){this._userLogHandler=t}debug(...t){this._userLogHandler&&this._userLogHandler(this,$n.DEBUG,...t),this._logHandler(this,$n.DEBUG,...t)}log(...t){this._userLogHandler&&this._userLogHandler(this,$n.VERBOSE,...t),this._logHandler(this,$n.VERBOSE,...t)}info(...t){this._userLogHandler&&this._userLogHandler(this,$n.INFO,...t),this._logHandler(this,$n.INFO,...t)}warn(...t){this._userLogHandler&&this._userLogHandler(this,$n.WARN,...t),this._logHandler(this,$n.WARN,...t)}error(...t){this._userLogHandler&&this._userLogHandler(this,$n.ERROR,...t),this._logHandler(this,$n.ERROR,...t)}}("https://www.gstatic.com/firebasejs/9.9.3/firebase-app.js"),ui={[hi]:"fire-core","@firebase/app-compat":"fire-core-compat","@firebase/analytics":"fire-analytics","@firebase/analytics-compat":"fire-analytics-compat","@firebase/app-check":"fire-app-check","@firebase/app-check-compat":"fire-app-check-compat","@firebase/auth":"fire-auth","@firebase/auth-compat":"fire-auth-compat","@firebase/database":"fire-rtdb","@firebase/database-compat":"fire-rtdb-compat","@firebase/functions":"fire-fn","@firebase/functions-compat":"fire-fn-compat","@firebase/installations":"fire-iid","@firebase/installations-compat":"fire-iid-compat","@firebase/messaging":"fire-fcm","@firebase/messaging-compat":"fire-fcm-compat","@firebase/performance":"fire-perf","@firebase/performance-compat":"fire-perf-compat","@firebase/remote-config":"fire-rc","@firebase/remote-config-compat":"fire-rc-compat","@firebase/storage":"fire-gcs","@firebase/storage-compat":"fire-gcs-compat","@firebase/firestore":"fire-fst","@firebase/firestore-compat":"fire-fst-compat","fire-js":"fire-js",firebase:"fire-js-all"},fi=new Map,di=new Map;function pi(t,e){try{t.container.addComponent(e)}catch(n){ci.debug(`Component ${e.name} failed to register with FirebaseApp ${t.name}`,n)}}function gi(t){const e=t.name;if(di.has(e))return ci.debug(`There were multiple attempts to register component ${e}.`),!1;di.set(e,t);for(const e of fi.values())pi(e,t);return!0}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const mi=new kn("app","Firebase",{"no-app":"No Firebase App '{$appName}' has been created - call Firebase App.initializeApp()","bad-app-name":"Illegal App name: '{$appName}","duplicate-app":"Firebase App named '{$appName}' already exists with different options or config","app-deleted":"Firebase App named '{$appName}' already deleted","invalid-app-argument":"firebase.{$appName}() takes either no argument or a Firebase App instance.","invalid-log-argument":"First argument to `onLog` must be null or a function.","idb-open":"Error thrown when opening IndexedDB. Original error: {$originalErrorMessage}.","idb-get":"Error thrown when reading from IndexedDB. Original error: {$originalErrorMessage}.","idb-set":"Error thrown when writing to IndexedDB. Original error: {$originalErrorMessage}.","idb-delete":"Error thrown when deleting from IndexedDB. Original error: {$originalErrorMessage}."});
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class yi{constructor(t,e,n){this._isDeleted=!1,this._options=Object.assign({},t),this._config=Object.assign({},e),this._name=e.name,this._automaticDataCollectionEnabled=e.automaticDataCollectionEnabled,this._container=n,this.container.addComponent(new Mn("app",(()=>this),"PUBLIC"))}get automaticDataCollectionEnabled(){return this.checkDestroyed(),this._automaticDataCollectionEnabled}set automaticDataCollectionEnabled(t){this.checkDestroyed(),this._automaticDataCollectionEnabled=t}get name(){return this.checkDestroyed(),this._name}get options(){return this.checkDestroyed(),this._options}get config(){return this.checkDestroyed(),this._config}get container(){return this._container}get isDeleted(){return this._isDeleted}set isDeleted(t){this._isDeleted=t}checkDestroyed(){if(this.isDeleted)throw mi.create("app-deleted",{appName:this._name})}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function vi(t,e,n){var i;let r=null!==(i=ui[t])&&void 0!==i?i:t;n&&(r+=`-${n}`);const s=r.match(/\s|\//),o=e.match(/\s|\//);if(s||o){const t=[`Unable to register library "${r}" with version "${e}":`];return s&&t.push(`library name "${r}" contains illegal characters (whitespace or "/")`),s&&o&&t.push("and"),o&&t.push(`version name "${e}" contains illegal characters (whitespace or "/")`),void ci.warn(t.join(" "))}gi(new Mn(`${r}-version`,(()=>({library:r,version:e})),"VERSION"))}let bi=null;function _i(){return bi||(bi=function(t,e,{blocked:n,upgrade:i,blocking:r,terminated:s}={}){const o=indexedDB.open(t,e),a=ni(o);return i&&o.addEventListener("upgradeneeded",(t=>{i(ni(o.result),t.oldVersion,t.newVersion,ni(o.transaction))})),n&&o.addEventListener("blocked",(()=>n())),a.then((t=>{s&&t.addEventListener("close",(()=>s())),r&&t.addEventListener("versionchange",(()=>r()))})).catch((()=>{})),a}("firebase-heartbeat-database",1,{upgrade:(t,e)=>{if(0===e)t.createObjectStore("firebase-heartbeat-store")}}).catch((t=>{throw mi.create("idb-open",{originalErrorMessage:t.message})}))),bi}async function Ei(t,e){var n;try{const n=(await _i()).transaction("firebase-heartbeat-store","readwrite"),i=n.objectStore("firebase-heartbeat-store");return await i.put(e,wi(t)),n.done}catch(t){if(t instanceof Ln)ci.warn(t.message);else{const e=mi.create("idb-set",{originalErrorMessage:null===(n=t)||void 0===n?void 0:n.message});ci.warn(e.message)}}}function wi(t){return`${t.name}!${t.options.appId}`}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Si{constructor(t){this.container=t,this._heartbeatsCache=null;const e=this.container.getProvider("app").getImmediate();this._storage=new xi(e),this._heartbeatsCachePromise=this._storage.read().then((t=>(this._heartbeatsCache=t,t)))}async triggerHeartbeat(){const t=this.container.getProvider("platform-logger").getImmediate().getPlatformInfoString(),e=Ai();if(null===this._heartbeatsCache&&(this._heartbeatsCache=await this._heartbeatsCachePromise),this._heartbeatsCache.lastSentHeartbeatDate!==e&&!this._heartbeatsCache.heartbeats.some((t=>t.date===e)))return this._heartbeatsCache.heartbeats.push({date:e,agent:t}),this._heartbeatsCache.heartbeats=this._heartbeatsCache.heartbeats.filter((t=>{const e=new Date(t.date).valueOf();return Date.now()-e<=2592e6})),this._storage.overwrite(this._heartbeatsCache)}async getHeartbeatsHeader(){if(null===this._heartbeatsCache&&await this._heartbeatsCachePromise,null===this._heartbeatsCache||0===this._heartbeatsCache.heartbeats.length)return"";const t=Ai(),{heartbeatsToSend:e,unsentEntries:n}=function(t,e=1024){const n=[];let i=t.slice();for(const r of t){const t=n.find((t=>t.agent===r.agent));if(t){if(t.dates.push(r.date),Ti(n)>e){t.dates.pop();break}}else if(n.push({agent:r.agent,dates:[r.date]}),Ti(n)>e){n.pop();break}i=i.slice(1)}return{heartbeatsToSend:n,unsentEntries:i}}(this._heartbeatsCache.heartbeats),i=Dn(JSON.stringify({version:2,heartbeats:e}));return this._heartbeatsCache.lastSentHeartbeatDate=t,n.length>0?(this._heartbeatsCache.heartbeats=n,await this._storage.overwrite(this._heartbeatsCache)):(this._heartbeatsCache.heartbeats=[],this._storage.overwrite(this._heartbeatsCache)),i}}function Ai(){return(new Date).toISOString().substring(0,10)}class xi{constructor(t){this.app=t,this._canUseIndexedDBPromise=this.runIndexedDBEnvironmentCheck()}async runIndexedDBEnvironmentCheck(){return"object"==typeof indexedDB&&new Promise(((t,e)=>{try{let n=!0;const i="validate-browser-context-for-indexeddb-analytics-module",r=self.indexedDB.open(i);r.onsuccess=()=>{r.result.close(),n||self.indexedDB.deleteDatabase(i),t(!0)},r.onupgradeneeded=()=>{n=!1},r.onerror=()=>{var t;e((null===(t=r.error)||void 0===t?void 0:t.message)||"")}}catch(t){e(t)}})).then((()=>!0)).catch((()=>!1))}async read(){if(await this._canUseIndexedDBPromise){return await async function(t){var e;try{return(await _i()).transaction("firebase-heartbeat-store").objectStore("firebase-heartbeat-store").get(wi(t))}catch(t){if(t instanceof Ln)ci.warn(t.message);else{const n=mi.create("idb-get",{originalErrorMessage:null===(e=t)||void 0===e?void 0:e.message});ci.warn(n.message)}}}(this.app)||{heartbeats:[]}}return{heartbeats:[]}}async overwrite(t){var e;if(await this._canUseIndexedDBPromise){const n=await this.read();return Ei(this.app,{lastSentHeartbeatDate:null!==(e=t.lastSentHeartbeatDate)&&void 0!==e?e:n.lastSentHeartbeatDate,heartbeats:t.heartbeats})}}async add(t){var e;if(await this._canUseIndexedDBPromise){const n=await this.read();return Ei(this.app,{lastSentHeartbeatDate:null!==(e=t.lastSentHeartbeatDate)&&void 0!==e?e:n.lastSentHeartbeatDate,heartbeats:[...n.heartbeats,...t.heartbeats]})}}}function Ti(t){return Dn(JSON.stringify({version:2,heartbeats:t})).length}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */var Ci;Ci="",gi(new Mn("platform-logger",(t=>new li(t)),"PRIVATE")),gi(new Mn("heartbeat",(t=>new Si(t)),"PRIVATE")),vi(hi,"0.7.31",Ci),vi(hi,"0.7.31","esm2017"),vi("fire-js","");
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
vi("firebase","9.9.3","cdn");
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const Ii=function(t){const e=[];let n=0;for(let i=0;i<t.length;i++){let r=t.charCodeAt(i);r<128?e[n++]=r:r<2048?(e[n++]=r>>6|192,e[n++]=63&r|128):55296==(64512&r)&&i+1<t.length&&56320==(64512&t.charCodeAt(i+1))?(r=65536+((1023&r)<<10)+(1023&t.charCodeAt(++i)),e[n++]=r>>18|240,e[n++]=r>>12&63|128,e[n++]=r>>6&63|128,e[n++]=63&r|128):(e[n++]=r>>12|224,e[n++]=r>>6&63|128,e[n++]=63&r|128)}return e},Di={byteToCharMap_:null,charToByteMap_:null,byteToCharMapWebSafe_:null,charToByteMapWebSafe_:null,ENCODED_VALS_BASE:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",get ENCODED_VALS(){return this.ENCODED_VALS_BASE+"+/="},get ENCODED_VALS_WEBSAFE(){return this.ENCODED_VALS_BASE+"-_."},HAS_NATIVE_SUPPORT:"function"==typeof atob,encodeByteArray(t,e){if(!Array.isArray(t))throw Error("encodeByteArray takes an array as a parameter");this.init_();const n=e?this.byteToCharMapWebSafe_:this.byteToCharMap_,i=[];for(let e=0;e<t.length;e+=3){const r=t[e],s=e+1<t.length,o=s?t[e+1]:0,a=e+2<t.length,l=a?t[e+2]:0,h=r>>2,c=(3&r)<<4|o>>4;let u=(15&o)<<2|l>>6,f=63&l;a||(f=64,s||(u=64)),i.push(n[h],n[c],n[u],n[f])}return i.join("")},encodeString(t,e){return this.HAS_NATIVE_SUPPORT&&!e?btoa(t):this.encodeByteArray(Ii(t),e)},decodeString(t,e){return this.HAS_NATIVE_SUPPORT&&!e?atob(t):function(t){const e=[];let n=0,i=0;for(;n<t.length;){const r=t[n++];if(r<128)e[i++]=String.fromCharCode(r);else if(r>191&&r<224){const s=t[n++];e[i++]=String.fromCharCode((31&r)<<6|63&s)}else if(r>239&&r<365){const s=((7&r)<<18|(63&t[n++])<<12|(63&t[n++])<<6|63&t[n++])-65536;e[i++]=String.fromCharCode(55296+(s>>10)),e[i++]=String.fromCharCode(56320+(1023&s))}else{const s=t[n++],o=t[n++];e[i++]=String.fromCharCode((15&r)<<12|(63&s)<<6|63&o)}}return e.join("")}(this.decodeStringToByteArray(t,e))},decodeStringToByteArray(t,e){this.init_();const n=e?this.charToByteMapWebSafe_:this.charToByteMap_,i=[];for(let e=0;e<t.length;){const r=n[t.charAt(e++)],s=e<t.length?n[t.charAt(e)]:0;++e;const o=e<t.length?n[t.charAt(e)]:64;++e;const a=e<t.length?n[t.charAt(e)]:64;if(++e,null==r||null==s||null==o||null==a)throw Error();const l=r<<2|s>>4;if(i.push(l),64!==o){const t=s<<4&240|o>>2;if(i.push(t),64!==a){const t=o<<6&192|a;i.push(t)}}}return i},init_(){if(!this.byteToCharMap_){this.byteToCharMap_={},this.charToByteMap_={},this.byteToCharMapWebSafe_={},this.charToByteMapWebSafe_={};for(let t=0;t<this.ENCODED_VALS.length;t++)this.byteToCharMap_[t]=this.ENCODED_VALS.charAt(t),this.charToByteMap_[this.byteToCharMap_[t]]=t,this.byteToCharMapWebSafe_[t]=this.ENCODED_VALS_WEBSAFE.charAt(t),this.charToByteMapWebSafe_[this.byteToCharMapWebSafe_[t]]=t,t>=this.ENCODED_VALS_BASE.length&&(this.charToByteMap_[this.ENCODED_VALS_WEBSAFE.charAt(t)]=t,this.charToByteMapWebSafe_[this.ENCODED_VALS.charAt(t)]=t)}}},Oi=function(t){return function(t){const e=Ii(t);return Di.encodeByteArray(e,!0)}(t).replace(/\./g,"")};
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Li(){return"object"==typeof indexedDB}class ki extends Error{constructor(t,e,n){super(e),this.code=t,this.customData=n,this.name="FirebaseError",Object.setPrototypeOf(this,ki.prototype),Error.captureStackTrace&&Error.captureStackTrace(this,Ni.prototype.create)}}class Ni{constructor(t,e,n){this.service=t,this.serviceName=e,this.errors=n}create(t,...e){const n=e[0]||{},i=`${this.service}/${t}`,r=this.errors[t],s=r?function(t,e){return t.replace(Ri,((t,n)=>{const i=e[n];return null!=i?String(i):`<${n}?>`}))}(r,n):"Error",o=`${this.serviceName}: ${s} (${i}).`;return new ki(i,o,n)}}const Ri=/\{\$([^}]+)}/g;
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Pi{constructor(t,e,n){this.name=t,this.instanceFactory=e,this.type=n,this.multipleInstances=!1,this.serviceProps={},this.instantiationMode="LAZY",this.onInstanceCreated=null}setInstantiationMode(t){return this.instantiationMode=t,this}setMultipleInstances(t){return this.multipleInstances=t,this}setServiceProps(t){return this.serviceProps=t,this}setInstanceCreatedCallback(t){return this.onInstanceCreated=t,this}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const Mi=[];var ji;!function(t){t[t.DEBUG=0]="DEBUG",t[t.VERBOSE=1]="VERBOSE",t[t.INFO=2]="INFO",t[t.WARN=3]="WARN",t[t.ERROR=4]="ERROR",t[t.SILENT=5]="SILENT"}(ji||(ji={}));const Bi={debug:ji.DEBUG,verbose:ji.VERBOSE,info:ji.INFO,warn:ji.WARN,error:ji.ERROR,silent:ji.SILENT},Fi=ji.INFO,$i={[ji.DEBUG]:"log",[ji.VERBOSE]:"log",[ji.INFO]:"info",[ji.WARN]:"warn",[ji.ERROR]:"error"},Ui=(t,e,...n)=>{if(e<t.logLevel)return;const i=(new Date).toISOString(),r=$i[e];if(!r)throw new Error(`Attempted to log a message with an invalid logType (value: ${e})`);console[r](`[${i}]  ${t.name}:`,...n)};class Hi{constructor(t){this.name=t,this._logLevel=Fi,this._logHandler=Ui,this._userLogHandler=null,Mi.push(this)}get logLevel(){return this._logLevel}set logLevel(t){if(!(t in ji))throw new TypeError(`Invalid value "${t}" assigned to \`logLevel\``);this._logLevel=t}setLogLevel(t){this._logLevel="string"==typeof t?Bi[t]:t}get logHandler(){return this._logHandler}set logHandler(t){if("function"!=typeof t)throw new TypeError("Value assigned to `logHandler` must be a function");this._logHandler=t}get userLogHandler(){return this._userLogHandler}set userLogHandler(t){this._userLogHandler=t}debug(...t){this._userLogHandler&&this._userLogHandler(this,ji.DEBUG,...t),this._logHandler(this,ji.DEBUG,...t)}log(...t){this._userLogHandler&&this._userLogHandler(this,ji.VERBOSE,...t),this._logHandler(this,ji.VERBOSE,...t)}info(...t){this._userLogHandler&&this._userLogHandler(this,ji.INFO,...t),this._logHandler(this,ji.INFO,...t)}warn(...t){this._userLogHandler&&this._userLogHandler(this,ji.WARN,...t),this._logHandler(this,ji.WARN,...t)}error(...t){this._userLogHandler&&this._userLogHandler(this,ji.ERROR,...t),this._logHandler(this,ji.ERROR,...t)}}let Vi,zi;const Wi=new WeakMap,Ki=new WeakMap,Gi=new WeakMap,qi=new WeakMap,Xi=new WeakMap;let Yi={get(t,e,n){if(t instanceof IDBTransaction){if("done"===e)return Ki.get(t);if("objectStoreNames"===e)return t.objectStoreNames||Gi.get(t);if("store"===e)return n.objectStoreNames[1]?void 0:n.objectStore(n.objectStoreNames[0])}return Zi(t[e])},set:(t,e,n)=>(t[e]=n,!0),has:(t,e)=>t instanceof IDBTransaction&&("done"===e||"store"===e)||e in t};function Ji(t){return t!==IDBDatabase.prototype.transaction||"objectStoreNames"in IDBTransaction.prototype?(zi||(zi=[IDBCursor.prototype.advance,IDBCursor.prototype.continue,IDBCursor.prototype.continuePrimaryKey])).includes(t)?function(...e){return t.apply(tr(this),e),Zi(Wi.get(this))}:function(...e){return Zi(t.apply(tr(this),e))}:function(e,...n){const i=t.call(tr(this),e,...n);return Gi.set(i,e.sort?e.sort():[e]),Zi(i)}}function Qi(t){return"function"==typeof t?Ji(t):(t instanceof IDBTransaction&&function(t){if(Ki.has(t))return;const e=new Promise(((e,n)=>{const i=()=>{t.removeEventListener("complete",r),t.removeEventListener("error",s),t.removeEventListener("abort",s)},r=()=>{e(),i()},s=()=>{n(t.error||new DOMException("AbortError","AbortError")),i()};t.addEventListener("complete",r),t.addEventListener("error",s),t.addEventListener("abort",s)}));Ki.set(t,e)}(t),e=t,(Vi||(Vi=[IDBDatabase,IDBObjectStore,IDBIndex,IDBCursor,IDBTransaction])).some((t=>e instanceof t))?new Proxy(t,Yi):t);var e}function Zi(t){if(t instanceof IDBRequest)return function(t){const e=new Promise(((e,n)=>{const i=()=>{t.removeEventListener("success",r),t.removeEventListener("error",s)},r=()=>{e(Zi(t.result)),i()},s=()=>{n(t.error),i()};t.addEventListener("success",r),t.addEventListener("error",s)}));return e.then((e=>{e instanceof IDBCursor&&Wi.set(e,t)})).catch((()=>{})),Xi.set(e,t),e}(t);if(qi.has(t))return qi.get(t);const e=Qi(t);return e!==t&&(qi.set(t,e),Xi.set(e,t)),e}const tr=t=>Xi.get(t);function er(t,e,{blocked:n,upgrade:i,blocking:r,terminated:s}={}){const o=indexedDB.open(t,e),a=Zi(o);return i&&o.addEventListener("upgradeneeded",(t=>{i(Zi(o.result),t.oldVersion,t.newVersion,Zi(o.transaction))})),n&&o.addEventListener("blocked",(()=>n())),a.then((t=>{s&&t.addEventListener("close",(()=>s())),r&&t.addEventListener("versionchange",(()=>r()))})).catch((()=>{})),a}const nr=["get","getKey","getAll","getAllKeys","count"],ir=["put","add","delete","clear"],rr=new Map;function sr(t,e){if(!(t instanceof IDBDatabase)||e in t||"string"!=typeof e)return;if(rr.get(e))return rr.get(e);const n=e.replace(/FromIndex$/,""),i=e!==n,r=ir.includes(n);if(!(n in(i?IDBIndex:IDBObjectStore).prototype)||!r&&!nr.includes(n))return;const s=async function(t,...e){const s=this.transaction(t,r?"readwrite":"readonly");let o=s.store;return i&&(o=o.index(e.shift())),(await Promise.all([o[n](...e),r&&s.done]))[0]};return rr.set(e,s),s}!function(t){Yi=t(Yi)}((t=>({...t,get:(e,n,i)=>sr(e,n)||t.get(e,n,i),has:(e,n)=>!!sr(e,n)||t.has(e,n)})));
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class or{constructor(t){this.container=t}getPlatformInfoString(){return this.container.getProviders().map((t=>{if(function(t){const e=t.getComponent();return"VERSION"===(null==e?void 0:e.type)}(t)){const e=t.getImmediate();return`${e.library}/${e.version}`}return null})).filter((t=>t)).join(" ")}}const ar=new Hi("@firebase/app"),lr={"@firebase/app":"fire-core","@firebase/app-compat":"fire-core-compat","@firebase/analytics":"fire-analytics","@firebase/analytics-compat":"fire-analytics-compat","@firebase/app-check":"fire-app-check","@firebase/app-check-compat":"fire-app-check-compat","@firebase/auth":"fire-auth","@firebase/auth-compat":"fire-auth-compat","@firebase/database":"fire-rtdb","@firebase/database-compat":"fire-rtdb-compat","@firebase/functions":"fire-fn","@firebase/functions-compat":"fire-fn-compat","@firebase/installations":"fire-iid","@firebase/installations-compat":"fire-iid-compat","@firebase/messaging":"fire-fcm","@firebase/messaging-compat":"fire-fcm-compat","@firebase/performance":"fire-perf","@firebase/performance-compat":"fire-perf-compat","@firebase/remote-config":"fire-rc","@firebase/remote-config-compat":"fire-rc-compat","@firebase/storage":"fire-gcs","@firebase/storage-compat":"fire-gcs-compat","@firebase/firestore":"fire-fst","@firebase/firestore-compat":"fire-fst-compat","fire-js":"fire-js",firebase:"fire-js-all"},hr=new Map,cr=new Map;function ur(t,e){try{t.container.addComponent(e)}catch(n){ar.debug(`Component ${e.name} failed to register with FirebaseApp ${t.name}`,n)}}function fr(t){const e=t.name;if(cr.has(e))return ar.debug(`There were multiple attempts to register component ${e}.`),!1;cr.set(e,t);for(const e of hr.values())ur(e,t);return!0}function dr(t,e){const n=t.container.getProvider("heartbeat").getImmediate({optional:!0});return n&&n.triggerHeartbeat(),t.container.getProvider(e)}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const pr=new Ni("app","Firebase",{"no-app":"No Firebase App '{$appName}' has been created - call Firebase App.initializeApp()","bad-app-name":"Illegal App name: '{$appName}","duplicate-app":"Firebase App named '{$appName}' already exists with different options or config","app-deleted":"Firebase App named '{$appName}' already deleted","invalid-app-argument":"firebase.{$appName}() takes either no argument or a Firebase App instance.","invalid-log-argument":"First argument to `onLog` must be null or a function.","idb-open":"Error thrown when opening IndexedDB. Original error: {$originalErrorMessage}.","idb-get":"Error thrown when reading from IndexedDB. Original error: {$originalErrorMessage}.","idb-set":"Error thrown when writing to IndexedDB. Original error: {$originalErrorMessage}.","idb-delete":"Error thrown when deleting from IndexedDB. Original error: {$originalErrorMessage}."});function gr(t="[DEFAULT]"){const e=hr.get(t);if(!e)throw pr.create("no-app",{appName:t});return e}function mr(t,e,n){var i;let r=null!==(i=lr[t])&&void 0!==i?i:t;n&&(r+=`-${n}`);const s=r.match(/\s|\//),o=e.match(/\s|\//);if(s||o){const t=[`Unable to register library "${r}" with version "${e}":`];return s&&t.push(`library name "${r}" contains illegal characters (whitespace or "/")`),s&&o&&t.push("and"),o&&t.push(`version name "${e}" contains illegal characters (whitespace or "/")`),void ar.warn(t.join(" "))}fr(new Pi(`${r}-version`,(()=>({library:r,version:e})),"VERSION"))}let yr=null;function vr(){return yr||(yr=er("firebase-heartbeat-database",1,{upgrade:(t,e)=>{if(0===e)t.createObjectStore("firebase-heartbeat-store")}}).catch((t=>{throw pr.create("idb-open",{originalErrorMessage:t.message})}))),yr}async function br(t,e){var n;try{const n=(await vr()).transaction("firebase-heartbeat-store","readwrite"),i=n.objectStore("firebase-heartbeat-store");return await i.put(e,_r(t)),n.done}catch(t){if(t instanceof ki)ar.warn(t.message);else{const e=pr.create("idb-set",{originalErrorMessage:null===(n=t)||void 0===n?void 0:n.message});ar.warn(e.message)}}}function _r(t){return`${t.name}!${t.options.appId}`}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Er{constructor(t){this.container=t,this._heartbeatsCache=null;const e=this.container.getProvider("app").getImmediate();this._storage=new Sr(e),this._heartbeatsCachePromise=this._storage.read().then((t=>(this._heartbeatsCache=t,t)))}async triggerHeartbeat(){const t=this.container.getProvider("platform-logger").getImmediate().getPlatformInfoString(),e=wr();if(null===this._heartbeatsCache&&(this._heartbeatsCache=await this._heartbeatsCachePromise),this._heartbeatsCache.lastSentHeartbeatDate!==e&&!this._heartbeatsCache.heartbeats.some((t=>t.date===e)))return this._heartbeatsCache.heartbeats.push({date:e,agent:t}),this._heartbeatsCache.heartbeats=this._heartbeatsCache.heartbeats.filter((t=>{const e=new Date(t.date).valueOf();return Date.now()-e<=2592e6})),this._storage.overwrite(this._heartbeatsCache)}async getHeartbeatsHeader(){if(null===this._heartbeatsCache&&await this._heartbeatsCachePromise,null===this._heartbeatsCache||0===this._heartbeatsCache.heartbeats.length)return"";const t=wr(),{heartbeatsToSend:e,unsentEntries:n}=function(t,e=1024){const n=[];let i=t.slice();for(const r of t){const t=n.find((t=>t.agent===r.agent));if(t){if(t.dates.push(r.date),Ar(n)>e){t.dates.pop();break}}else if(n.push({agent:r.agent,dates:[r.date]}),Ar(n)>e){n.pop();break}i=i.slice(1)}return{heartbeatsToSend:n,unsentEntries:i}}(this._heartbeatsCache.heartbeats),i=Oi(JSON.stringify({version:2,heartbeats:e}));return this._heartbeatsCache.lastSentHeartbeatDate=t,n.length>0?(this._heartbeatsCache.heartbeats=n,await this._storage.overwrite(this._heartbeatsCache)):(this._heartbeatsCache.heartbeats=[],this._storage.overwrite(this._heartbeatsCache)),i}}function wr(){return(new Date).toISOString().substring(0,10)}class Sr{constructor(t){this.app=t,this._canUseIndexedDBPromise=this.runIndexedDBEnvironmentCheck()}async runIndexedDBEnvironmentCheck(){return!!Li()&&new Promise(((t,e)=>{try{let n=!0;const i="validate-browser-context-for-indexeddb-analytics-module",r=self.indexedDB.open(i);r.onsuccess=()=>{r.result.close(),n||self.indexedDB.deleteDatabase(i),t(!0)},r.onupgradeneeded=()=>{n=!1},r.onerror=()=>{var t;e((null===(t=r.error)||void 0===t?void 0:t.message)||"")}}catch(t){e(t)}})).then((()=>!0)).catch((()=>!1))}async read(){if(await this._canUseIndexedDBPromise){return await async function(t){var e;try{return(await vr()).transaction("firebase-heartbeat-store").objectStore("firebase-heartbeat-store").get(_r(t))}catch(t){if(t instanceof ki)ar.warn(t.message);else{const n=pr.create("idb-get",{originalErrorMessage:null===(e=t)||void 0===e?void 0:e.message});ar.warn(n.message)}}}(this.app)||{heartbeats:[]}}return{heartbeats:[]}}async overwrite(t){var e;if(await this._canUseIndexedDBPromise){const n=await this.read();return br(this.app,{lastSentHeartbeatDate:null!==(e=t.lastSentHeartbeatDate)&&void 0!==e?e:n.lastSentHeartbeatDate,heartbeats:t.heartbeats})}}async add(t){var e;if(await this._canUseIndexedDBPromise){const n=await this.read();return br(this.app,{lastSentHeartbeatDate:null!==(e=t.lastSentHeartbeatDate)&&void 0!==e?e:n.lastSentHeartbeatDate,heartbeats:[...n.heartbeats,...t.heartbeats]})}}}function Ar(t){return Oi(JSON.stringify({version:2,heartbeats:t})).length}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */!function(t){fr(new Pi("platform-logger",(t=>new or(t)),"PRIVATE")),fr(new Pi("heartbeat",(t=>new Er(t)),"PRIVATE")),mr("@firebase/app","0.7.31",t),mr("@firebase/app","0.7.31","esm2017"),mr("fire-js","")}("");var xr,Tr="undefined"!=typeof globalThis?globalThis:"undefined"!=typeof window?window:void 0!==r?r:"undefined"!=typeof self?self:{},Cr={},Ir=Ir||{},Dr=Tr||self;function Or(){}function Lr(t){var e=typeof t;return"array"==(e="object"!=e?e:t?Array.isArray(t)?"array":e:"null")||"object"==e&&"number"==typeof t.length}function kr(t){var e=typeof t;return"object"==e&&null!=t||"function"==e}var Nr="closure_uid_"+(1e9*Math.random()>>>0),Rr=0;function Pr(t,e,n){return t.call.apply(t.bind,arguments)}function Mr(t,e,n){if(!t)throw Error();if(2<arguments.length){var i=Array.prototype.slice.call(arguments,2);return function(){var n=Array.prototype.slice.call(arguments);return Array.prototype.unshift.apply(n,i),t.apply(e,n)}}return function(){return t.apply(e,arguments)}}function jr(t,e,n){return(jr=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?Pr:Mr).apply(null,arguments)}function Br(t,e){var n=Array.prototype.slice.call(arguments,1);return function(){var e=n.slice();return e.push.apply(e,arguments),t.apply(this,e)}}function Fr(t,e){function n(){}n.prototype=e.prototype,t.Z=e.prototype,t.prototype=new n,t.prototype.constructor=t,t.Vb=function(t,n,i){for(var r=Array(arguments.length-2),s=2;s<arguments.length;s++)r[s-2]=arguments[s];return e.prototype[n].apply(t,r)}}function $r(){this.s=this.s,this.o=this.o}var Ur={};$r.prototype.s=!1,$r.prototype.na=function(){if(!this.s&&(this.s=!0,this.M(),0)){var t=function(t){return Object.prototype.hasOwnProperty.call(t,Nr)&&t[Nr]||(t[Nr]=++Rr)}(this);delete Ur[t]}},$r.prototype.M=function(){if(this.o)for(;this.o.length;)this.o.shift()()};const Hr=Array.prototype.indexOf?function(t,e){return Array.prototype.indexOf.call(t,e,void 0)}:function(t,e){if("string"==typeof t)return"string"!=typeof e||1!=e.length?-1:t.indexOf(e,0);for(let n=0;n<t.length;n++)if(n in t&&t[n]===e)return n;return-1},Vr=Array.prototype.forEach?function(t,e,n){Array.prototype.forEach.call(t,e,n)}:function(t,e,n){const i=t.length,r="string"==typeof t?t.split(""):t;for(let s=0;s<i;s++)s in r&&e.call(n,r[s],s,t)};function zr(t){return Array.prototype.concat.apply([],arguments)}function Wr(t){const e=t.length;if(0<e){const n=Array(e);for(let i=0;i<e;i++)n[i]=t[i];return n}return[]}function Kr(t){return/^[\s\xa0]*$/.test(t)}var Gr,qr=String.prototype.trim?function(t){return t.trim()}:function(t){return/^[\s\xa0]*([\s\S]*?)[\s\xa0]*$/.exec(t)[1]};function Xr(t,e){return-1!=t.indexOf(e)}function Yr(t,e){return t<e?-1:t>e?1:0}t:{var Jr=Dr.navigator;if(Jr){var Qr=Jr.userAgent;if(Qr){Gr=Qr;break t}}Gr=""}function Zr(t,e,n){for(const i in t)e.call(n,t[i],i,t)}function ts(t){const e={};for(const n in t)e[n]=t[n];return e}var es="constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf".split(" ");function ns(t,e){let n,i;for(let e=1;e<arguments.length;e++){for(n in i=arguments[e],i)t[n]=i[n];for(let e=0;e<es.length;e++)n=es[e],Object.prototype.hasOwnProperty.call(i,n)&&(t[n]=i[n])}}function is(t){return is[" "](t),t}is[" "]=Or;var rs,ss,os=Xr(Gr,"Opera"),as=Xr(Gr,"Trident")||Xr(Gr,"MSIE"),ls=Xr(Gr,"Edge"),hs=ls||as,cs=Xr(Gr,"Gecko")&&!(Xr(Gr.toLowerCase(),"webkit")&&!Xr(Gr,"Edge"))&&!(Xr(Gr,"Trident")||Xr(Gr,"MSIE"))&&!Xr(Gr,"Edge"),us=Xr(Gr.toLowerCase(),"webkit")&&!Xr(Gr,"Edge");function fs(){var t=Dr.document;return t?t.documentMode:void 0}t:{var ds="",ps=(ss=Gr,cs?/rv:([^\);]+)(\)|;)/.exec(ss):ls?/Edge\/([\d\.]+)/.exec(ss):as?/\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(ss):us?/WebKit\/(\S+)/.exec(ss):os?/(?:Version)[ \/]?(\S+)/.exec(ss):void 0);if(ps&&(ds=ps?ps[1]:""),as){var gs=fs();if(null!=gs&&gs>parseFloat(ds)){rs=String(gs);break t}}rs=ds}var ms,ys={};function vs(){return function(t){var e=ys;return Object.prototype.hasOwnProperty.call(e,9)?e[9]:e[9]=t(9)}((function(){let t=0;const e=qr(String(rs)).split("."),n=qr("9").split("."),i=Math.max(e.length,n.length);for(let o=0;0==t&&o<i;o++){var r=e[o]||"",s=n[o]||"";do{if(r=/(\d*)(\D*)(.*)/.exec(r)||["","","",""],s=/(\d*)(\D*)(.*)/.exec(s)||["","","",""],0==r[0].length&&0==s[0].length)break;t=Yr(0==r[1].length?0:parseInt(r[1],10),0==s[1].length?0:parseInt(s[1],10))||Yr(0==r[2].length,0==s[2].length)||Yr(r[2],s[2]),r=r[3],s=s[3]}while(0==t)}return 0<=t}))}if(Dr.document&&as){var bs=fs();ms=bs||(parseInt(rs,10)||void 0)}else ms=void 0;var _s=ms,Es=function(){if(!Dr.addEventListener||!Object.defineProperty)return!1;var t=!1,e=Object.defineProperty({},"passive",{get:function(){t=!0}});try{Dr.addEventListener("test",Or,e),Dr.removeEventListener("test",Or,e)}catch(t){}return t}();function ws(t,e){this.type=t,this.g=this.target=e,this.defaultPrevented=!1}function Ss(t,e){if(ws.call(this,t?t.type:""),this.relatedTarget=this.g=this.target=null,this.button=this.screenY=this.screenX=this.clientY=this.clientX=0,this.key="",this.metaKey=this.shiftKey=this.altKey=this.ctrlKey=!1,this.state=null,this.pointerId=0,this.pointerType="",this.i=null,t){var n=this.type=t.type,i=t.changedTouches&&t.changedTouches.length?t.changedTouches[0]:null;if(this.target=t.target||t.srcElement,this.g=e,e=t.relatedTarget){if(cs){t:{try{is(e.nodeName);var r=!0;break t}catch(t){}r=!1}r||(e=null)}}else"mouseover"==n?e=t.fromElement:"mouseout"==n&&(e=t.toElement);this.relatedTarget=e,i?(this.clientX=void 0!==i.clientX?i.clientX:i.pageX,this.clientY=void 0!==i.clientY?i.clientY:i.pageY,this.screenX=i.screenX||0,this.screenY=i.screenY||0):(this.clientX=void 0!==t.clientX?t.clientX:t.pageX,this.clientY=void 0!==t.clientY?t.clientY:t.pageY,this.screenX=t.screenX||0,this.screenY=t.screenY||0),this.button=t.button,this.key=t.key||"",this.ctrlKey=t.ctrlKey,this.altKey=t.altKey,this.shiftKey=t.shiftKey,this.metaKey=t.metaKey,this.pointerId=t.pointerId||0,this.pointerType="string"==typeof t.pointerType?t.pointerType:As[t.pointerType]||"",this.state=t.state,this.i=t,t.defaultPrevented&&Ss.Z.h.call(this)}}ws.prototype.h=function(){this.defaultPrevented=!0},Fr(Ss,ws);var As={2:"touch",3:"pen",4:"mouse"};Ss.prototype.h=function(){Ss.Z.h.call(this);var t=this.i;t.preventDefault?t.preventDefault():t.returnValue=!1};var xs="closure_listenable_"+(1e6*Math.random()|0),Ts=0;function Cs(t,e,n,i,r){this.listener=t,this.proxy=null,this.src=e,this.type=n,this.capture=!!i,this.ia=r,this.key=++Ts,this.ca=this.fa=!1}function Is(t){t.ca=!0,t.listener=null,t.proxy=null,t.src=null,t.ia=null}function Ds(t){this.src=t,this.g={},this.h=0}function Os(t,e){var n=e.type;if(n in t.g){var i,r=t.g[n],s=Hr(r,e);(i=0<=s)&&Array.prototype.splice.call(r,s,1),i&&(Is(e),0==t.g[n].length&&(delete t.g[n],t.h--))}}function Ls(t,e,n,i){for(var r=0;r<t.length;++r){var s=t[r];if(!s.ca&&s.listener==e&&s.capture==!!n&&s.ia==i)return r}return-1}Ds.prototype.add=function(t,e,n,i,r){var s=t.toString();(t=this.g[s])||(t=this.g[s]=[],this.h++);var o=Ls(t,e,i,r);return-1<o?(e=t[o],n||(e.fa=!1)):((e=new Cs(e,this.src,s,!!i,r)).fa=n,t.push(e)),e};var ks="closure_lm_"+(1e6*Math.random()|0),Ns={};function Rs(t,e,n,i,r){if(i&&i.once)return Ms(t,e,n,i,r);if(Array.isArray(e)){for(var s=0;s<e.length;s++)Rs(t,e[s],n,i,r);return null}return n=Vs(n),t&&t[xs]?t.N(e,n,kr(i)?!!i.capture:!!i,r):Ps(t,e,n,!1,i,r)}function Ps(t,e,n,i,r,s){if(!e)throw Error("Invalid event type");var o=kr(r)?!!r.capture:!!r,a=Us(t);if(a||(t[ks]=a=new Ds(t)),(n=a.add(e,n,i,o,s)).proxy)return n;if(i=function(){function t(n){return e.call(t.src,t.listener,n)}var e=$s;return t}(),n.proxy=i,i.src=t,i.listener=n,t.addEventListener)Es||(r=o),void 0===r&&(r=!1),t.addEventListener(e.toString(),i,r);else if(t.attachEvent)t.attachEvent(Fs(e.toString()),i);else{if(!t.addListener||!t.removeListener)throw Error("addEventListener and attachEvent are unavailable.");t.addListener(i)}return n}function Ms(t,e,n,i,r){if(Array.isArray(e)){for(var s=0;s<e.length;s++)Ms(t,e[s],n,i,r);return null}return n=Vs(n),t&&t[xs]?t.O(e,n,kr(i)?!!i.capture:!!i,r):Ps(t,e,n,!0,i,r)}function js(t,e,n,i,r){if(Array.isArray(e))for(var s=0;s<e.length;s++)js(t,e[s],n,i,r);else i=kr(i)?!!i.capture:!!i,n=Vs(n),t&&t[xs]?(t=t.i,(e=String(e).toString())in t.g&&(-1<(n=Ls(s=t.g[e],n,i,r))&&(Is(s[n]),Array.prototype.splice.call(s,n,1),0==s.length&&(delete t.g[e],t.h--)))):t&&(t=Us(t))&&(e=t.g[e.toString()],t=-1,e&&(t=Ls(e,n,i,r)),(n=-1<t?e[t]:null)&&Bs(n))}function Bs(t){if("number"!=typeof t&&t&&!t.ca){var e=t.src;if(e&&e[xs])Os(e.i,t);else{var n=t.type,i=t.proxy;e.removeEventListener?e.removeEventListener(n,i,t.capture):e.detachEvent?e.detachEvent(Fs(n),i):e.addListener&&e.removeListener&&e.removeListener(i),(n=Us(e))?(Os(n,t),0==n.h&&(n.src=null,e[ks]=null)):Is(t)}}}function Fs(t){return t in Ns?Ns[t]:Ns[t]="on"+t}function $s(t,e){if(t.ca)t=!0;else{e=new Ss(e,this);var n=t.listener,i=t.ia||t.src;t.fa&&Bs(t),t=n.call(i,e)}return t}function Us(t){return(t=t[ks])instanceof Ds?t:null}var Hs="__closure_events_fn_"+(1e9*Math.random()>>>0);function Vs(t){return"function"==typeof t?t:(t[Hs]||(t[Hs]=function(e){return t.handleEvent(e)}),t[Hs])}function zs(){$r.call(this),this.i=new Ds(this),this.P=this,this.I=null}function Ws(t,e){var n,i=t.I;if(i)for(n=[];i;i=i.I)n.push(i);if(t=t.P,i=e.type||e,"string"==typeof e)e=new ws(e,t);else if(e instanceof ws)e.target=e.target||t;else{var r=e;ns(e=new ws(i,t),r)}if(r=!0,n)for(var s=n.length-1;0<=s;s--){var o=e.g=n[s];r=Ks(o,i,!0,e)&&r}if(r=Ks(o=e.g=t,i,!0,e)&&r,r=Ks(o,i,!1,e)&&r,n)for(s=0;s<n.length;s++)r=Ks(o=e.g=n[s],i,!1,e)&&r}function Ks(t,e,n,i){if(!(e=t.i.g[String(e)]))return!0;e=e.concat();for(var r=!0,s=0;s<e.length;++s){var o=e[s];if(o&&!o.ca&&o.capture==n){var a=o.listener,l=o.ia||o.src;o.fa&&Os(t.i,o),r=!1!==a.call(l,i)&&r}}return r&&!i.defaultPrevented}Fr(zs,$r),zs.prototype[xs]=!0,zs.prototype.removeEventListener=function(t,e,n,i){js(this,t,e,n,i)},zs.prototype.M=function(){if(zs.Z.M.call(this),this.i){var t,e=this.i;for(t in e.g){for(var n=e.g[t],i=0;i<n.length;i++)Is(n[i]);delete e.g[t],e.h--}}this.I=null},zs.prototype.N=function(t,e,n,i){return this.i.add(String(t),e,!1,n,i)},zs.prototype.O=function(t,e,n,i){return this.i.add(String(t),e,!0,n,i)};var Gs=Dr.JSON.stringify;function qs(){var t=eo;let e=null;return t.g&&(e=t.g,t.g=t.g.next,t.g||(t.h=null),e.next=null),e}var Xs,Ys=new class{constructor(t,e){this.i=t,this.j=e,this.h=0,this.g=null}get(){let t;return 0<this.h?(this.h--,t=this.g,this.g=t.next,t.next=null):t=this.i(),t}}((()=>new Js),(t=>t.reset()));class Js{constructor(){this.next=this.g=this.h=null}set(t,e){this.h=t,this.g=e,this.next=null}reset(){this.next=this.g=this.h=null}}function Qs(t){Dr.setTimeout((()=>{throw t}),0)}function Zs(t,e){Xs||function(){var t=Dr.Promise.resolve(void 0);Xs=function(){t.then(no)}}(),to||(Xs(),to=!0),eo.add(t,e)}var to=!1,eo=new class{constructor(){this.h=this.g=null}add(t,e){const n=Ys.get();n.set(t,e),this.h?this.h.next=n:this.g=n,this.h=n}};function no(){for(var t;t=qs();){try{t.h.call(t.g)}catch(t){Qs(t)}var e=Ys;e.j(t),100>e.h&&(e.h++,t.next=e.g,e.g=t)}to=!1}function io(t,e){zs.call(this),this.h=t||1,this.g=e||Dr,this.j=jr(this.kb,this),this.l=Date.now()}function ro(t){t.da=!1,t.S&&(t.g.clearTimeout(t.S),t.S=null)}function so(t,e,n){if("function"==typeof t)n&&(t=jr(t,n));else{if(!t||"function"!=typeof t.handleEvent)throw Error("Invalid listener argument");t=jr(t.handleEvent,t)}return 2147483647<Number(e)?-1:Dr.setTimeout(t,e||0)}function oo(t){t.g=so((()=>{t.g=null,t.i&&(t.i=!1,oo(t))}),t.j);const e=t.h;t.h=null,t.m.apply(null,e)}Fr(io,zs),(xr=io.prototype).da=!1,xr.S=null,xr.kb=function(){if(this.da){var t=Date.now()-this.l;0<t&&t<.8*this.h?this.S=this.g.setTimeout(this.j,this.h-t):(this.S&&(this.g.clearTimeout(this.S),this.S=null),Ws(this,"tick"),this.da&&(ro(this),this.start()))}},xr.start=function(){this.da=!0,this.S||(this.S=this.g.setTimeout(this.j,this.h),this.l=Date.now())},xr.M=function(){io.Z.M.call(this),ro(this),delete this.g};class ao extends $r{constructor(t,e){super(),this.m=t,this.j=e,this.h=null,this.i=!1,this.g=null}l(t){this.h=arguments,this.g?this.i=!0:oo(this)}M(){super.M(),this.g&&(Dr.clearTimeout(this.g),this.g=null,this.i=!1,this.h=null)}}function lo(t){$r.call(this),this.h=t,this.g={}}Fr(lo,$r);var ho=[];function co(t,e,n,i){Array.isArray(n)||(n&&(ho[0]=n.toString()),n=ho);for(var r=0;r<n.length;r++){var s=Rs(e,n[r],i||t.handleEvent,!1,t.h||t);if(!s)break;t.g[s.key]=s}}function uo(t){Zr(t.g,(function(t,e){this.g.hasOwnProperty(e)&&Bs(t)}),t),t.g={}}function fo(){this.g=!0}function po(t,e,n,i){t.info((function(){return"XMLHTTP TEXT ("+e+"): "+function(t,e){if(!t.g)return e;if(!e)return null;try{var n=JSON.parse(e);if(n)for(t=0;t<n.length;t++)if(Array.isArray(n[t])){var i=n[t];if(!(2>i.length)){var r=i[1];if(Array.isArray(r)&&!(1>r.length)){var s=r[0];if("noop"!=s&&"stop"!=s&&"close"!=s)for(var o=1;o<r.length;o++)r[o]=""}}}return Gs(n)}catch(t){return e}}(t,n)+(i?" "+i:"")}))}lo.prototype.M=function(){lo.Z.M.call(this),uo(this)},lo.prototype.handleEvent=function(){throw Error("EventHandler.handleEvent not implemented")},fo.prototype.Aa=function(){this.g=!1},fo.prototype.info=function(){};var go={},mo=null;function yo(){return mo=mo||new zs}function vo(t){ws.call(this,go.Ma,t)}function bo(t){const e=yo();Ws(e,new vo(e,t))}function _o(t,e){ws.call(this,go.STAT_EVENT,t),this.stat=e}function Eo(t){const e=yo();Ws(e,new _o(e,t))}function wo(t,e){ws.call(this,go.Na,t),this.size=e}function So(t,e){if("function"!=typeof t)throw Error("Fn must not be null and must be a function");return Dr.setTimeout((function(){t()}),e)}go.Ma="serverreachability",Fr(vo,ws),go.STAT_EVENT="statevent",Fr(_o,ws),go.Na="timingevent",Fr(wo,ws);var Ao={NO_ERROR:0,lb:1,yb:2,xb:3,sb:4,wb:5,zb:6,Ja:7,TIMEOUT:8,Cb:9},xo={qb:"complete",Mb:"success",Ka:"error",Ja:"abort",Eb:"ready",Fb:"readystatechange",TIMEOUT:"timeout",Ab:"incrementaldata",Db:"progress",tb:"downloadprogress",Ub:"uploadprogress"};function To(){}function Co(t){return t.h||(t.h=t.i())}function Io(){}To.prototype.h=null;var Do,Oo={OPEN:"a",pb:"b",Ka:"c",Bb:"d"};function Lo(){ws.call(this,"d")}function ko(){ws.call(this,"c")}function No(){}function Ro(t,e,n,i){this.l=t,this.j=e,this.m=n,this.X=i||1,this.V=new lo(this),this.P=Mo,t=hs?125:void 0,this.W=new io(t),this.H=null,this.i=!1,this.s=this.A=this.v=this.K=this.F=this.Y=this.B=null,this.D=[],this.g=null,this.C=0,this.o=this.u=null,this.N=-1,this.I=!1,this.O=0,this.L=null,this.aa=this.J=this.$=this.U=!1,this.h=new Po}function Po(){this.i=null,this.g="",this.h=!1}Fr(Lo,ws),Fr(ko,ws),Fr(No,To),No.prototype.g=function(){return new XMLHttpRequest},No.prototype.i=function(){return{}},Do=new No;var Mo=45e3,jo={},Bo={};function Fo(t,e,n){t.K=1,t.v=la(na(e)),t.s=n,t.U=!0,$o(t,null)}function $o(t,e){t.F=Date.now(),zo(t),t.A=na(t.v);var n=t.A,i=t.X;Array.isArray(i)||(i=[String(i)]),Ea(n.h,"t",i),t.C=0,n=t.l.H,t.h=new Po,t.g=El(t.l,n?e:null,!t.s),0<t.O&&(t.L=new ao(jr(t.Ia,t,t.g),t.O)),co(t.V,t.g,"readystatechange",t.gb),e=t.H?ts(t.H):{},t.s?(t.u||(t.u="POST"),e["Content-Type"]="application/x-www-form-urlencoded",t.g.ea(t.A,t.u,t.s,e)):(t.u="GET",t.g.ea(t.A,t.u,null,e)),bo(1),function(t,e,n,i,r,s){t.info((function(){if(t.g)if(s)for(var o="",a=s.split("&"),l=0;l<a.length;l++){var h=a[l].split("=");if(1<h.length){var c=h[0];h=h[1];var u=c.split("_");o=2<=u.length&&"type"==u[1]?o+(c+"=")+h+"&":o+(c+"=redacted&")}}else o=null;else o=s;return"XMLHTTP REQ ("+i+") [attempt "+r+"]: "+e+"\n"+n+"\n"+o}))}(t.j,t.u,t.A,t.m,t.X,t.s)}function Uo(t){return!!t.g&&("GET"==t.u&&2!=t.K&&t.l.Ba)}function Ho(t,e,n){let i,r=!0;for(;!t.I&&t.C<n.length;){if(i=Vo(t,n),i==Bo){4==e&&(t.o=4,Eo(14),r=!1),po(t.j,t.m,null,"[Incomplete Response]");break}if(i==jo){t.o=4,Eo(15),po(t.j,t.m,n,"[Invalid Chunk]"),r=!1;break}po(t.j,t.m,i,null),Xo(t,i)}Uo(t)&&i!=Bo&&i!=jo&&(t.h.g="",t.C=0),4!=e||0!=n.length||t.h.h||(t.o=1,Eo(16),r=!1),t.i=t.i&&r,r?0<n.length&&!t.aa&&(t.aa=!0,(e=t.l).g==t&&e.$&&!e.L&&(e.h.info("Great, no buffering proxy detected. Bytes received: "+n.length),dl(e),e.L=!0,Eo(11))):(po(t.j,t.m,n,"[Invalid Chunked Response]"),qo(t),Go(t))}function Vo(t,e){var n=t.C,i=e.indexOf("\n",n);return-1==i?Bo:(n=Number(e.substring(n,i)),isNaN(n)?jo:(i+=1)+n>e.length?Bo:(e=e.substr(i,n),t.C=i+n,e))}function zo(t){t.Y=Date.now()+t.P,Wo(t,t.P)}function Wo(t,e){if(null!=t.B)throw Error("WatchDog timer not null");t.B=So(jr(t.eb,t),e)}function Ko(t){t.B&&(Dr.clearTimeout(t.B),t.B=null)}function Go(t){0==t.l.G||t.I||ml(t.l,t)}function qo(t){Ko(t);var e=t.L;e&&"function"==typeof e.na&&e.na(),t.L=null,ro(t.W),uo(t.V),t.g&&(e=t.g,t.g=null,e.abort(),e.na())}function Xo(t,e){try{var n=t.l;if(0!=n.G&&(n.g==t||Ca(n.i,t)))if(n.I=t.N,!t.J&&Ca(n.i,t)&&3==n.G){try{var i=n.Ca.g.parse(e)}catch(t){i=null}if(Array.isArray(i)&&3==i.length){var r=i;if(0==r[0]){t:if(!n.u){if(n.g){if(!(n.g.F+3e3<t.F))break t;gl(n),rl(n)}fl(n),Eo(18)}}else n.ta=r[1],0<n.ta-n.U&&37500>r[2]&&n.N&&0==n.A&&!n.v&&(n.v=So(jr(n.ab,n),6e3));if(1>=Ta(n.i)&&n.ka){try{n.ka()}catch(t){}n.ka=void 0}}else vl(n,11)}else if((t.J||n.g==t)&&gl(n),!Kr(e))for(r=n.Ca.g.parse(e),e=0;e<r.length;e++){let h=r[e];if(n.U=h[0],h=h[1],2==n.G)if("c"==h[0]){n.J=h[1],n.la=h[2];const e=h[3];null!=e&&(n.ma=e,n.h.info("VER="+n.ma));const r=h[4];null!=r&&(n.za=r,n.h.info("SVER="+n.za));const c=h[5];null!=c&&"number"==typeof c&&0<c&&(i=1.5*c,n.K=i,n.h.info("backChannelRequestTimeoutMs_="+i)),i=n;const u=t.g;if(u){const t=u.g?u.g.getResponseHeader("X-Client-Wire-Protocol"):null;if(t){var s=i.i;!s.g&&(Xr(t,"spdy")||Xr(t,"quic")||Xr(t,"h2"))&&(s.j=s.l,s.g=new Set,s.h&&(Ia(s,s.h),s.h=null))}if(i.D){const t=u.g?u.g.getResponseHeader("X-HTTP-Session-Id"):null;t&&(i.sa=t,aa(i.F,i.D,t))}}n.G=3,n.j&&n.j.xa(),n.$&&(n.O=Date.now()-t.F,n.h.info("Handshake RTT: "+n.O+"ms"));var o=t;if((i=n).oa=_l(i,i.H?i.la:null,i.W),o.J){Da(i.i,o);var a=o,l=i.K;l&&a.setTimeout(l),a.B&&(Ko(a),zo(a)),i.g=o}else ul(i);0<n.l.length&&al(n)}else"stop"!=h[0]&&"close"!=h[0]||vl(n,7);else 3==n.G&&("stop"==h[0]||"close"==h[0]?"stop"==h[0]?vl(n,7):il(n):"noop"!=h[0]&&n.j&&n.j.wa(h),n.A=0)}bo(4)}catch(t){}}function Yo(t,e){if(t.forEach&&"function"==typeof t.forEach)t.forEach(e,void 0);else if(Lr(t)||"string"==typeof t)Vr(t,e,void 0);else{if(t.T&&"function"==typeof t.T)var n=t.T();else if(t.R&&"function"==typeof t.R)n=void 0;else if(Lr(t)||"string"==typeof t){n=[];for(var i=t.length,r=0;r<i;r++)n.push(r)}else for(r in n=[],i=0,t)n[i++]=r;i=function(t){if(t.R&&"function"==typeof t.R)return t.R();if("string"==typeof t)return t.split("");if(Lr(t)){for(var e=[],n=t.length,i=0;i<n;i++)e.push(t[i]);return e}for(i in e=[],n=0,t)e[n++]=t[i];return e}(t),r=i.length;for(var s=0;s<r;s++)e.call(void 0,i[s],n&&n[s],t)}}function Jo(t,e){this.h={},this.g=[],this.i=0;var n=arguments.length;if(1<n){if(n%2)throw Error("Uneven number of arguments");for(var i=0;i<n;i+=2)this.set(arguments[i],arguments[i+1])}else if(t)if(t instanceof Jo)for(n=t.T(),i=0;i<n.length;i++)this.set(n[i],t.get(n[i]));else for(i in t)this.set(i,t[i])}function Qo(t){if(t.i!=t.g.length){for(var e=0,n=0;e<t.g.length;){var i=t.g[e];Zo(t.h,i)&&(t.g[n++]=i),e++}t.g.length=n}if(t.i!=t.g.length){var r={};for(n=e=0;e<t.g.length;)Zo(r,i=t.g[e])||(t.g[n++]=i,r[i]=1),e++;t.g.length=n}}function Zo(t,e){return Object.prototype.hasOwnProperty.call(t,e)}(xr=Ro.prototype).setTimeout=function(t){this.P=t},xr.gb=function(t){t=t.target;const e=this.L;e&&3==Qa(t)?e.l():this.Ia(t)},xr.Ia=function(t){try{if(t==this.g)t:{const c=Qa(this.g);var e=this.g.Da();const u=this.g.ba();if(!(3>c)&&(3!=c||hs||this.g&&(this.h.h||this.g.ga()||Za(this.g)))){this.I||4!=c||7==e||bo(8==e||0>=u?3:2),Ko(this);var n=this.g.ba();this.N=n;e:if(Uo(this)){var i=Za(this.g);t="";var r=i.length,s=4==Qa(this.g);if(!this.h.i){if("undefined"==typeof TextDecoder){qo(this),Go(this);var o="";break e}this.h.i=new Dr.TextDecoder}for(e=0;e<r;e++)this.h.h=!0,t+=this.h.i.decode(i[e],{stream:s&&e==r-1});i.splice(0,r),this.h.g+=t,this.C=0,o=this.h.g}else o=this.g.ga();if(this.i=200==n,function(t,e,n,i,r,s,o){t.info((function(){return"XMLHTTP RESP ("+i+") [ attempt "+r+"]: "+e+"\n"+n+"\n"+s+" "+o}))}(this.j,this.u,this.A,this.m,this.X,c,n),this.i){if(this.$&&!this.J){e:{if(this.g){var a,l=this.g;if((a=l.g?l.g.getResponseHeader("X-HTTP-Initial-Response"):null)&&!Kr(a)){var h=a;break e}}h=null}if(!(n=h)){this.i=!1,this.o=3,Eo(12),qo(this),Go(this);break t}po(this.j,this.m,n,"Initial handshake response via X-HTTP-Initial-Response"),this.J=!0,Xo(this,n)}this.U?(Ho(this,c,o),hs&&this.i&&3==c&&(co(this.V,this.W,"tick",this.fb),this.W.start())):(po(this.j,this.m,o,null),Xo(this,o)),4==c&&qo(this),this.i&&!this.I&&(4==c?ml(this.l,this):(this.i=!1,zo(this)))}else 400==n&&0<o.indexOf("Unknown SID")?(this.o=3,Eo(12)):(this.o=0,Eo(13)),qo(this),Go(this)}}}catch(t){}},xr.fb=function(){if(this.g){var t=Qa(this.g),e=this.g.ga();this.C<e.length&&(Ko(this),Ho(this,t,e),this.i&&4!=t&&zo(this))}},xr.cancel=function(){this.I=!0,qo(this)},xr.eb=function(){this.B=null;const t=Date.now();0<=t-this.Y?(function(t,e){t.info((function(){return"TIMEOUT: "+e}))}(this.j,this.A),2!=this.K&&(bo(3),Eo(17)),qo(this),this.o=2,Go(this)):Wo(this,this.Y-t)},(xr=Jo.prototype).R=function(){Qo(this);for(var t=[],e=0;e<this.g.length;e++)t.push(this.h[this.g[e]]);return t},xr.T=function(){return Qo(this),this.g.concat()},xr.get=function(t,e){return Zo(this.h,t)?this.h[t]:e},xr.set=function(t,e){Zo(this.h,t)||(this.i++,this.g.push(t)),this.h[t]=e},xr.forEach=function(t,e){for(var n=this.T(),i=0;i<n.length;i++){var r=n[i],s=this.get(r);t.call(e,s,r,this)}};var ta=/^(?:([^:/?#.]+):)?(?:\/\/(?:([^\\/?#]*)@)?([^\\/?#]*?)(?::([0-9]+))?(?=[\\/?#]|$))?([^?#]+)?(?:\?([^#]*))?(?:#([\s\S]*))?$/;function ea(t,e){if(this.i=this.s=this.j="",this.m=null,this.o=this.l="",this.g=!1,t instanceof ea){this.g=void 0!==e?e:t.g,ia(this,t.j),this.s=t.s,ra(this,t.i),sa(this,t.m),this.l=t.l,e=t.h;var n=new ya;n.i=e.i,e.g&&(n.g=new Jo(e.g),n.h=e.h),oa(this,n),this.o=t.o}else t&&(n=String(t).match(ta))?(this.g=!!e,ia(this,n[1]||"",!0),this.s=ha(n[2]||""),ra(this,n[3]||"",!0),sa(this,n[4]),this.l=ha(n[5]||"",!0),oa(this,n[6]||"",!0),this.o=ha(n[7]||"")):(this.g=!!e,this.h=new ya(null,this.g))}function na(t){return new ea(t)}function ia(t,e,n){t.j=n?ha(e,!0):e,t.j&&(t.j=t.j.replace(/:$/,""))}function ra(t,e,n){t.i=n?ha(e,!0):e}function sa(t,e){if(e){if(e=Number(e),isNaN(e)||0>e)throw Error("Bad port number "+e);t.m=e}else t.m=null}function oa(t,e,n){e instanceof ya?(t.h=e,function(t,e){e&&!t.j&&(va(t),t.i=null,t.g.forEach((function(t,e){var n=e.toLowerCase();e!=n&&(ba(this,e),Ea(this,n,t))}),t)),t.j=e}(t.h,t.g)):(n||(e=ca(e,ga)),t.h=new ya(e,t.g))}function aa(t,e,n){t.h.set(e,n)}function la(t){return aa(t,"zx",Math.floor(2147483648*Math.random()).toString(36)+Math.abs(Math.floor(2147483648*Math.random())^Date.now()).toString(36)),t}function ha(t,e){return t?e?decodeURI(t.replace(/%25/g,"%2525")):decodeURIComponent(t):""}function ca(t,e,n){return"string"==typeof t?(t=encodeURI(t).replace(e,ua),n&&(t=t.replace(/%25([0-9a-fA-F]{2})/g,"%$1")),t):null}function ua(t){return"%"+((t=t.charCodeAt(0))>>4&15).toString(16)+(15&t).toString(16)}ea.prototype.toString=function(){var t=[],e=this.j;e&&t.push(ca(e,fa,!0),":");var n=this.i;return(n||"file"==e)&&(t.push("//"),(e=this.s)&&t.push(ca(e,fa,!0),"@"),t.push(encodeURIComponent(String(n)).replace(/%25([0-9a-fA-F]{2})/g,"%$1")),null!=(n=this.m)&&t.push(":",String(n))),(n=this.l)&&(this.i&&"/"!=n.charAt(0)&&t.push("/"),t.push(ca(n,"/"==n.charAt(0)?pa:da,!0))),(n=this.h.toString())&&t.push("?",n),(n=this.o)&&t.push("#",ca(n,ma)),t.join("")};var fa=/[#\/\?@]/g,da=/[#\?:]/g,pa=/[#\?]/g,ga=/[#\?@]/g,ma=/#/g;function ya(t,e){this.h=this.g=null,this.i=t||null,this.j=!!e}function va(t){t.g||(t.g=new Jo,t.h=0,t.i&&function(t,e){if(t){t=t.split("&");for(var n=0;n<t.length;n++){var i=t[n].indexOf("="),r=null;if(0<=i){var s=t[n].substring(0,i);r=t[n].substring(i+1)}else s=t[n];e(s,r?decodeURIComponent(r.replace(/\+/g," ")):"")}}}(t.i,(function(e,n){t.add(decodeURIComponent(e.replace(/\+/g," ")),n)})))}function ba(t,e){va(t),e=wa(t,e),Zo(t.g.h,e)&&(t.i=null,t.h-=t.g.get(e).length,Zo((t=t.g).h,e)&&(delete t.h[e],t.i--,t.g.length>2*t.i&&Qo(t)))}function _a(t,e){return va(t),e=wa(t,e),Zo(t.g.h,e)}function Ea(t,e,n){ba(t,e),0<n.length&&(t.i=null,t.g.set(wa(t,e),Wr(n)),t.h+=n.length)}function wa(t,e){return e=String(e),t.j&&(e=e.toLowerCase()),e}(xr=ya.prototype).add=function(t,e){va(this),this.i=null,t=wa(this,t);var n=this.g.get(t);return n||this.g.set(t,n=[]),n.push(e),this.h+=1,this},xr.forEach=function(t,e){va(this),this.g.forEach((function(n,i){Vr(n,(function(n){t.call(e,n,i,this)}),this)}),this)},xr.T=function(){va(this);for(var t=this.g.R(),e=this.g.T(),n=[],i=0;i<e.length;i++)for(var r=t[i],s=0;s<r.length;s++)n.push(e[i]);return n},xr.R=function(t){va(this);var e=[];if("string"==typeof t)_a(this,t)&&(e=zr(e,this.g.get(wa(this,t))));else{t=this.g.R();for(var n=0;n<t.length;n++)e=zr(e,t[n])}return e},xr.set=function(t,e){return va(this),this.i=null,_a(this,t=wa(this,t))&&(this.h-=this.g.get(t).length),this.g.set(t,[e]),this.h+=1,this},xr.get=function(t,e){return t&&0<(t=this.R(t)).length?String(t[0]):e},xr.toString=function(){if(this.i)return this.i;if(!this.g)return"";for(var t=[],e=this.g.T(),n=0;n<e.length;n++){var i=e[n],r=encodeURIComponent(String(i));i=this.R(i);for(var s=0;s<i.length;s++){var o=r;""!==i[s]&&(o+="="+encodeURIComponent(String(i[s]))),t.push(o)}}return this.i=t.join("&")};function Sa(t){this.l=t||Aa,Dr.PerformanceNavigationTiming?t=0<(t=Dr.performance.getEntriesByType("navigation")).length&&("hq"==t[0].nextHopProtocol||"h2"==t[0].nextHopProtocol):t=!!(Dr.g&&Dr.g.Ea&&Dr.g.Ea()&&Dr.g.Ea().Zb),this.j=t?this.l:1,this.g=null,1<this.j&&(this.g=new Set),this.h=null,this.i=[]}var Aa=10;function xa(t){return!!t.h||!!t.g&&t.g.size>=t.j}function Ta(t){return t.h?1:t.g?t.g.size:0}function Ca(t,e){return t.h?t.h==e:!!t.g&&t.g.has(e)}function Ia(t,e){t.g?t.g.add(e):t.h=e}function Da(t,e){t.h&&t.h==e?t.h=null:t.g&&t.g.has(e)&&t.g.delete(e)}function Oa(t){if(null!=t.h)return t.i.concat(t.h.D);if(null!=t.g&&0!==t.g.size){let e=t.i;for(const n of t.g.values())e=e.concat(n.D);return e}return Wr(t.i)}function La(){}function ka(){this.g=new La}function Na(t,e,n){const i=n||"";try{Yo(t,(function(t,n){let r=t;kr(t)&&(r=Gs(t)),e.push(i+n+"="+encodeURIComponent(r))}))}catch(t){throw e.push(i+"type="+encodeURIComponent("_badmap")),t}}function Ra(t,e,n,i,r){try{e.onload=null,e.onerror=null,e.onabort=null,e.ontimeout=null,r(i)}catch(t){}}function Pa(t){this.l=t.$b||null,this.j=t.ib||!1}function Ma(t,e){zs.call(this),this.D=t,this.u=e,this.m=void 0,this.readyState=ja,this.status=0,this.responseType=this.responseText=this.response=this.statusText="",this.onreadystatechange=null,this.v=new Headers,this.h=null,this.C="GET",this.B="",this.g=!1,this.A=this.j=this.l=null}Sa.prototype.cancel=function(){if(this.i=Oa(this),this.h)this.h.cancel(),this.h=null;else if(this.g&&0!==this.g.size){for(const t of this.g.values())t.cancel();this.g.clear()}},La.prototype.stringify=function(t){return Dr.JSON.stringify(t,void 0)},La.prototype.parse=function(t){return Dr.JSON.parse(t,void 0)},Fr(Pa,To),Pa.prototype.g=function(){return new Ma(this.l,this.j)},Pa.prototype.i=function(t){return function(){return t}}({}),Fr(Ma,zs);var ja=0;function Ba(t){t.j.read().then(t.Sa.bind(t)).catch(t.ha.bind(t))}function Fa(t){t.readyState=4,t.l=null,t.j=null,t.A=null,$a(t)}function $a(t){t.onreadystatechange&&t.onreadystatechange.call(t)}(xr=Ma.prototype).open=function(t,e){if(this.readyState!=ja)throw this.abort(),Error("Error reopening a connection");this.C=t,this.B=e,this.readyState=1,$a(this)},xr.send=function(t){if(1!=this.readyState)throw this.abort(),Error("need to call open() first. ");this.g=!0;const e={headers:this.v,method:this.C,credentials:this.m,cache:void 0};t&&(e.body=t),(this.D||Dr).fetch(new Request(this.B,e)).then(this.Va.bind(this),this.ha.bind(this))},xr.abort=function(){this.response=this.responseText="",this.v=new Headers,this.status=0,this.j&&this.j.cancel("Request was aborted."),1<=this.readyState&&this.g&&4!=this.readyState&&(this.g=!1,Fa(this)),this.readyState=ja},xr.Va=function(t){if(this.g&&(this.l=t,this.h||(this.status=this.l.status,this.statusText=this.l.statusText,this.h=t.headers,this.readyState=2,$a(this)),this.g&&(this.readyState=3,$a(this),this.g)))if("arraybuffer"===this.responseType)t.arrayBuffer().then(this.Ta.bind(this),this.ha.bind(this));else if(void 0!==Dr.ReadableStream&&"body"in t){if(this.j=t.body.getReader(),this.u){if(this.responseType)throw Error('responseType must be empty for "streamBinaryChunks" mode responses.');this.response=[]}else this.response=this.responseText="",this.A=new TextDecoder;Ba(this)}else t.text().then(this.Ua.bind(this),this.ha.bind(this))},xr.Sa=function(t){if(this.g){if(this.u&&t.value)this.response.push(t.value);else if(!this.u){var e=t.value?t.value:new Uint8Array(0);(e=this.A.decode(e,{stream:!t.done}))&&(this.response=this.responseText+=e)}t.done?Fa(this):$a(this),3==this.readyState&&Ba(this)}},xr.Ua=function(t){this.g&&(this.response=this.responseText=t,Fa(this))},xr.Ta=function(t){this.g&&(this.response=t,Fa(this))},xr.ha=function(){this.g&&Fa(this)},xr.setRequestHeader=function(t,e){this.v.append(t,e)},xr.getResponseHeader=function(t){return this.h&&this.h.get(t.toLowerCase())||""},xr.getAllResponseHeaders=function(){if(!this.h)return"";const t=[],e=this.h.entries();for(var n=e.next();!n.done;)n=n.value,t.push(n[0]+": "+n[1]),n=e.next();return t.join("\r\n")},Object.defineProperty(Ma.prototype,"withCredentials",{get:function(){return"include"===this.m},set:function(t){this.m=t?"include":"same-origin"}});var Ua=Dr.JSON.parse;function Ha(t){zs.call(this),this.headers=new Jo,this.u=t||null,this.h=!1,this.C=this.g=null,this.H="",this.m=0,this.j="",this.l=this.F=this.v=this.D=!1,this.B=0,this.A=null,this.J=Va,this.K=this.L=!1}Fr(Ha,zs);var Va="",za=/^https?$/i,Wa=["POST","PUT"];function Ka(t){return"content-type"==t.toLowerCase()}function Ga(t,e){t.h=!1,t.g&&(t.l=!0,t.g.abort(),t.l=!1),t.j=e,t.m=5,qa(t),Ya(t)}function qa(t){t.D||(t.D=!0,Ws(t,"complete"),Ws(t,"error"))}function Xa(t){if(t.h&&void 0!==Ir&&(!t.C[1]||4!=Qa(t)||2!=t.ba()))if(t.v&&4==Qa(t))so(t.Fa,0,t);else if(Ws(t,"readystatechange"),4==Qa(t)){t.h=!1;try{const a=t.ba();t:switch(a){case 200:case 201:case 202:case 204:case 206:case 304:case 1223:var e=!0;break t;default:e=!1}var n;if(!(n=e)){var i;if(i=0===a){var r=String(t.H).match(ta)[1]||null;if(!r&&Dr.self&&Dr.self.location){var s=Dr.self.location.protocol;r=s.substr(0,s.length-1)}i=!za.test(r?r.toLowerCase():"")}n=i}if(n)Ws(t,"complete"),Ws(t,"success");else{t.m=6;try{var o=2<Qa(t)?t.g.statusText:""}catch(t){o=""}t.j=o+" ["+t.ba()+"]",qa(t)}}finally{Ya(t)}}}function Ya(t,e){if(t.g){Ja(t);const n=t.g,i=t.C[0]?Or:null;t.g=null,t.C=null,e||Ws(t,"ready");try{n.onreadystatechange=i}catch(t){}}}function Ja(t){t.g&&t.K&&(t.g.ontimeout=null),t.A&&(Dr.clearTimeout(t.A),t.A=null)}function Qa(t){return t.g?t.g.readyState:0}function Za(t){try{if(!t.g)return null;if("response"in t.g)return t.g.response;switch(t.J){case Va:case"text":return t.g.responseText;case"arraybuffer":if("mozResponseArrayBuffer"in t.g)return t.g.mozResponseArrayBuffer}return null}catch(t){return null}}function tl(t,e,n){t:{for(i in n){var i=!1;break t}i=!0}i||(n=function(t){let e="";return Zr(t,(function(t,n){e+=n,e+=":",e+=t,e+="\r\n"})),e}(n),"string"==typeof t?null!=n&&encodeURIComponent(String(n)):aa(t,e,n))}function el(t,e,n){return n&&n.internalChannelParams&&n.internalChannelParams[t]||e}function nl(t){this.za=0,this.l=[],this.h=new fo,this.la=this.oa=this.F=this.W=this.g=this.sa=this.D=this.aa=this.o=this.P=this.s=null,this.Za=this.V=0,this.Xa=el("failFast",!1,t),this.N=this.v=this.u=this.m=this.j=null,this.X=!0,this.I=this.ta=this.U=-1,this.Y=this.A=this.C=0,this.Pa=el("baseRetryDelayMs",5e3,t),this.$a=el("retryDelaySeedMs",1e4,t),this.Ya=el("forwardChannelMaxRetries",2,t),this.ra=el("forwardChannelRequestTimeoutMs",2e4,t),this.qa=t&&t.xmlHttpFactory||void 0,this.Ba=t&&t.Yb||!1,this.K=void 0,this.H=t&&t.supportsCrossDomainXhr||!1,this.J="",this.i=new Sa(t&&t.concurrentRequestLimit),this.Ca=new ka,this.ja=t&&t.fastHandshake||!1,this.Ra=t&&t.Wb||!1,t&&t.Aa&&this.h.Aa(),t&&t.forceLongPolling&&(this.X=!1),this.$=!this.ja&&this.X&&t&&t.detectBufferingProxy||!1,this.ka=void 0,this.O=0,this.L=!1,this.B=null,this.Wa=!t||!1!==t.Xb}function il(t){if(sl(t),3==t.G){var e=t.V++,n=na(t.F);aa(n,"SID",t.J),aa(n,"RID",e),aa(n,"TYPE","terminate"),hl(t,n),(e=new Ro(t,t.h,e,void 0)).K=2,e.v=la(na(n)),n=!1,Dr.navigator&&Dr.navigator.sendBeacon&&(n=Dr.navigator.sendBeacon(e.v.toString(),"")),!n&&Dr.Image&&((new Image).src=e.v,n=!0),n||(e.g=El(e.l,null),e.g.ea(e.v)),e.F=Date.now(),zo(e)}bl(t)}function rl(t){t.g&&(dl(t),t.g.cancel(),t.g=null)}function sl(t){rl(t),t.u&&(Dr.clearTimeout(t.u),t.u=null),gl(t),t.i.cancel(),t.m&&("number"==typeof t.m&&Dr.clearTimeout(t.m),t.m=null)}function ol(t,e){t.l.push(new class{constructor(t,e){this.h=t,this.g=e}}(t.Za++,e)),3==t.G&&al(t)}function al(t){xa(t.i)||t.m||(t.m=!0,Zs(t.Ha,t),t.C=0)}function ll(t,e){var n;n=e?e.m:t.V++;const i=na(t.F);aa(i,"SID",t.J),aa(i,"RID",n),aa(i,"AID",t.U),hl(t,i),t.o&&t.s&&tl(i,t.o,t.s),n=new Ro(t,t.h,n,t.C+1),null===t.o&&(n.H=t.s),e&&(t.l=e.D.concat(t.l)),e=cl(t,n,1e3),n.setTimeout(Math.round(.5*t.ra)+Math.round(.5*t.ra*Math.random())),Ia(t.i,n),Fo(n,i,e)}function hl(t,e){t.j&&Yo({},(function(t,n){aa(e,n,t)}))}function cl(t,e,n){n=Math.min(t.l.length,n);var i=t.j?jr(t.j.Oa,t.j,t):null;t:{var r=t.l;let e=-1;for(;;){const t=["count="+n];-1==e?0<n?(e=r[0].h,t.push("ofs="+e)):e=0:t.push("ofs="+e);let s=!0;for(let o=0;o<n;o++){let n=r[o].h;const a=r[o].g;if(n-=e,0>n)e=Math.max(0,r[o].h-100),s=!1;else try{Na(a,t,"req"+n+"_")}catch(t){i&&i(a)}}if(s){i=t.join("&");break t}}}return t=t.l.splice(0,n),e.D=t,i}function ul(t){t.g||t.u||(t.Y=1,Zs(t.Ga,t),t.A=0)}function fl(t){return!(t.g||t.u||3<=t.A)&&(t.Y++,t.u=So(jr(t.Ga,t),yl(t,t.A)),t.A++,!0)}function dl(t){null!=t.B&&(Dr.clearTimeout(t.B),t.B=null)}function pl(t){t.g=new Ro(t,t.h,"rpc",t.Y),null===t.o&&(t.g.H=t.s),t.g.O=0;var e=na(t.oa);aa(e,"RID","rpc"),aa(e,"SID",t.J),aa(e,"CI",t.N?"0":"1"),aa(e,"AID",t.U),hl(t,e),aa(e,"TYPE","xmlhttp"),t.o&&t.s&&tl(e,t.o,t.s),t.K&&t.g.setTimeout(t.K);var n=t.g;t=t.la,n.K=1,n.v=la(na(e)),n.s=null,n.U=!0,$o(n,t)}function gl(t){null!=t.v&&(Dr.clearTimeout(t.v),t.v=null)}function ml(t,e){var n=null;if(t.g==e){gl(t),dl(t),t.g=null;var i=2}else{if(!Ca(t.i,e))return;n=e.D,Da(t.i,e),i=1}if(t.I=e.N,0!=t.G)if(e.i)if(1==i){n=e.s?e.s.length:0,e=Date.now()-e.F;var r=t.C;Ws(i=yo(),new wo(i,n,e,r)),al(t)}else ul(t);else if(3==(r=e.o)||0==r&&0<t.I||!(1==i&&function(t,e){return!(Ta(t.i)>=t.i.j-(t.m?1:0)||(t.m?(t.l=e.D.concat(t.l),0):1==t.G||2==t.G||t.C>=(t.Xa?0:t.Ya)||(t.m=So(jr(t.Ha,t,e),yl(t,t.C)),t.C++,0)))}(t,e)||2==i&&fl(t)))switch(n&&0<n.length&&(e=t.i,e.i=e.i.concat(n)),r){case 1:vl(t,5);break;case 4:vl(t,10);break;case 3:vl(t,6);break;default:vl(t,2)}}function yl(t,e){let n=t.Pa+Math.floor(Math.random()*t.$a);return t.j||(n*=2),n*e}function vl(t,e){if(t.h.info("Error code "+e),2==e){var n=null;t.j&&(n=null);var i=jr(t.jb,t);n||(n=new ea("//www.google.com/images/cleardot.gif"),Dr.location&&"http"==Dr.location.protocol||ia(n,"https"),la(n)),function(t,e){const n=new fo;if(Dr.Image){const i=new Image;i.onload=Br(Ra,n,i,"TestLoadImage: loaded",!0,e),i.onerror=Br(Ra,n,i,"TestLoadImage: error",!1,e),i.onabort=Br(Ra,n,i,"TestLoadImage: abort",!1,e),i.ontimeout=Br(Ra,n,i,"TestLoadImage: timeout",!1,e),Dr.setTimeout((function(){i.ontimeout&&i.ontimeout()}),1e4),i.src=t}else e(!1)}(n.toString(),i)}else Eo(2);t.G=0,t.j&&t.j.va(e),bl(t),sl(t)}function bl(t){t.G=0,t.I=-1,t.j&&(0==Oa(t.i).length&&0==t.l.length||(t.i.i.length=0,Wr(t.l),t.l.length=0),t.j.ua())}function _l(t,e,n){let i=function(t){return t instanceof ea?na(t):new ea(t,void 0)}(n);if(""!=i.i)e&&ra(i,e+"."+i.i),sa(i,i.m);else{const t=Dr.location;i=function(t,e,n,i){var r=new ea(null,void 0);return t&&ia(r,t),e&&ra(r,e),n&&sa(r,n),i&&(r.l=i),r}(t.protocol,e?e+"."+t.hostname:t.hostname,+t.port,n)}return t.aa&&Zr(t.aa,(function(t,e){aa(i,e,t)})),e=t.D,n=t.sa,e&&n&&aa(i,e,n),aa(i,"VER",t.ma),hl(t,i),i}function El(t,e,n){if(e&&!t.H)throw Error("Can't create secondary domain capable XhrIo object.");return(e=n&&t.Ba&&!t.qa?new Ha(new Pa({ib:!0})):new Ha(t.qa)).L=t.H,e}function wl(){}function Sl(){if(as&&!(10<=Number(_s)))throw Error("Environmental error: no available transport.")}function Al(t,e){zs.call(this),this.g=new nl(e),this.l=t,this.h=e&&e.messageUrlParams||null,t=e&&e.messageHeaders||null,e&&e.clientProtocolHeaderRequired&&(t?t["X-Client-Protocol"]="webchannel":t={"X-Client-Protocol":"webchannel"}),this.g.s=t,t=e&&e.initMessageHeaders||null,e&&e.messageContentType&&(t?t["X-WebChannel-Content-Type"]=e.messageContentType:t={"X-WebChannel-Content-Type":e.messageContentType}),e&&e.ya&&(t?t["X-WebChannel-Client-Profile"]=e.ya:t={"X-WebChannel-Client-Profile":e.ya}),this.g.P=t,(t=e&&e.httpHeadersOverwriteParam)&&!Kr(t)&&(this.g.o=t),this.A=e&&e.supportsCrossDomainXhr||!1,this.v=e&&e.sendRawJson||!1,(e=e&&e.httpSessionIdParam)&&!Kr(e)&&(this.g.D=e,null!==(t=this.h)&&e in t&&(e in(t=this.h)&&delete t[e])),this.j=new Cl(this)}function xl(t){Lo.call(this);var e=t.__sm__;if(e){t:{for(const n in e){t=n;break t}t=void 0}(this.i=t)&&(t=this.i,e=null!==e&&t in e?e[t]:void 0),this.data=e}else this.data=t}function Tl(){ko.call(this),this.status=1}function Cl(t){this.g=t}(xr=Ha.prototype).ea=function(t,e,n,i){if(this.g)throw Error("[goog.net.XhrIo] Object is active with another request="+this.H+"; newUri="+t);e=e?e.toUpperCase():"GET",this.H=t,this.j="",this.m=0,this.D=!1,this.h=!0,this.g=this.u?this.u.g():Do.g(),this.C=this.u?Co(this.u):Co(Do),this.g.onreadystatechange=jr(this.Fa,this);try{this.F=!0,this.g.open(e,String(t),!0),this.F=!1}catch(t){return void Ga(this,t)}t=n||"";const r=new Jo(this.headers);i&&Yo(i,(function(t,e){r.set(e,t)})),i=function(t){t:{var e=Ka;const n=t.length,i="string"==typeof t?t.split(""):t;for(let r=0;r<n;r++)if(r in i&&e.call(void 0,i[r],r,t)){e=r;break t}e=-1}return 0>e?null:"string"==typeof t?t.charAt(e):t[e]}(r.T()),n=Dr.FormData&&t instanceof Dr.FormData,!(0<=Hr(Wa,e))||i||n||r.set("Content-Type","application/x-www-form-urlencoded;charset=utf-8"),r.forEach((function(t,e){this.g.setRequestHeader(e,t)}),this),this.J&&(this.g.responseType=this.J),"withCredentials"in this.g&&this.g.withCredentials!==this.L&&(this.g.withCredentials=this.L);try{Ja(this),0<this.B&&((this.K=function(t){return as&&vs()&&"number"==typeof t.timeout&&void 0!==t.ontimeout}(this.g))?(this.g.timeout=this.B,this.g.ontimeout=jr(this.pa,this)):this.A=so(this.pa,this.B,this)),this.v=!0,this.g.send(t),this.v=!1}catch(t){Ga(this,t)}},xr.pa=function(){void 0!==Ir&&this.g&&(this.j="Timed out after "+this.B+"ms, aborting",this.m=8,Ws(this,"timeout"),this.abort(8))},xr.abort=function(t){this.g&&this.h&&(this.h=!1,this.l=!0,this.g.abort(),this.l=!1,this.m=t||7,Ws(this,"complete"),Ws(this,"abort"),Ya(this))},xr.M=function(){this.g&&(this.h&&(this.h=!1,this.l=!0,this.g.abort(),this.l=!1),Ya(this,!0)),Ha.Z.M.call(this)},xr.Fa=function(){this.s||(this.F||this.v||this.l?Xa(this):this.cb())},xr.cb=function(){Xa(this)},xr.ba=function(){try{return 2<Qa(this)?this.g.status:-1}catch(t){return-1}},xr.ga=function(){try{return this.g?this.g.responseText:""}catch(t){return""}},xr.Qa=function(t){if(this.g){var e=this.g.responseText;return t&&0==e.indexOf(t)&&(e=e.substring(t.length)),Ua(e)}},xr.Da=function(){return this.m},xr.La=function(){return"string"==typeof this.j?this.j:String(this.j)},(xr=nl.prototype).ma=8,xr.G=1,xr.hb=function(t){try{this.h.info("Origin Trials invoked: "+t)}catch(t){}},xr.Ha=function(t){if(this.m)if(this.m=null,1==this.G){if(!t){this.V=Math.floor(1e5*Math.random()),t=this.V++;const r=new Ro(this,this.h,t,void 0);let s=this.s;if(this.P&&(s?(s=ts(s),ns(s,this.P)):s=this.P),null===this.o&&(r.H=s),this.ja)t:{for(var e=0,n=0;n<this.l.length;n++){var i=this.l[n];if(void 0===(i="__data__"in i.g&&"string"==typeof(i=i.g.__data__)?i.length:void 0))break;if(4096<(e+=i)){e=n;break t}if(4096===e||n===this.l.length-1){e=n+1;break t}}e=1e3}else e=1e3;e=cl(this,r,e),aa(n=na(this.F),"RID",t),aa(n,"CVER",22),this.D&&aa(n,"X-HTTP-Session-Id",this.D),hl(this,n),this.o&&s&&tl(n,this.o,s),Ia(this.i,r),this.Ra&&aa(n,"TYPE","init"),this.ja?(aa(n,"$req",e),aa(n,"SID","null"),r.$=!0,Fo(r,n,null)):Fo(r,n,e),this.G=2}}else 3==this.G&&(t?ll(this,t):0==this.l.length||xa(this.i)||ll(this))},xr.Ga=function(){if(this.u=null,pl(this),this.$&&!(this.L||null==this.g||0>=this.O)){var t=2*this.O;this.h.info("BP detection timer enabled: "+t),this.B=So(jr(this.bb,this),t)}},xr.bb=function(){this.B&&(this.B=null,this.h.info("BP detection timeout reached."),this.h.info("Buffering proxy detected and switch to long-polling!"),this.N=!1,this.L=!0,Eo(10),rl(this),pl(this))},xr.ab=function(){null!=this.v&&(this.v=null,rl(this),fl(this),Eo(19))},xr.jb=function(t){t?(this.h.info("Successfully pinged google.com"),Eo(2)):(this.h.info("Failed to ping google.com"),Eo(1))},(xr=wl.prototype).xa=function(){},xr.wa=function(){},xr.va=function(){},xr.ua=function(){},xr.Oa=function(){},Sl.prototype.g=function(t,e){return new Al(t,e)},Fr(Al,zs),Al.prototype.m=function(){this.g.j=this.j,this.A&&(this.g.H=!0);var t=this.g,e=this.l,n=this.h||void 0;t.Wa&&(t.h.info("Origin Trials enabled."),Zs(jr(t.hb,t,e))),Eo(0),t.W=e,t.aa=n||{},t.N=t.X,t.F=_l(t,null,t.W),al(t)},Al.prototype.close=function(){il(this.g)},Al.prototype.u=function(t){if("string"==typeof t){var e={};e.__data__=t,ol(this.g,e)}else this.v?((e={}).__data__=Gs(t),ol(this.g,e)):ol(this.g,t)},Al.prototype.M=function(){this.g.j=null,delete this.j,il(this.g),delete this.g,Al.Z.M.call(this)},Fr(xl,Lo),Fr(Tl,ko),Fr(Cl,wl),Cl.prototype.xa=function(){Ws(this.g,"a")},Cl.prototype.wa=function(t){Ws(this.g,new xl(t))},Cl.prototype.va=function(t){Ws(this.g,new Tl(t))},Cl.prototype.ua=function(){Ws(this.g,"b")},Sl.prototype.createWebChannel=Sl.prototype.g,Al.prototype.send=Al.prototype.u,Al.prototype.open=Al.prototype.m,Al.prototype.close=Al.prototype.close,Ao.NO_ERROR=0,Ao.TIMEOUT=8,Ao.HTTP_ERROR=6,xo.COMPLETE="complete",Io.EventType=Oo,Oo.OPEN="a",Oo.CLOSE="b",Oo.ERROR="c",Oo.MESSAGE="d",zs.prototype.listen=zs.prototype.N,Ha.prototype.listenOnce=Ha.prototype.O,Ha.prototype.getLastError=Ha.prototype.La,Ha.prototype.getLastErrorCode=Ha.prototype.Da,Ha.prototype.getStatus=Ha.prototype.ba,Ha.prototype.getResponseJson=Ha.prototype.Qa,Ha.prototype.getResponseText=Ha.prototype.ga,Ha.prototype.send=Ha.prototype.ea;Cr.createWebChannelTransport=function(){return new Sl},Cr.getStatEventTarget=function(){return yo()},Cr.ErrorCode=Ao,Cr.EventType=xo,Cr.Event=go,Cr.Stat={rb:0,ub:1,vb:2,Ob:3,Tb:4,Qb:5,Rb:6,Pb:7,Nb:8,Sb:9,PROXY:10,NOPROXY:11,Lb:12,Hb:13,Ib:14,Gb:15,Jb:16,Kb:17,nb:18,mb:19,ob:20},Cr.FetchXmlHttpFactory=Pa,Cr.WebChannel=Io,Cr.XhrIo=Ha;var Il,Dl,Ol={};function Ll(){throw new Error("setTimeout has not been defined")}function kl(){throw new Error("clearTimeout has not been defined")}function Nl(t){if(Il===setTimeout)return setTimeout(t,0);if((Il===Ll||!Il)&&setTimeout)return Il=setTimeout,setTimeout(t,0);try{return Il(t,0)}catch(e){try{return Il.call(null,t,0)}catch(e){return Il.call(this,t,0)}}}!function(){try{Il="function"==typeof setTimeout?setTimeout:Ll}catch(t){Il=Ll}try{Dl="function"==typeof clearTimeout?clearTimeout:kl}catch(t){Dl=kl}}();var Rl,Pl=[],Ml=!1,jl=-1;function Bl(){Ml&&Rl&&(Ml=!1,Rl.length?Pl=Rl.concat(Pl):jl=-1,Pl.length&&Fl())}function Fl(){if(!Ml){var t=Nl(Bl);Ml=!0;for(var e=Pl.length;e;){for(Rl=Pl,Pl=[];++jl<e;)Rl&&Rl[jl].run();jl=-1,e=Pl.length}Rl=null,Ml=!1,function(t){if(Dl===clearTimeout)return clearTimeout(t);if((Dl===kl||!Dl)&&clearTimeout)return Dl=clearTimeout,clearTimeout(t);try{Dl(t)}catch(e){try{return Dl.call(null,t)}catch(e){return Dl.call(this,t)}}}(t)}}function $l(t,e){this.fun=t,this.array=e}function Ul(){}Ol.nextTick=function(t){var e=new Array(arguments.length-1);if(arguments.length>1)for(var n=1;n<arguments.length;n++)e[n-1]=arguments[n];Pl.push(new $l(t,e)),1!==Pl.length||Ml||Nl(Fl)},$l.prototype.run=function(){this.fun.apply(null,this.array)},Ol.title="browser",Ol.browser=!0,Ol.env={},Ol.argv=[],Ol.version="",Ol.versions={},Ol.on=Ul,Ol.addListener=Ul,Ol.once=Ul,Ol.off=Ul,Ol.removeListener=Ul,Ol.removeAllListeners=Ul,Ol.emit=Ul,Ol.prependListener=Ul,Ol.prependOnceListener=Ul,Ol.listeners=function(t){return[]},Ol.binding=function(t){throw new Error("process.binding is not supported")},Ol.cwd=function(){return"/"},Ol.chdir=function(t){throw new Error("process.chdir is not supported")},Ol.umask=function(){return 0};
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Hl{constructor(t){this.uid=t}isAuthenticated(){return null!=this.uid}toKey(){return this.isAuthenticated()?"uid:"+this.uid:"anonymous-user"}isEqual(t){return t.uid===this.uid}}Hl.UNAUTHENTICATED=new Hl(null),Hl.GOOGLE_CREDENTIALS=new Hl("google-credentials-uid"),Hl.FIRST_PARTY=new Hl("first-party-uid"),Hl.MOCK_USER=new Hl("mock-user");
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let Vl="9.9.2";
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const zl=new Hi("@firebase/firestore");function Wl(t,...e){if(zl.logLevel<=ji.DEBUG){const n=e.map(Gl);zl.debug(`Firestore (${Vl}): ${t}`,...n)}}function Kl(t,...e){if(zl.logLevel<=ji.ERROR){const n=e.map(Gl);zl.error(`Firestore (${Vl}): ${t}`,...n)}}function Gl(t){if("string"==typeof t)return t;try{return e=t,JSON.stringify(e)}catch(e){return t}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */var e}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function ql(t="Unexpected state"){const e=`FIRESTORE (${Vl}) INTERNAL ASSERTION FAILED: `+t;throw Kl(e),new Error(e)}function Xl(t,e){t||ql()}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const Yl={OK:"ok",CANCELLED:"cancelled",UNKNOWN:"unknown",INVALID_ARGUMENT:"invalid-argument",DEADLINE_EXCEEDED:"deadline-exceeded",NOT_FOUND:"not-found",ALREADY_EXISTS:"already-exists",PERMISSION_DENIED:"permission-denied",UNAUTHENTICATED:"unauthenticated",RESOURCE_EXHAUSTED:"resource-exhausted",FAILED_PRECONDITION:"failed-precondition",ABORTED:"aborted",OUT_OF_RANGE:"out-of-range",UNIMPLEMENTED:"unimplemented",INTERNAL:"internal",UNAVAILABLE:"unavailable",DATA_LOSS:"data-loss"};class Jl extends ki{constructor(t,e){super(t,e),this.code=t,this.message=e,this.toString=()=>`${this.name}: [code=${this.code}]: ${this.message}`}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Ql{constructor(){this.promise=new Promise(((t,e)=>{this.resolve=t,this.reject=e}))}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Zl{constructor(t,e){this.user=e,this.type="OAuth",this.headers=new Map,this.headers.set("Authorization",`Bearer ${t}`)}}class th{getToken(){return Promise.resolve(null)}invalidateToken(){}start(t,e){t.enqueueRetryable((()=>e(Hl.UNAUTHENTICATED)))}shutdown(){}}class eh{constructor(t){this.t=t,this.currentUser=Hl.UNAUTHENTICATED,this.i=0,this.forceRefresh=!1,this.auth=null}start(t,e){let n=this.i;const i=t=>this.i!==n?(n=this.i,e(t)):Promise.resolve();let r=new Ql;this.o=()=>{this.i++,this.currentUser=this.u(),r.resolve(),r=new Ql,t.enqueueRetryable((()=>i(this.currentUser)))};const s=()=>{const e=r;t.enqueueRetryable((async()=>{await e.promise,await i(this.currentUser)}))},o=t=>{Wl("FirebaseAuthCredentialsProvider","Auth detected"),this.auth=t,this.auth.addAuthTokenListener(this.o),s()};this.t.onInit((t=>o(t))),setTimeout((()=>{if(!this.auth){const t=this.t.getImmediate({optional:!0});t?o(t):(Wl("FirebaseAuthCredentialsProvider","Auth not yet detected"),r.resolve(),r=new Ql)}}),0),s()}getToken(){const t=this.i,e=this.forceRefresh;return this.forceRefresh=!1,this.auth?this.auth.getToken(e).then((e=>this.i!==t?(Wl("FirebaseAuthCredentialsProvider","getToken aborted due to token change."),this.getToken()):e?(Xl("string"==typeof e.accessToken),new Zl(e.accessToken,this.currentUser)):null)):Promise.resolve(null)}invalidateToken(){this.forceRefresh=!0}shutdown(){this.auth&&this.auth.removeAuthTokenListener(this.o)}u(){const t=this.auth&&this.auth.getUid();return Xl(null===t||"string"==typeof t),new Hl(t)}}class nh{constructor(t,e,n){this.type="FirstParty",this.user=Hl.FIRST_PARTY,this.headers=new Map,this.headers.set("X-Goog-AuthUser",e);const i=t.auth.getAuthHeaderValueForFirstParty([]);i&&this.headers.set("Authorization",i),n&&this.headers.set("X-Goog-Iam-Authorization-Token",n)}}class ih{constructor(t,e,n){this.h=t,this.l=e,this.m=n}getToken(){return Promise.resolve(new nh(this.h,this.l,this.m))}start(t,e){t.enqueueRetryable((()=>e(Hl.FIRST_PARTY)))}shutdown(){}invalidateToken(){}}class rh{constructor(t){this.value=t,this.type="AppCheck",this.headers=new Map,t&&t.length>0&&this.headers.set("x-firebase-appcheck",this.value)}}class sh{constructor(t){this.g=t,this.forceRefresh=!1,this.appCheck=null,this.p=null}start(t,e){const n=t=>{null!=t.error&&Wl("FirebaseAppCheckTokenProvider",`Error getting App Check token; using placeholder token instead. Error: ${t.error.message}`);const n=t.token!==this.p;return this.p=t.token,Wl("FirebaseAppCheckTokenProvider",`Received ${n?"new":"existing"} token.`),n?e(t.token):Promise.resolve()};this.o=e=>{t.enqueueRetryable((()=>n(e)))};const i=t=>{Wl("FirebaseAppCheckTokenProvider","AppCheck detected"),this.appCheck=t,this.appCheck.addTokenListener(this.o)};this.g.onInit((t=>i(t))),setTimeout((()=>{if(!this.appCheck){const t=this.g.getImmediate({optional:!0});t?i(t):Wl("FirebaseAppCheckTokenProvider","AppCheck not yet detected")}}),0)}getToken(){const t=this.forceRefresh;return this.forceRefresh=!1,this.appCheck?this.appCheck.getToken(t).then((t=>t?(Xl("string"==typeof t.token),this.p=t.token,new rh(t.token)):null)):Promise.resolve(null)}invalidateToken(){this.forceRefresh=!0}shutdown(){this.appCheck&&this.appCheck.removeTokenListener(this.o)}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function oh(t){const e="undefined"!=typeof self&&(self.crypto||self.msCrypto),n=new Uint8Array(t);if(e&&"function"==typeof e.getRandomValues)e.getRandomValues(n);else for(let e=0;e<t;e++)n[e]=Math.floor(256*Math.random());return n}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class ah{static I(){const t="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",e=Math.floor(256/t.length)*t.length;let n="";for(;n.length<20;){const i=oh(40);for(let r=0;r<i.length;++r)n.length<20&&i[r]<e&&(n+=t.charAt(i[r]%t.length))}return n}}function lh(t,e){return t<e?-1:t>e?1:0}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class hh{constructor(t,e,n){void 0===e?e=0:e>t.length&&ql(),void 0===n?n=t.length-e:n>t.length-e&&ql(),this.segments=t,this.offset=e,this.len=n}get length(){return this.len}isEqual(t){return 0===hh.comparator(this,t)}child(t){const e=this.segments.slice(this.offset,this.limit());return t instanceof hh?t.forEach((t=>{e.push(t)})):e.push(t),this.construct(e)}limit(){return this.offset+this.length}popFirst(t){return t=void 0===t?1:t,this.construct(this.segments,this.offset+t,this.length-t)}popLast(){return this.construct(this.segments,this.offset,this.length-1)}firstSegment(){return this.segments[this.offset]}lastSegment(){return this.get(this.length-1)}get(t){return this.segments[this.offset+t]}isEmpty(){return 0===this.length}isPrefixOf(t){if(t.length<this.length)return!1;for(let e=0;e<this.length;e++)if(this.get(e)!==t.get(e))return!1;return!0}isImmediateParentOf(t){if(this.length+1!==t.length)return!1;for(let e=0;e<this.length;e++)if(this.get(e)!==t.get(e))return!1;return!0}forEach(t){for(let e=this.offset,n=this.limit();e<n;e++)t(this.segments[e])}toArray(){return this.segments.slice(this.offset,this.limit())}static comparator(t,e){const n=Math.min(t.length,e.length);for(let i=0;i<n;i++){const n=t.get(i),r=e.get(i);if(n<r)return-1;if(n>r)return 1}return t.length<e.length?-1:t.length>e.length?1:0}}class ch extends hh{construct(t,e,n){return new ch(t,e,n)}canonicalString(){return this.toArray().join("/")}toString(){return this.canonicalString()}static fromString(...t){const e=[];for(const n of t){if(n.indexOf("//")>=0)throw new Jl(Yl.INVALID_ARGUMENT,`Invalid segment (${n}). Paths must not contain // in them.`);e.push(...n.split("/").filter((t=>t.length>0)))}return new ch(e)}static emptyPath(){return new ch([])}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class uh{constructor(t){this.path=t}static fromPath(t){return new uh(ch.fromString(t))}static fromName(t){return new uh(ch.fromString(t).popFirst(5))}static empty(){return new uh(ch.emptyPath())}get collectionGroup(){return this.path.popLast().lastSegment()}hasCollectionId(t){return this.path.length>=2&&this.path.get(this.path.length-2)===t}getCollectionGroup(){return this.path.get(this.path.length-2)}getCollectionPath(){return this.path.popLast()}isEqual(t){return null!==t&&0===ch.comparator(this.path,t.path)}toString(){return this.path.toString()}static comparator(t,e){return ch.comparator(t.path,e.path)}static isDocumentKey(t){return t.length%2==0}static fromSegments(t){return new uh(new ch(t.slice()))}}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class fh{constructor(t,e,n,i){this.indexId=t,this.collectionGroup=e,this.fields=n,this.indexState=i}}fh.UNKNOWN_ID=-1;function dh(t){return"IndexedDbTransactionError"===t.name}
/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class ph{constructor(t,e){this.previousValue=t,e&&(e.sequenceNumberHandler=t=>this.it(t),this.rt=t=>e.writeSequenceNumber(t))}it(t){return this.previousValue=Math.max(t,this.previousValue),this.previousValue}next(){const t=++this.previousValue;return this.rt&&this.rt(t),t}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
ph.ot=-1;class gh{constructor(t,e){this.comparator=t,this.root=e||yh.EMPTY}insert(t,e){return new gh(this.comparator,this.root.insert(t,e,this.comparator).copy(null,null,yh.BLACK,null,null))}remove(t){return new gh(this.comparator,this.root.remove(t,this.comparator).copy(null,null,yh.BLACK,null,null))}get(t){let e=this.root;for(;!e.isEmpty();){const n=this.comparator(t,e.key);if(0===n)return e.value;n<0?e=e.left:n>0&&(e=e.right)}return null}indexOf(t){let e=0,n=this.root;for(;!n.isEmpty();){const i=this.comparator(t,n.key);if(0===i)return e+n.left.size;i<0?n=n.left:(e+=n.left.size+1,n=n.right)}return-1}isEmpty(){return this.root.isEmpty()}get size(){return this.root.size}minKey(){return this.root.minKey()}maxKey(){return this.root.maxKey()}inorderTraversal(t){return this.root.inorderTraversal(t)}forEach(t){this.inorderTraversal(((e,n)=>(t(e,n),!1)))}toString(){const t=[];return this.inorderTraversal(((e,n)=>(t.push(`${e}:${n}`),!1))),`{${t.join(", ")}}`}reverseTraversal(t){return this.root.reverseTraversal(t)}getIterator(){return new mh(this.root,null,this.comparator,!1)}getIteratorFrom(t){return new mh(this.root,t,this.comparator,!1)}getReverseIterator(){return new mh(this.root,null,this.comparator,!0)}getReverseIteratorFrom(t){return new mh(this.root,t,this.comparator,!0)}}class mh{constructor(t,e,n,i){this.isReverse=i,this.nodeStack=[];let r=1;for(;!t.isEmpty();)if(r=e?n(t.key,e):1,e&&i&&(r*=-1),r<0)t=this.isReverse?t.left:t.right;else{if(0===r){this.nodeStack.push(t);break}this.nodeStack.push(t),t=this.isReverse?t.right:t.left}}getNext(){let t=this.nodeStack.pop();const e={key:t.key,value:t.value};if(this.isReverse)for(t=t.left;!t.isEmpty();)this.nodeStack.push(t),t=t.right;else for(t=t.right;!t.isEmpty();)this.nodeStack.push(t),t=t.left;return e}hasNext(){return this.nodeStack.length>0}peek(){if(0===this.nodeStack.length)return null;const t=this.nodeStack[this.nodeStack.length-1];return{key:t.key,value:t.value}}}class yh{constructor(t,e,n,i,r){this.key=t,this.value=e,this.color=null!=n?n:yh.RED,this.left=null!=i?i:yh.EMPTY,this.right=null!=r?r:yh.EMPTY,this.size=this.left.size+1+this.right.size}copy(t,e,n,i,r){return new yh(null!=t?t:this.key,null!=e?e:this.value,null!=n?n:this.color,null!=i?i:this.left,null!=r?r:this.right)}isEmpty(){return!1}inorderTraversal(t){return this.left.inorderTraversal(t)||t(this.key,this.value)||this.right.inorderTraversal(t)}reverseTraversal(t){return this.right.reverseTraversal(t)||t(this.key,this.value)||this.left.reverseTraversal(t)}min(){return this.left.isEmpty()?this:this.left.min()}minKey(){return this.min().key}maxKey(){return this.right.isEmpty()?this.key:this.right.maxKey()}insert(t,e,n){let i=this;const r=n(t,i.key);return i=r<0?i.copy(null,null,null,i.left.insert(t,e,n),null):0===r?i.copy(null,e,null,null,null):i.copy(null,null,null,null,i.right.insert(t,e,n)),i.fixUp()}removeMin(){if(this.left.isEmpty())return yh.EMPTY;let t=this;return t.left.isRed()||t.left.left.isRed()||(t=t.moveRedLeft()),t=t.copy(null,null,null,t.left.removeMin(),null),t.fixUp()}remove(t,e){let n,i=this;if(e(t,i.key)<0)i.left.isEmpty()||i.left.isRed()||i.left.left.isRed()||(i=i.moveRedLeft()),i=i.copy(null,null,null,i.left.remove(t,e),null);else{if(i.left.isRed()&&(i=i.rotateRight()),i.right.isEmpty()||i.right.isRed()||i.right.left.isRed()||(i=i.moveRedRight()),0===e(t,i.key)){if(i.right.isEmpty())return yh.EMPTY;n=i.right.min(),i=i.copy(n.key,n.value,null,null,i.right.removeMin())}i=i.copy(null,null,null,null,i.right.remove(t,e))}return i.fixUp()}isRed(){return this.color}fixUp(){let t=this;return t.right.isRed()&&!t.left.isRed()&&(t=t.rotateLeft()),t.left.isRed()&&t.left.left.isRed()&&(t=t.rotateRight()),t.left.isRed()&&t.right.isRed()&&(t=t.colorFlip()),t}moveRedLeft(){let t=this.colorFlip();return t.right.left.isRed()&&(t=t.copy(null,null,null,null,t.right.rotateRight()),t=t.rotateLeft(),t=t.colorFlip()),t}moveRedRight(){let t=this.colorFlip();return t.left.left.isRed()&&(t=t.rotateRight(),t=t.colorFlip()),t}rotateLeft(){const t=this.copy(null,null,yh.RED,null,this.right.left);return this.right.copy(null,null,this.color,t,null)}rotateRight(){const t=this.copy(null,null,yh.RED,this.left.right,null);return this.left.copy(null,null,this.color,null,t)}colorFlip(){const t=this.left.copy(null,null,!this.left.color,null,null),e=this.right.copy(null,null,!this.right.color,null,null);return this.copy(null,null,!this.color,t,e)}checkMaxDepth(){const t=this.check();return Math.pow(2,t)<=this.size+1}check(){if(this.isRed()&&this.left.isRed())throw ql();if(this.right.isRed())throw ql();const t=this.left.check();if(t!==this.right.check())throw ql();return t+(this.isRed()?0:1)}}yh.EMPTY=null,yh.RED=!0,yh.BLACK=!1,yh.EMPTY=new class{constructor(){this.size=0}get key(){throw ql()}get value(){throw ql()}get color(){throw ql()}get left(){throw ql()}get right(){throw ql()}copy(t,e,n,i,r){return this}insert(t,e,n){return new yh(t,e)}remove(t,e){return this}isEmpty(){return!0}inorderTraversal(t){return!1}reverseTraversal(t){return!1}minKey(){return null}maxKey(){return null}isRed(){return!1}checkMaxDepth(){return!0}check(){return 0}};
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class vh{constructor(t){this.comparator=t,this.data=new gh(this.comparator)}has(t){return null!==this.data.get(t)}first(){return this.data.minKey()}last(){return this.data.maxKey()}get size(){return this.data.size}indexOf(t){return this.data.indexOf(t)}forEach(t){this.data.inorderTraversal(((e,n)=>(t(e),!1)))}forEachInRange(t,e){const n=this.data.getIteratorFrom(t[0]);for(;n.hasNext();){const i=n.getNext();if(this.comparator(i.key,t[1])>=0)return;e(i.key)}}forEachWhile(t,e){let n;for(n=void 0!==e?this.data.getIteratorFrom(e):this.data.getIterator();n.hasNext();)if(!t(n.getNext().key))return}firstAfterOrEqual(t){const e=this.data.getIteratorFrom(t);return e.hasNext()?e.getNext().key:null}getIterator(){return new bh(this.data.getIterator())}getIteratorFrom(t){return new bh(this.data.getIteratorFrom(t))}add(t){return this.copy(this.data.remove(t).insert(t,!0))}delete(t){return this.has(t)?this.copy(this.data.remove(t)):this}isEmpty(){return this.data.isEmpty()}unionWith(t){let e=this;return e.size<t.size&&(e=t,t=this),t.forEach((t=>{e=e.add(t)})),e}isEqual(t){if(!(t instanceof vh))return!1;if(this.size!==t.size)return!1;const e=this.data.getIterator(),n=t.data.getIterator();for(;e.hasNext();){const t=e.getNext().key,i=n.getNext().key;if(0!==this.comparator(t,i))return!1}return!0}toArray(){const t=[];return this.forEach((e=>{t.push(e)})),t}toString(){const t=[];return this.forEach((e=>t.push(e))),"SortedSet("+t.toString()+")"}copy(t){const e=new vh(this.comparator);return e.data=t,e}}class bh{constructor(t){this.iter=t}getNext(){return this.iter.getNext().key}hasNext(){return this.iter.hasNext()}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class _h{constructor(t){this.binaryString=t}static fromBase64String(t){const e=atob(t);return new _h(e)}static fromUint8Array(t){const e=function(t){let e="";for(let n=0;n<t.length;++n)e+=String.fromCharCode(t[n]);return e}(t);return new _h(e)}[Symbol.iterator](){let t=0;return{next:()=>t<this.binaryString.length?{value:this.binaryString.charCodeAt(t++),done:!1}:{value:void 0,done:!0}}}toBase64(){var t;return t=this.binaryString,btoa(t)}toUint8Array(){return function(t){const e=new Uint8Array(t.length);for(let n=0;n<t.length;n++)e[n]=t.charCodeAt(n);return e}(this.binaryString)}approximateByteSize(){return 2*this.binaryString.length}compareTo(t){return lh(this.binaryString,t.binaryString)}isEqual(t){return this.binaryString===t.binaryString}}_h.EMPTY_BYTE_STRING=new _h("");new RegExp(/^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d(?:\.(\d+))?Z$/);function Eh(t){return"number"==typeof t?t:"string"==typeof t?Number(t):0}function wh(t){return"string"==typeof t?_h.fromBase64String(t):_h.fromUint8Array(t)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Sh{constructor(t,e,n,i,r,s,o,a){this.databaseId=t,this.appId=e,this.persistenceKey=n,this.host=i,this.ssl=r,this.forceLongPolling=s,this.autoDetectLongPolling=o,this.useFetchStreams=a}}class Ah{constructor(t,e){this.projectId=t,this.database=e||"(default)"}static empty(){return new Ah("","")}get isDefaultDatabase(){return"(default)"===this.database}isEqual(t){return t instanceof Ah&&t.projectId===this.projectId&&t.database===this.database}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function xh(t){return 0===t&&1/t==-1/0}function Th(t){return"__max__"===(((t.mapValue||{}).fields||{}).__type__||{}).stringValue}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var Ch,Ih;(Ih=Ch||(Ch={}))[Ih.OK=0]="OK",Ih[Ih.CANCELLED=1]="CANCELLED",Ih[Ih.UNKNOWN=2]="UNKNOWN",Ih[Ih.INVALID_ARGUMENT=3]="INVALID_ARGUMENT",Ih[Ih.DEADLINE_EXCEEDED=4]="DEADLINE_EXCEEDED",Ih[Ih.NOT_FOUND=5]="NOT_FOUND",Ih[Ih.ALREADY_EXISTS=6]="ALREADY_EXISTS",Ih[Ih.PERMISSION_DENIED=7]="PERMISSION_DENIED",Ih[Ih.UNAUTHENTICATED=16]="UNAUTHENTICATED",Ih[Ih.RESOURCE_EXHAUSTED=8]="RESOURCE_EXHAUSTED",Ih[Ih.FAILED_PRECONDITION=9]="FAILED_PRECONDITION",Ih[Ih.ABORTED=10]="ABORTED",Ih[Ih.OUT_OF_RANGE=11]="OUT_OF_RANGE",Ih[Ih.UNIMPLEMENTED=12]="UNIMPLEMENTED",Ih[Ih.INTERNAL=13]="INTERNAL",Ih[Ih.UNAVAILABLE=14]="UNAVAILABLE",Ih[Ih.DATA_LOSS=15]="DATA_LOSS";
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
new gh(uh.comparator);new gh(uh.comparator);new gh(uh.comparator),new vh(uh.comparator);new vh(lh);const Dh=["mutationQueues","mutations","documentMutations","remoteDocuments","targets","owner","targetGlobal","targetDocuments","clientMetadata","remoteDocumentGlobal","collectionParents","bundles","namedQueries"],Oh=["mutationQueues","mutations","documentMutations","remoteDocumentsV14","targets","owner","targetGlobal","targetDocuments","clientMetadata","remoteDocumentGlobal","collectionParents","bundles","namedQueries","documentOverlays"],Lh=Oh;
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class kh{constructor(){}re(t,e){this.oe(t,e),e.ue()}oe(t,e){if("nullValue"in t)this.ce(e,5);else if("booleanValue"in t)this.ce(e,10),e.ae(t.booleanValue?1:0);else if("integerValue"in t)this.ce(e,15),e.ae(Eh(t.integerValue));else if("doubleValue"in t){const n=Eh(t.doubleValue);isNaN(n)?this.ce(e,13):(this.ce(e,15),xh(n)?e.ae(0):e.ae(n))}else if("timestampValue"in t){const n=t.timestampValue;this.ce(e,20),"string"==typeof n?e.he(n):(e.he(`${n.seconds||""}`),e.ae(n.nanos||0))}else if("stringValue"in t)this.le(t.stringValue,e),this.fe(e);else if("bytesValue"in t)this.ce(e,30),e.de(wh(t.bytesValue)),this.fe(e);else if("referenceValue"in t)this._e(t.referenceValue,e);else if("geoPointValue"in t){const n=t.geoPointValue;this.ce(e,45),e.ae(n.latitude||0),e.ae(n.longitude||0)}else"mapValue"in t?Th(t)?this.ce(e,Number.MAX_SAFE_INTEGER):(this.we(t.mapValue,e),this.fe(e)):"arrayValue"in t?(this.me(t.arrayValue,e),this.fe(e)):ql()}le(t,e){this.ce(e,25),this.ge(t,e)}ge(t,e){e.he(t)}we(t,e){const n=t.fields||{};this.ce(e,55);for(const t of Object.keys(n))this.le(t,e),this.oe(n[t],e)}me(t,e){const n=t.values||[];this.ce(e,50);for(const t of n)this.oe(t,e)}_e(t,e){this.ce(e,37),uh.fromName(t).path.forEach((t=>{this.ce(e,60),this.ge(t,e)}))}ce(t,e){t.ae(e)}fe(t){t.ae(2)}}kh.ye=new kh;
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
new Uint8Array(0);class Nh{constructor(t,e,n){this.cacheSizeCollectionThreshold=t,this.percentileToCollect=e,this.maximumSequenceNumbersToCollect=n}static withCacheSize(t){return new Nh(t,Nh.DEFAULT_COLLECTION_PERCENTILE,Nh.DEFAULT_MAX_SEQUENCE_NUMBERS_TO_COLLECT)}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */Nh.DEFAULT_COLLECTION_PERCENTILE=10,Nh.DEFAULT_MAX_SEQUENCE_NUMBERS_TO_COLLECT=1e3,Nh.DEFAULT=new Nh(41943040,Nh.DEFAULT_COLLECTION_PERCENTILE,Nh.DEFAULT_MAX_SEQUENCE_NUMBERS_TO_COLLECT),Nh.DISABLED=new Nh(-1,0,0);function Rh(){return"undefined"!=typeof document?document:null}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Ph{constructor(t,e,n=1e3,i=1.5,r=6e4){this.js=t,this.timerId=e,this.lo=n,this.fo=i,this._o=r,this.wo=0,this.mo=null,this.yo=Date.now(),this.reset()}reset(){this.wo=0}po(){this.wo=this._o}Io(t){this.cancel();const e=Math.floor(this.wo+this.To()),n=Math.max(0,Date.now()-this.yo),i=Math.max(0,e-n);i>0&&Wl("ExponentialBackoff",`Backing off for ${i} ms (base delay: ${this.wo} ms, delay with jitter: ${e} ms, last attempt: ${n} ms ago)`),this.mo=this.js.enqueueAfterDelay(this.timerId,i,(()=>(this.yo=Date.now(),t()))),this.wo*=this.fo,this.wo<this.lo&&(this.wo=this.lo),this.wo>this._o&&(this.wo=this._o)}Eo(){null!==this.mo&&(this.mo.skipDelay(),this.mo=null)}cancel(){null!==this.mo&&(this.mo.cancel(),this.mo=null)}To(){return(Math.random()-.5)*this.wo}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Mh{constructor(t,e,n,i,r){this.asyncQueue=t,this.timerId=e,this.targetTimeMs=n,this.op=i,this.removalCallback=r,this.deferred=new Ql,this.then=this.deferred.promise.then.bind(this.deferred.promise),this.deferred.promise.catch((t=>{}))}static createAndSchedule(t,e,n,i,r){const s=Date.now()+n,o=new Mh(t,e,s,i,r);return o.start(n),o}start(t){this.timerHandle=setTimeout((()=>this.handleDelayElapsed()),t)}skipDelay(){return this.handleDelayElapsed()}cancel(t){null!==this.timerHandle&&(this.clearTimeout(),this.deferred.reject(new Jl(Yl.CANCELLED,"Operation cancelled"+(t?": "+t:""))))}handleDelayElapsed(){this.asyncQueue.enqueueAndForget((()=>null!==this.timerHandle?(this.clearTimeout(),this.op().then((t=>this.deferred.resolve(t)))):Promise.resolve()))}clearTimeout(){null!==this.timerHandle&&(this.removalCallback(this),clearTimeout(this.timerHandle),this.timerHandle=null)}}function jh(t,e){if(Kl("AsyncQueue",`${e}: ${t}`),dh(t))return new Jl(Yl.UNAVAILABLE,`${e}: ${t}`);throw t}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Bh{constructor(t,e,n,i){this.authCredentials=t,this.appCheckCredentials=e,this.asyncQueue=n,this.databaseInfo=i,this.user=Hl.UNAUTHENTICATED,this.clientId=ah.I(),this.authCredentialListener=()=>Promise.resolve(),this.appCheckCredentialListener=()=>Promise.resolve(),this.authCredentials.start(n,(async t=>{Wl("FirestoreClient","Received user=",t.uid),await this.authCredentialListener(t),this.user=t})),this.appCheckCredentials.start(n,(t=>(Wl("FirestoreClient","Received new app check token=",t),this.appCheckCredentialListener(t,this.user))))}async getConfiguration(){return{asyncQueue:this.asyncQueue,databaseInfo:this.databaseInfo,clientId:this.clientId,authCredentials:this.authCredentials,appCheckCredentials:this.appCheckCredentials,initialUser:this.user,maxConcurrentLimboResolutions:100}}setCredentialChangeListener(t){this.authCredentialListener=t}setAppCheckTokenChangeListener(t){this.appCheckCredentialListener=t}verifyNotTerminated(){if(this.asyncQueue.isShuttingDown)throw new Jl(Yl.FAILED_PRECONDITION,"The client has already been terminated.")}terminate(){this.asyncQueue.enterRestrictedMode();const t=new Ql;return this.asyncQueue.enqueueAndForgetEvenWhileRestricted((async()=>{try{this.onlineComponents&&await this.onlineComponents.terminate(),this.offlineComponents&&await this.offlineComponents.terminate(),this.authCredentials.shutdown(),this.appCheckCredentials.shutdown(),t.resolve()}catch(e){const n=jh(e,"Failed to shutdown persistence");t.reject(n)}})),t.promise}}const Fh=new Map;
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class $h{constructor(t){var e;if(void 0===t.host){if(void 0!==t.ssl)throw new Jl(Yl.INVALID_ARGUMENT,"Can't provide ssl option if host option is not set");this.host="firestore.googleapis.com",this.ssl=!0}else this.host=t.host,this.ssl=null===(e=t.ssl)||void 0===e||e;if(this.credentials=t.credentials,this.ignoreUndefinedProperties=!!t.ignoreUndefinedProperties,void 0===t.cacheSizeBytes)this.cacheSizeBytes=41943040;else{if(-1!==t.cacheSizeBytes&&t.cacheSizeBytes<1048576)throw new Jl(Yl.INVALID_ARGUMENT,"cacheSizeBytes must be at least 1048576");this.cacheSizeBytes=t.cacheSizeBytes}this.experimentalForceLongPolling=!!t.experimentalForceLongPolling,this.experimentalAutoDetectLongPolling=!!t.experimentalAutoDetectLongPolling,this.useFetchStreams=!!t.useFetchStreams,function(t,e,n,i){if(!0===e&&!0===i)throw new Jl(Yl.INVALID_ARGUMENT,`${t} and ${n} cannot be used together.`)}("experimentalForceLongPolling",t.experimentalForceLongPolling,"experimentalAutoDetectLongPolling",t.experimentalAutoDetectLongPolling)}isEqual(t){return this.host===t.host&&this.ssl===t.ssl&&this.credentials===t.credentials&&this.cacheSizeBytes===t.cacheSizeBytes&&this.experimentalForceLongPolling===t.experimentalForceLongPolling&&this.experimentalAutoDetectLongPolling===t.experimentalAutoDetectLongPolling&&this.ignoreUndefinedProperties===t.ignoreUndefinedProperties&&this.useFetchStreams===t.useFetchStreams}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Uh{constructor(t,e,n){this._authCredentials=e,this._appCheckCredentials=n,this.type="firestore-lite",this._persistenceKey="(lite)",this._settings=new $h({}),this._settingsFrozen=!1,t instanceof Ah?this._databaseId=t:(this._app=t,this._databaseId=function(t){if(!Object.prototype.hasOwnProperty.apply(t.options,["projectId"]))throw new Jl(Yl.INVALID_ARGUMENT,'"projectId" not provided in firebase.initializeApp.');return new Ah(t.options.projectId)}(t))}get app(){if(!this._app)throw new Jl(Yl.FAILED_PRECONDITION,"Firestore was not initialized using the Firebase SDK. 'app' is not available");return this._app}get _initialized(){return this._settingsFrozen}get _terminated(){return void 0!==this._terminateTask}_setSettings(t){if(this._settingsFrozen)throw new Jl(Yl.FAILED_PRECONDITION,"Firestore has already been started and its settings can no longer be changed. You can only modify settings before calling any other methods on a Firestore object.");this._settings=new $h(t),void 0!==t.credentials&&(this._authCredentials=function(t){if(!t)return new th;switch(t.type){case"gapi":const e=t.client;return Xl(!("object"!=typeof e||null===e||!e.auth||!e.auth.getAuthHeaderValueForFirstParty)),new ih(e,t.sessionIndex||"0",t.iamToken||null);case"provider":return t.client;default:throw new Jl(Yl.INVALID_ARGUMENT,"makeAuthCredentialsProvider failed due to invalid credential type")}}(t.credentials))}_getSettings(){return this._settings}_freezeSettings(){return this._settingsFrozen=!0,this._settings}_delete(){return this._terminateTask||(this._terminateTask=this._terminate()),this._terminateTask}toJSON(){return{app:this._app,databaseId:this._databaseId,settings:this._settings}}_terminate(){return function(t){const e=Fh.get(t);e&&(Wl("ComponentProvider","Removing Datastore"),Fh.delete(t),e.terminate())}(this),Promise.resolve()}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Hh{constructor(){this.Mc=Promise.resolve(),this.Oc=[],this.Fc=!1,this.$c=[],this.Bc=null,this.Lc=!1,this.Uc=!1,this.qc=[],this.So=new Ph(this,"async_queue_retry"),this.Kc=()=>{const t=Rh();t&&Wl("AsyncQueue","Visibility state changed to "+t.visibilityState),this.So.Eo()};const t=Rh();t&&"function"==typeof t.addEventListener&&t.addEventListener("visibilitychange",this.Kc)}get isShuttingDown(){return this.Fc}enqueueAndForget(t){this.enqueue(t)}enqueueAndForgetEvenWhileRestricted(t){this.Gc(),this.Qc(t)}enterRestrictedMode(t){if(!this.Fc){this.Fc=!0,this.Uc=t||!1;const e=Rh();e&&"function"==typeof e.removeEventListener&&e.removeEventListener("visibilitychange",this.Kc)}}enqueue(t){if(this.Gc(),this.Fc)return new Promise((()=>{}));const e=new Ql;return this.Qc((()=>this.Fc&&this.Uc?Promise.resolve():(t().then(e.resolve,e.reject),e.promise))).then((()=>e.promise))}enqueueRetryable(t){this.enqueueAndForget((()=>(this.Oc.push(t),this.jc())))}async jc(){if(0!==this.Oc.length){try{await this.Oc[0](),this.Oc.shift(),this.So.reset()}catch(t){if(!dh(t))throw t;Wl("AsyncQueue","Operation failed with retryable error: "+t)}this.Oc.length>0&&this.So.Io((()=>this.jc()))}}Qc(t){const e=this.Mc.then((()=>(this.Lc=!0,t().catch((t=>{this.Bc=t,this.Lc=!1;const e=function(t){let e=t.message||"";return t.stack&&(e=t.stack.includes(t.message)?t.stack:t.message+"\n"+t.stack),e}(t);throw Kl("INTERNAL UNHANDLED ERROR: ",e),t})).then((t=>(this.Lc=!1,t))))));return this.Mc=e,e}enqueueAfterDelay(t,e,n){this.Gc(),this.qc.indexOf(t)>-1&&(e=0);const i=Mh.createAndSchedule(this,t,e,n,(t=>this.Wc(t)));return this.$c.push(i),i}Gc(){this.Bc&&ql()}verifyOperationInProgress(){}async zc(){let t;do{t=this.Mc,await t}while(t!==this.Mc)}Hc(t){for(const e of this.$c)if(e.timerId===t)return!0;return!1}Jc(t){return this.zc().then((()=>{this.$c.sort(((t,e)=>t.targetTimeMs-e.targetTimeMs));for(const e of this.$c)if(e.skipDelay(),"all"!==t&&e.timerId===t)break;return this.zc()}))}Yc(t){this.qc.push(t)}Wc(t){const e=this.$c.indexOf(t);this.$c.splice(e,1)}}class Vh extends Uh{constructor(t,e,n){super(t,e,n),this.type="firestore",this._queue=new Hh,this._persistenceKey="name"in t?t.name:"[DEFAULT]"}_terminate(){return this._firestoreClient||zh(this),this._firestoreClient.terminate()}}function zh(t){var e;const n=t._freezeSettings(),i=function(t,e,n,i){return new Sh(t,e,n,i.host,i.ssl,i.experimentalForceLongPolling,i.experimentalAutoDetectLongPolling,i.useFetchStreams)}(t._databaseId,(null===(e=t._app)||void 0===e?void 0:e.options.appId)||"",t._persistenceKey,n);t._firestoreClient=new Bh(t._authCredentials,t._appCheckCredentials,t._queue,i)}new RegExp("[~\\*/\\[\\]]");!function(t,e=!0){Vl="9.9.3",fr(new Pi("firestore",((t,{options:n})=>{const i=t.getProvider("app").getImmediate(),r=new Vh(i,new eh(t.getProvider("auth-internal")),new sh(t.getProvider("app-check-internal")));return n=Object.assign({useFetchStreams:e},n),r._setSettings(n),r}),"PUBLIC")),mr("@firebase/firestore","3.4.14",t),mr("@firebase/firestore","3.4.14","esm2017")}();!function(t=gr()){dr(t,"firestore").getImmediate()}(function(t,e={}){if("object"!=typeof e){e={name:e}}const n=Object.assign({name:"[DEFAULT]",automaticDataCollectionEnabled:!1},e),i=n.name;if("string"!=typeof i||!i)throw mi.create("bad-app-name",{appName:String(i)});const r=fi.get(i);if(r){if(Rn(t,r.options)&&Rn(n,r.config))return r;throw mi.create("duplicate-app",{appName:i})}const s=new Bn(i);for(const t of di.values())s.addComponent(t);const o=new yi(t,n,s);return fi.set(i,o),o}({apiKey:"AIzaSyBXlQu8PqZ1MyPB3NzWo66lcFhuS2buySY",authDomain:"atelier-opteos.firebaseapp.com",projectId:"atelier-opteos",storageBucket:"atelier-opteos.appspot.com",messagingSenderId:"262677179481",appId:"1:262677179481:web:338032507b4d2a3e9c9548"}));var Wh={init(){}};Tn.store("app",Wh),Tn.data("card",(()=>({init(){}}))),window.Alpine=Tn,Tn.start();
//# sourceMappingURL=index.0402f5e7.js.map
