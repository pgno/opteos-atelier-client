import {initializeApp} from 'firebase/firebase-app';
import {getFirestore} from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyBXlQu8PqZ1MyPB3NzWo66lcFhuS2buySY",
    authDomain: "atelier-opteos.firebaseapp.com",
    projectId: "atelier-opteos",
    storageBucket: "atelier-opteos.appspot.com",
    messagingSenderId: "262677179481",
    appId: "1:262677179481:web:338032507b4d2a3e9c9548"
  };
  
const app = initializeApp(firebaseConfig)
const db = getFirestore(app)

export default db;