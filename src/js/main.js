import Alpine from 'alpinejs';
import '../scss/style.scss';

import app from './store/app'
import card from './components/card'

Alpine.store('app',app);
Alpine.data('card',card)

window.Alpine = Alpine
Alpine.start()